<div class="boxed-area blocks-spacer">
    <div class="container">

        <div class="row">
            <div class="span12">
                <div class="main-titles lined">
                    <h2 class="title"><span class="light">Разделы</span> товаров</h2>
                </div>
            </div>
        </div>


        <div class="row popup-products blocks-spacer" style="margin-bottom: 25px;">
            {foreach key=nKey from=$sections item=sec}

            {if $nKey>0 && $nKey%4==0}
        </div>
        <div class="row popup-products blocks-spacer">
            {/if}

            <!-- Product -->
            <div class="span3">
                <div class="product2">
                    <div class="product-inner">
                        <a href="/products/section/{$sec.link}">
                        <div class="product-img">
                            <div class="picture">
                                {if $sec.image!=""}
                                    <img src="/images/sections/{$sec.image}_middle.jpg" alt="" width="100%" />
                                {else}
                                    <img src="/images/sections/default_big.png" alt="" width="100%" />
                                {/if}
                            </div>
                        </div>
                        <div class="main-titles no-margin">
                            <div class="titleHeader" style="border-top:1px solid #b2b2b2;">
                                <h5 class="no-margin" style="font-weight: normal;">{$sec.title|stripslashes|strip_tags}</h5>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
            </div>

            {/foreach}
        </div>

    </div>
</div>