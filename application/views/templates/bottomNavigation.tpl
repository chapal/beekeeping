<ul>
    <li><a href="/about-company.html" {if $menuActive=='about-company'}class="active"{/if}>О компании</a></li>
    <li><a href="/news/page/1" {if $menuActive=='news'}class="active"{/if}>Новости</a></li>
    <li><a href="/catalog/page/1" {if $menuActive=='production'}class="active"{/if}>Продукция</a></li>
    <li><a href="/documentation.html" {if $menuActive=='documentation'}class="active"{/if}>Документы</a></li>
    <li><a href="/wholesalers.html" {if $menuActive=='wholesalers'}class="active"{/if}>Оптовикам</a></li>
    <li><a href="/reviews/page/1" {if $menuActive=='reviews'}class="active"{/if}>Отзывы</a></li>
    <li><a class="last {if $menuActive=='contacts'}active{/if}" href="/contacts.html">Контакты</a></li>
</ul>
