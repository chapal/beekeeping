<div class="darker-stripe">
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul class="breadcrumb">
                    <li>
                        <a href="/">Главная</a>
                    </li>
                    <li><span class="icon-chevron-right"></span></li>
                    <li>
                        <a href="/articles/page/1">Статьи</a>
                    </li>
                    {if $currSection}
                        <li><span class="icon-chevron-right"></span></li>
                        <li>
                            <a href="/articles/section/{$currSection.link}/page/1">{$currSection.title|strip_tags|stripslashes}</a>
                        </li>
                        {if $currCategory}
                            <li><span class="icon-chevron-right"></span></li>
                            <li>
                                <a href="/articles/category/{$currCategory.link}/page/1">{$currCategory.title|strip_tags|stripslashes}</a>
                            </li>
                        {/if}
                    {/if}
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="push-up blocks-spacer">
        <div class="row">

            <!-- Main content -->
            <section class="span12">

                <div class="underlined push-down-15">
                    <div class="row">
                        <div class="span12">
                            <h3>
                                Каталог
                                {if $currSection}
                                    &nbsp;<span class="light">::</span>&nbsp;
                                    <a href="/products/section/{$currSection.link}"><span class="light">{$currSection.title|strip_tags|stripslashes}</span></a>
                                    {if $currCategory}
                                        &nbsp;<span class="light">::</span>&nbsp;
                                        <a href="/products/category/{$currCategory.link}"><span class="light">{$currCategory.title|strip_tags|stripslashes}</span></a>
                                    {/if}
                                {/if}
                            </h3>
                        </div>
                    </div>
                </div>



                <!-- Articles -->
                <div class="row" style="min-height: 500px;">

                    <div class="span3">
                        {foreach from=$item.images item=pImage}
                        <div style="float: left; margin-bottom: 20px;">
                            <img src="/images/products/{$pImage.image}_middle.jpg" style="box-shadow: 0 0 10px rgba(0,0,0,0.5);" />
                        </div>
                        {/foreach}

                    </div>

                    <div class="span9">
                        <div class="product-title">
                            <h1 class="name"><span class="light" id="prodTitle7">{$item.title|stripslashes|strip_tags}</span></h1>
                        </div>
                        <div class="product-description product" style="color:#777777;">
                            {$item.description|stripslashes}
                            <hr>

                            <table border="0" cellpadding="0" cellspacing="0" class="block2_content_td" width="100%">
                                {if $options}
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>Название</td>
                                        {foreach from=$options item=opt}
                                            <td>{$opt.title|strip_tags|stripslashes}</td>
                                        {/foreach}
                                        <td>Количество</td>
                                    </tr>
                                    {foreach from=$related item=prod}
                                    <tr class="plist" onclick="selectProduct({$prod.id});">
                                        <td>
                                            <input type="checkbox" name="products[]" id="check{$prod.id}" value="{$prod.id}" />
                                        </td>
                                        <td>{$prod.title|strip_tags|stripslashes}</td>
                                        {foreach from=$prod.options item=opt}
                                            <td style="text-align: center; vertical-align: middle;">
                                                {if $opt.properties}

                                                    {foreach from=$opt.properties item=prop}
                                                        {if $prop.selected==1}
                                                            {$prop.title|strip_tags|stripslashes}
                                                        {/if}
                                                    {/foreach}
                                                {else}
                                                    &nbsp;
                                                {/if}
                                            </td>
                                        {/foreach}
                                        <td style="width: 70px; vertical-align: middle; text-align: center;">
                                            <input type="text" maxlength="2" class="pCount" value="1" />
                                        </td>
                                    </tr>
                                    {/foreach}
                                {/if}
                            </table>

                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>
{literal}
<script>
    function selectProduct(id){
        if(!$("#check"+id).prop('checked')){
            $("#check"+id).prop('checked', true);
        } else{
            $("#check"+id).prop('checked', false);
        }

    }
</script>
{/literal}