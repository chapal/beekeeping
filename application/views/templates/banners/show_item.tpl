<div class="header_txt3">{$settings.articles|stripslashes|strip_tags}</div>
<div class="content_container">
<div class="content_block">
	<div class="content_title">{$item.article_title|stripslashes|strip_tags}</div>
	<div class="content_text"><p>{$item.article_description|stripslashes}</p></div>
</div>
</div>