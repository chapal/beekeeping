<div class="staticPageBox">
    <div class="pageTitle">{$content.title|stripslashes|strip_tags}</div>
    <div class="pageText">
        {$content.text|stripslashes}
    </div>
</div>