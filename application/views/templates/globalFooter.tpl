<!--  ==========  -->
<!--  = Footer =  -->
<!--  ==========  -->
<footer>

    <!--  ==========  -->
    <!--  = Upper footer =  -->
    <!--  ==========  -->
    {*<div class="foot-light">*}
        {*<div class="container">*}
            {*<div class="row">*}
                {*<div class="span5">*}
                    {*<h2 class="pacifico">*}
                        {*Soft Script &nbsp;*}
                        {*<img src="/images/webmarket.png" alt="Webmarket Cart" class="align-baseline" />*}
                    {*</h2>*}
                    {*<p>*}
                        {*Мы продаем то, что поможет вам ускорить получение прибыли.<br />*}
                        {*Только у нас самые надежные и проверенные решения для вашего бизнесаю*}
                    {*</p>*}
                {*</div>*}
                {*<div class="span3">*}
                    {*<div class="main-titles lined">*}
                        {*<h3 class="title">Facebook</h3>*}
                    {*</div>*}
                    {*<div class="bordered">*}
                        {*<div class="fill-iframe">*}
                            {*<div class="fb-like-box" data-href="https://www.facebook.com/ProteusNet" data-width="292" data-height="200" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>*}
                        {*</div>*}
                    {*</div>*}
                {*</div>*}
                {*<div class="span4">*}
                    {*<div class="main-titles lined">*}
                        {*<h3 class="title">Подписаться</h3>*}
                    {*</div>*}
                    {*<p>*}
                        {*Хотите получать новости нашей компании? Просто введите адрес электронной почты и нажмите отправить.*}
                    {*</p>*}
                    {*<!-- Begin MailChimp Signup Form -->*}
                    {*<div id="mc_embed_signup">*}
                        {*<form action="" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form form-inline" target="_blank" novalidate>*}
                            {*<div class="mc-field-group">*}
                                {*<input type="email" value="" placeholder="Введите ваш e-mail здесь" name="EMAIL" class="required email" id="mce-EMAIL">*}
                                {*<input type="button" value="Подписаться" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary" >*}
                            {*</div>*}
                            {*<div id="mce-responses" class="clear">*}
                                {*<div class="response" id="mce-error-response" style="display:none"></div>*}
                                {*<div class="response" id="mce-success-response" style="display:none"></div>*}
                            {*</div>*}

                        {*</form>*}
                    {*</div>*}
                    {*<!--End mc_embed_signup-->*}
                {*</div>*}
            {*</div>*}
        {*</div>*}
    {*</div> <!-- /upper footer -->*}

    <!--  ==========  -->
    <!--  = Middle footer =  -->
    <!--  ==========  -->
    <div class="foot-dark">
        <div class="container">
            <div class="row">

                <div class="span3">
                    <div class="main-titles" style="text-align: center;">
                        <h3 class="title"><span class="light"><a href="/" style="color: #fff;">Главная</a></span></h3>
                    </div>
                </div>

                <div class="span3">
                    <div class="main-titles" style="text-align: center;">
                        <h3 class="title"><span class="light"><a href="/catalog.html" style="color: #fff;">Каталог</a></span></h3>
                    </div>
                </div>

                <div class="span3">
                    <div class="main-titles" style="text-align: center;">
                        <h3 class="title"><span class="light"><a href="/articles/page/1" style="color: #fff;">Статьи</a></span></h3>
                    </div>
                </div>

                {*<div class="span3">*}
                    {*<div class="main-titles" style="text-align: center;">*}
                        {*<h3 class="title"><span class="light"><a href="/about-us.html" style="color: #fff;">О нас</a></span></h3>*}
                    {*</div>*}
                {*</div>*}

                <div class="span3">
                    <div class="main-titles" style="text-align: center;">
                        <h3 class="title"><span class="light"><a href="/feedback.html" style="color: #fff;">Контакты</a></span></h3>
                    </div>
                </div>

                {*<!--  ==========  -->*}
                {*<!--  = Menu 1 =  -->*}
                {*<!--  ==========  -->*}
                {*<div class="span3">*}
                    {*<div class="main-titles lined">*}
                        {*<h3 class="title"><span class="light">Main</span> Navigation</h3>*}
                    {*</div>*}
                    {*<ul class="nav bold">*}
                        {*<li><a href="#">Home</a></li>*}
                        {*<li><a href="#">Pages</a></li>*}
                        {*<li><a href="#">About Us</a></li>*}
                        {*<li><a href="#">Shortcodes</a></li>*}
                        {*<li><a href="#">Gallery</a></li>*}
                        {*<li><a href="#">Contact Us</a></li>*}
                    {*</ul>*}
                {*</div>*}

                {*<!--  ==========  -->*}
                {*<!--  = Menu 2 =  -->*}
                {*<!--  ==========  -->*}
                {*<div class="span3">*}
                    {*<div class="main-titles lined">*}
                        {*<h3 class="title"><span class="light">Second</span> Navigation</h3>*}
                    {*</div>*}
                    {*<ul class="nav">*}
                        {*<li><a href="#">Lorem Ipsum Dolor Sit</a></li>*}
                        {*<li><a href="#">Amet Webmarket Signup</a></li>*}
                        {*<li><a href="#">Brands</a></li>*}
                        {*<li><a href="#">Latest Tweets Sometging</a></li>*}
                        {*<li><a href="#">Ipsum Sit Lorem Amet</a></li>*}
                    {*</ul>*}
                {*</div>*}

                {*<!--  ==========  -->*}
                {*<!--  = Menu 3 =  -->*}
                {*<!--  ==========  -->*}
                {*<div class="span3">*}
                    {*<div class="main-titles lined">*}
                        {*<h3 class="title"><span class="light">Third</span> Navigation</h3>*}
                    {*</div>*}
                    {*<ul class="nav">*}
                        {*<li><a href="#">Lorem Ipsum Dolor Sit</a></li>*}
                        {*<li><a href="#">Amet Webmarket Signup</a></li>*}
                        {*<li><a href="#">Brands</a></li>*}
                        {*<li><a href="#">Latest Tweets Sometging</a></li>*}
                        {*<li><a href="#">Ipsum Sit Lorem Amet</a></li>*}
                    {*</ul>*}
                {*</div>*}

                {*<!--  ==========  -->*}
                {*<!--  = Menu 4 =  -->*}
                {*<!--  ==========  -->*}
                {*<div class="span3">*}
                    {*<div class="main-titles lined">*}
                        {*<h3 class="title"><span class="light">Fourth</span> Navigation</h3>*}
                    {*</div>*}
                    {*<ul class="nav">*}
                        {*<li><a href="#">Lorem Ipsum Dolor Sit</a></li>*}
                        {*<li><a href="#">Amet Webmarket Signup</a></li>*}
                        {*<li><a href="#">Brands</a></li>*}
                        {*<li><a href="#">Latest Tweets Sometging</a></li>*}
                        {*<li><a href="#">Ipsum Sit Lorem Amet</a></li>*}
                    {*</ul>*}
                {*</div>*}
            </div>
        </div>
    </div> <!-- /middle footer -->

    <!--  ==========  -->
    <!--  = Bottom Footer =  -->
    <!--  ==========  -->
    <div class="foot-last">
        <a href="#" id="toTheTop">
            <span class="icon-chevron-up"></span>
        </a>
        {*<div class="container">*}
            {*<div class="row">*}
                {*<div class="span6">*}
                    {*&copy; Copyright 2014.*}
                    {*Images of products by <a target="_blank" href="http://www.horsefeathers.eu/">Horsefeathers</a>.*}
                {*</div>*}
                {*<div class="span6">*}
                    {*<div class="pull-right">www.softscript.ru</div>*}
                {*</div>*}
            {*</div>*}
        {*</div>*}
    </div> <!-- /bottom footer -->
</footer> <!-- /footer -->


</div>

<div id="fb-root"></div>
{literal}
    <script type="text/javascript">
    (function($){
        $(document).ready(function(){
            $('input[name=subscribe]').on('click',function(){
                var email = $(this).prev().val();
                var regV = /[A-Za-z0-9_\.]{2,20}@[A-Za-z0-9_]{2,20}\.[a-zA-Z]{2,5}/;
                if(email.search(regV) !== -1) {
                    $(this).prev().removeClass('error-state');
                    $.ajax({
                        type:'POST',
                        url:'/ajax/index/subscribe',
                        data:{email:email},
                        dataType:'json',
                        success:function(data){

                                $('.modalNotification').remove();
                                $('<div />').hide().text(data.msg).addClass('modalNotification').appendTo('body').css({'boxShadow':'0px 0px 20px #00BECC','fontSize':'20px','position':'fixed','padding':'20px','backgroundColor':'#FFF','border':'1px solid #666','left':'50%','top':'50%','zIndex':99999,'marginLeft':'-'+$('.modalNotification').width()/2+'px','marginTop':'-'+$('.modalNotification').height()/2+'px'}).show();
                                setTimeout(function(){
                                    $('.modalNotification').remove();
                                },3000);

                        }
                    });

                } else {
                   $(this).prev().removeClass('error-state').addClass('error-state');
                   return false;
                }


            });
        });
    })(jQuery);
    </script>
{/literal}
</body>
</html>