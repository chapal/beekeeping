<!-- Shopping Cart -->
<div class="darker-stripe">
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul class="breadcrumb">
                    <li>
                        <a href="/">Главная</a>
                    </li>
                    <li><span class="icon-chevron-right"></span></li>
                    <li>
                        <a href="/shopping-cart.html">Шаги покупки</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="push-up top-equal blocks-spacer-last">
        <div class="row">

            <!--  Main Title -->
            <div class="span12">
                <div class="title-area">
                    <h1 class="inline"><span class="light">Шаги покупки</span></h1>
                </div>
            </div>

            <div class="span12">
                <div class="center-align">
                    <h1><span class="light">Способы оплаты</span></h1>
                </div>
            </div>

            <form action="/orders/index/payment-methods-do" method="post" name="payment_form" id="payment_form">
                <div class="row" style="min-height: 400px;">
                    <div class="span10 offset1">

                        <!-- Steps -->
                        <div class="checkout-steps">
                            <div class="clearfix">

                                <div class="step done">
                                    <div class="step-badge"><i class="icon-ok"></i></div>
                                    <a href="/shopping-cart.html">
                                        Корзина покупок
                                    </a>
                                </div>

                                <div class="step done">
                                    <div class="step-badge"><i class="icon-ok"></i></div>
                                    <a href="/order-form.html">
                                        Контактная информация
                                    </a>
                                </div>
                                <div class="step active">
                                    <div class="step-badge">3</div>
                                    Способы оплаты
                                </div>
                                <div class="step">
                                    <div class="step-badge">4</div>
                                    Подтвердить и оплатить
                                </div>
                            </div>
                        </div>
                        <!-- /Steps -->

                        <h5><span class="light">Выберите</span> платежную систему</h5>
                        <table class="table table-bordered table-hover paymentList">
                            <thead>
                            <tr>
                                <th>

                                </th>
                                <th>Название</th>
                                <th style="width: 180px;">Логотип</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr  style="cursor: pointer;">
                                <td style="width: 20px; text-align: center; vertical-align: middle;"><input type="radio" name="payment_method"  value="7" checked="checked" /></td>
                                <td style="text-align: left; vertical-align: middle;"><h2><span class="light"><span class="btn btn-danger circle pull-left" style="font-size: 25px; font-style: italic; font-weight: bold;">R</span>obo</span>Kassa</h2></td>
                                <td><img src="/images/robokassa.png" alt="Robokassa" width="180" height="80" /></td>
                            </tr>
                            <tr  style="cursor: pointer;">
                                <td style="width: 20px; text-align: center; vertical-align: middle;"><input type="radio" name="payment_method"  value="5" /></td>
                                <td style="text-align: left; vertical-align: middle;"><h2>WMZ</h2></td>
                                <td style="text-align: center"><img src="/application/views/templates/bills/assets/img/pay_wmz.png" alt="WMZ" height="80" /></td>
                            </tr>
                            <tr  style="cursor: pointer;">
                                <td style="width: 20px; text-align: center; vertical-align: middle;"><input type="radio" name="payment_method" value="4"  /></td>
                                <td style="text-align: left; vertical-align: middle;"><h2>Card</h2></td>
                                <td style="text-align: center"><img src="/application/views/templates/bills/assets/img/cc.png" alt="CC" height="80" /></td>
                            </tr>
                            <tr  style="cursor: pointer;">
                                <td style="width: 20px; text-align: center; vertical-align: middle;"><input type="radio" name="payment_method"  value="1"  /></td>
                                <td style="text-align: left; vertical-align: middle;"><h2>EasyPay</h2></td>
                                <td style="text-align: center"><img src="/application/views/templates/bills/assets/img/pay_easypay.gif" alt="Easypay" width="150" /></td>
                            </tr>
                            <tr  style="cursor: pointer;">
                                <td style="width: 20px; text-align: center; vertical-align: middle;"><input type="radio" name="payment_method"  value="3"  /></td>
                                <td style="text-align: left; vertical-align: middle;"><h2>iPay life:)</h2></td>
                                <td style="text-align: center"><img src="/application/views/templates/bills/assets/img/pay_ipay_life.gif" alt="IPayLife" width="150" /></td>
                            </tr>
                            <tr  style="cursor: pointer;">
                                <td style="width: 20px; text-align: center; vertical-align: middle;"><input type="radio" name="payment_method"  value="2"  /></td>
                                <td style="text-align: left; vertical-align: middle;"><h2>iPay МТС</h2></td>
                                <td style="text-align: center"><img src="/application/views/templates/bills/assets/img/pay_ipay_mts.gif" alt="iPay MTS" width="150" /></td>
                            </tr>
                            <tr  style="cursor: pointer;">
                                <td style="width: 20px; text-align: center; vertical-align: middle;"><input type="radio" name="payment_method" value="6"  /></td>
                                <td style="text-align: left; vertical-align: middle;"><h2>WMB</h2></td>
                                <td style="text-align: center"><img src="/application/views/templates/bills/assets/img/pay_wmb.png" alt="WMB" width="150" /></td>
                            </tr>

                            </tbody>
                        </table>


                        <div class="alert alert-danger in fade hidden" id="warning"></div>

                        <p class="right-align">
                            <input type="hidden" name="status" id="status" value="0" />
                            <a href="/order-form.html" class="btn btn-primary higher bold">НА ШАГ НАЗАД</a>
                            <a href="" id="payment_method_do" class="btn btn-primary higher bold">ПРОДОЛЖИТЬ</a>
                        </p>
                    </div>
                </div>
            </form>



        </div>
    </div>
</div>
{literal}
    <script type="text/javascript">
        if(window.jQuery === undefined) {
            throw "you should install jquery library from http://jquery.com"
        }

        (function($){
            $(document).ready(function(){

                $('#payment_method_do').on('click',function(event){
                    event.preventDefault();
                    var auth;
                    $.ajax({
                       type:'GET',
                       url:'/bills/index/checkauth',
                       dataType:'json',
                       async:false,
                       success:function(data) {
                        if(data.status==500) {
                            auth=false;

                        }
                         if(data.status==200) {
                             auth=true;
                         }
                       }
                    });
                    if(auth == false) {
                        $('a[href="#loginModal"]:contains("Войти")').trigger('click');
                        return false;
                    }


                   var selectedPayment = $('.paymentList input:checked').val();
                   $.ajax({
                       type:'POST',
                       url:'/bills/index/buy',
                       dataType:'html',
                       data:{'payment_system':selectedPayment},
                       beforeSend:function(){
                           $('#payment_method_do').hide();
                       },
                       success:function(response){
                           $(response).hide().appendTo('body');
                           $('form[name="pay"]').submit();
                       }
                   });
                });
            });
        })(jQuery);
    </script>
{/literal}