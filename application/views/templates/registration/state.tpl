<select name="state" id="state" style="width:146px; *width:155px; border:1px solid #dddddd; margin:0 0 0 5px;">
 
<option value="" selected> ---------- </option> 

<option value="AL" {if $member_state=="AL"} selected {/if}>Alabama</option> 

<option value="AK" {if $member_state=="AK"} selected {/if}>Alaska</option> 

<option value="AZ" {if $member_state=="AZ"} selected {/if}>Arizona</option> 

<option value="AR" {if $member_state=="AR"} selected {/if}>Arkansas</option> 

<option value="CA" {if $member_state=="CA"} selected {/if}>California</option> 

<option value="CO" {if $member_state=="CO"} selected {/if}>Colorado</option> 

<option value="CT" {if $member_state=="CT"} selected {/if}>Connecticut</option> 

<option value="DE" {if $member_state=="DE"} selected {/if}>Delaware</option> 

<option value="DC" {if $member_state=="DC"} selected {/if}>District Of Columbia</option> 

<option value="FL" {if $member_state=="FL"} selected {/if}>Florida</option> 

<option value="GA">Georgia</option> 

<option value="HI">Hawaii</option> 

<option value="ID">Idaho</option> 

<option value="IL">Illinois</option> 

<option value="IN">Indiana</option> 

<option value="IA">Iowa</option> 

<option value="KS">Kansas</option> 

<option value="KY">Kentucky</option> 

<option value="LA">Louisiana</option> 

<option value="ME">Maine</option> 

<option value="MD">Maryland</option> 

<option value="MA">Massachusetts</option> 

<option value="MI">Michigan</option> 

<option value="MN">Minnesota</option> 

<option value="MS">Mississippi</option> 

<option value="MO">Missouri</option> 

<option value="MT">Montana</option> 

<option value="NE">Nebraska</option> 

<option value="NV">Nevada</option> 

<option value="NH">New Hampshire</option> 

<option value="NJ">New Jersey</option> 

<option value="NM">New Mexico</option> 

<option value="NY">New York</option> 

<option value="NC">North Carolina</option> 

<option value="ND">North Dakota</option> 

<option value="OH">Ohio</option> 

<option value="OK">Oklahoma</option> 

<option value="OR">Oregon</option> 

<option value="PA">Pennsylvania</option> 

<option value="RI">Rhode Island</option> 

<option value="SC">South Carolina</option> 

<option value="SD">South Dakota</option> 

<option value="TN">Tennessee</option> 

<option value="TX">Texas</option> 

<option value="UT">Utah</option> 

<option value="VT">Vermont</option> 

<option value="VA">Virginia</option> 

<option value="WA">Washington</option> 

<option value="WV">West Virginia</option> 

<option value="WI">Wisconsin</option> 

<option value="WY">Wyoming</option>

</select>