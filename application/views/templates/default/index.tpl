<!-- Slider -->
{include file="topSlider.tpl"}

<!-- Main container -->
<div class="container">
    <div class="row">
    <div class="span12">
        {*<div class="push-up over-slider blocks-spacer">*}

            {*<!--  ==========  -->*}
            {*<!--  = Three Banners =  -->*}
            {*<!--  ==========  -->*}
            {*<div class="row">*}
                {*<div class="span4">*}
                    {*<a href="javascript:void(0);" class="btn btn-block banner">*}
                        {*<span class="title"><span class="light">ПОСЛЕДНИЕ</span> ПРОДАЖИ</span>*}
                        {*<em>более <span style="font-size: 22px;">10</span> продаж в день</em>*}
                    {*</a>*}
                {*</div>*}
                {*<div class="span4">*}
                    {*<a href="javascript:void(0);" class="btn btn-block colored banner">*}
                        {*<span class="title"><span class="light">НАКОПИТЕЛЬНАЯ</span> СКИДКА</span>*}
                        {*<em>скидки до <span style="font-size: 22px;">20%</span></em>*}
                    {*</a>*}
                {*</div>*}
                {*<div class="span4">*}
                    {*<a href="javascript:void(0);" class="btn btn-block banner">*}
                        {*<span class="title"><span class="light">НОВЫЕ</span> ТОВАРЫ</span>*}
                        {*<em>более <span style="font-size: 22px;">10</span> новых товаров каждый день</em>*}
                    {*</a>*}
                {*</div>*}
            {*</div> <!-- /three banners -->*}
        {*</div>*}
    </div>
</div>

    <!-- Featured Products -->
    {*{include file="products/featured_products.tpl"}*}
</div>


<!-- Sections -->
{include file="sections/sections.tpl"}

<!-- New Products -->
{*{include file="products/new_products.tpl"}*}

<!-- Popular products -->
{*{include file="products/popular_products.tpl"}*}

<!-- Latest news -->
{*{include file="news/latest_news.tpl"}*}

<!-- Partners list -->
{*{include file="partners/index.tpl"}*}

