<div class="header_txt3">{$frontendLangParams.ORDER_LOGIN_OR_REGISTER}</div>
<div class="content_container">
<div class="content_block">
	<div class="content_title" style="text-decoration:none;">
		{$frontendLangParams.ORDER_LOGIN}
	</div>
	{include file='login/login2.tpl'}
</div>
<div class="content_block">
	<div class="content_title" style="text-decoration:none;">
		{$frontendLangParams.ORDER_REGISTER}
	</div>
	{include file='registration/form2.tpl'}
</div>
</div>