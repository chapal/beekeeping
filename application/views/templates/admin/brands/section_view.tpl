<table width="100%"  style="border:0px;">
<tr>
	<td align="center" style="border:0px;">
		<span style="font-size:14px;">{$section.title|stripslashes}</span>
	</td>
</tr>
<tr>
	<td style="border:0px;">
		<div style="overflow:auto; height:270px;">
			{$section.description|stripslashes|strip_tags}
		</div>
	</td>
</tr>
</table>