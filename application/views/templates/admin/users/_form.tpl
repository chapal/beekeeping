<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                {if $action == 'edit'}Изменение{else}Добавление{/if} пользователя <small>&nbsp;</small>
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="/admin/users/index/page/1">Пользователи</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:void(0);">{if $action == 'edit'}Изменение{else}Добавление{/if} пользователя</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="dashboard">

        <div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption">{if $action == 'edit'}<i class="icon-pencil"></i> Изменение{else}<i class="icon-plus"></i> Добавление{/if} пользователя</div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
                            <div class="span12">
                                <form method="POST" action="/admin/users/{$action}" name="users_form" id="users_form" enctype="multipart/form-data">
                                    {if $action == 'edit'}
                                        <input type="hidden" name="Users[id]" value="{$item.id}">
                                    {/if}

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Имя:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="required" data-cvalidation-fieldname="Имя" name="Users[first_name]" value="{$item.first_name|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Фамилия:</label>
                                            <div class="controls">
                                                <input type="text" name="Users[last_name]" value="{$item.last_name|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Личный Email:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="email required" data-cvalidation-fieldname="Email" name="Users[email]" value="{$item.email|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Рабочий Email:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="email" data-cvalidation-fieldname="Email" name="Users[work_email]" value="{$item.work_email|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Дополнительный Email:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="email" data-cvalidation-fieldname="Email" name="Users[additional_email]" value="{$item.additional_email|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Skype:</label>
                                            <div class="controls">
                                                <input type="text" name="Users[skype]" value="{$item.skype|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Сайт:</label>
                                            <div class="controls">
                                                <input type="text" name="Users[site]" value="{$item.site|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Комментарий:</label>
                                            <div class="controls">
                                                <textarea name="Users[comment]" class="span6 m-wrap">{$item.comment|stripslashes}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Личный Телефон:</label>
                                            <div class="controls">
                                                <input type="text" name="Users[phone]"  data-cvalidation="toBeInt" data-cvalidation-fieldname="Телефон" value="{$item.phone|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Рабочий Телефон:</label>
                                            <div class="controls">
                                                <input type="text" name="Users[work_phone]"  data-cvalidation="toBeInt" data-cvalidation-fieldname="Телефон" value="{$item.work_phone|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Дополнительный Телефон:</label>
                                            <div class="controls">
                                                <input type="text" name="Users[additional_phone]"  data-cvalidation="toBeInt" data-cvalidation-fieldname="Телефон" value="{$item.additional_phone|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>
                                    {if $action == 'add'}
                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Пароль:  <button type="button" class="btn mini blue generatePass">Сгенерировать пароль</button> <span style="color: #0000ff" class="generatedPassValue"></span></label>
                                            <div class="controls">
                                                <input type="password" data-cvalidation="required equal=confirm_password" data-cvalidation-fieldname="пароль" name="Users[password]" value="{$item.password|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Подтвердить пароль:</label>
                                            <div class="controls">
                                                <input type="password" data-cvalidation="equal=Users[password]" data-cvalidation-fieldname="Подтвердить пароль" name="confirm_password" value="{$item.password|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>
                                    {/if}
                                    {if $action != 'edit'}
                                    <div style="padding-left: 10px; padding-top: 10px;">

                                        <label class="control-label">Выслать пользователю  <input name="sendToUser" type="checkbox" value=""/></label>
                                    </div>
                                    {/if}
                                    {if $action =='edit'}
                                        <div style="padding-left: 10px; padding-top: 10px;margin-bottom: 10px;">
                                            <button type="button" class="sendNewPassword btn blue">Выслать новый пароль</button>
                                        </div>
                                    {/if}
                                    <div class="form-actions" style="padding-left: 20px;">
                                        <button type="submit" class="btn blue submitForm"><i class="icon-ok"></i>Сохранить</button>
                                        <button type="button" class="btn" onclick="document.location.href='/admin/users/index/page/1'"><i class="icon-undo"></i> Отмена</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END INLINE TABS PORTLET-->
            </div>
        </div>

    </div>

</div>

{literal}
    <script type="text/javascript" src="/js/CValidation.js"></script>
    <script>
        var valid = new CValidation('ru');
        valid.ready(function(){
            //$('input[name="Users[phone]"]').mask("+999-99-999-99-99");
            $('.submitForm').click(function(event){
                event.preventDefault();
                valid.submitForm('#users_form');
            });
        });

        (function($){
            $(document).ready(function(){
                $('.generatePass').on('click',function(){
                    var generatedPass = str_rand();
                    $('.generatedPassValue').text(generatedPass);
                    $('input[name="Users[password]"]').val(generatedPass);
                    $('input[name="confirm_password"]').val(generatedPass);
                });

                $('.sendNewPassword').on('click',function(){
                    var id = $('input[name="Users[id]"]').val();
                    var $this = $(this);
                    $.ajax({
                        type:'POST',
                        url:'/admin/users/sendnewpass',
                        data:{id:id},
                        beforeSend:function(){
                            $this.removeClass('blue');
                            $this.addClass('purple');
                            $this.text('Отправка нового пароля');
                        },
                        dataType:'json',
                        success:function(data) {
                            if(data.status==200) {
                                $this.removeClass('purple');
                                $this.addClass('green');
                                $this.text('Пароль отправлен успешно');
                                setTimeout(function(){
                                    $this.removeClass('green');
                                    $this.addClass('blue');
                                    $this.text('Выслать новый пароль');
                                },4000);
                            }

                        }
                    });
                });

            });
            function str_rand() {
                var result       = '';
                var words        = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
                var max_position = words.length - 1;
                for( i = 0; i < 10; ++i ) {
                    position = Math.floor ( Math.random() * max_position );
                    result = result + words.substring(position, position + 1);
                }
                return result;
            }

        })(jQuery);

    </script>
{/literal}