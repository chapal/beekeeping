
<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Пользователи
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <i class="icon-user"></i>
                    <a href="/admin/users/index/page/1">Пользователи</a>
                </li>
            </ul>

        </div>
    </div>

    <div id="dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption" id="pulsate-regular"><i class="icon-list"></i>Фильтр</div>
                    </div>
                    <div class="portlet-body" id="gallery">

                        <form action="" id="filter" class="form-horizontal" style="margin: 10px 0px 10px 0px;" method="post">
                            <div class="control-group" style="padding-top: 0px; margin-bottom: 0px;">
                                <input type="hidden" name="Filter[model]" value="Users">
                                <input type="hidden" name="Filter[active]" value="">
                                <div class="row-fluid">

                                    <div class="span4">
                                        <label class="control-label" style="width: 80px;">Email:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <input type="text" name="Filter[email]" id="filter_email" value="{$filter.email}" class="m-wrap chosen" />
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <label class="control-label" style="width: 80px;">Сайт:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <input type="text" name="Filter[site]" id="filter_site" value="{$filter.site}" class="m-wrap chosen" />
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <label class="control-label" style="width: 80px;">Телефон:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <input type="text" name="Filter[phone]" id="filter_phone" value="{$filter.phone}" class="m-wrap chosen" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid" style="margin-top: 20px;">
                                    <div class="span4">
                                        <label class="control-label" style="width: 80px;">Имя:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <input type="text" name="Filter[first_name]" id="filter_first_name" value="{$filter.first_name}" class="m-wrap chosen" />
                                        </div>
                                    </div>

                                    <div class="span3" style="margin-left: 120px;">
                                        <a href="javascript:void(0);" class="btn green" onclick="filterOn();" style="padding: 6px 14px;" /><i class="icon-ok"></i> Включить</a>
                                        <a href="javascript:void(0);" class="btn yellow" onclick="filterOff();" style="padding: 6px 14px;" /><i class="icon-refresh"></i> Сбросить</a>
                                    </div>
                                </div>

                            </div>
                        </form>


                    </div>


                </div>


            </div>

        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Список пользователей</div>
                        <div class="actions">
                            <a href="/admin/users/add" class="btn blue"><i class="icon-plus"></i> Добавить</a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <table class="table table-striped table-bordered" style="margin-bottom: 0px;">
                            <thead>
                            <tr>
                                <th class="span1" style="text-align: center;">#</th>
                                <th>Имя</th>
                                <th class="hidden-480 span5" style="text-align: center;">Контакты</th>
                                <th class="hidden-480" style="text-align: center;">Сайт</th>
                                <th class="hidden-480" style="text-align: center;">Комментарий</th>
                                <th class="hidden-480" style="text-align: center;">Дата</th>
                                <th style="text-align: center;">Статус</th>
                                <th style="text-align: center;">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach from=$users item=item}
                                <tr>
                                    <td class="span1 userid" style="text-align: center; vertical-align: middle;">{$item.id}</td>
                                    <td style="vertical-align: middle;">
                                        {$item.first_name|stripslashes} {$item.last_name|stripslashes}
                                    </td>
                                    <td class="hidden-480" style="text-align: center; max-width: 150px; vertical-align: middle;">
                                        <table class="table table-bordered" style="margin-bottom: 0px;">
                                            <tbody>
                                            <tr>
                                                <td style="background: #f9f9f9;" class="span2">Email:</td>
                                                <td style="background: #fff;" class="useremail">{$item.email}</td>
                                            </tr>
                                            {if $item.work_email!=''}
                                                <tr style="background: #fff;">
                                                    <td style="background: #f9f9f9;" class="span2">Рабочий Email:</td>
                                                    <td style="background: #fff;">{$item.work_email}</td>
                                                </tr>
                                            {/if}
                                            {if $item.additional_email !=''}
                                                <tr style="background: #fff;">
                                                    <td style="background: #f9f9f9;" class="span2">Дополнительный Email:</td>
                                                    <td style="background: #fff;">{$item.additional_email}</td>
                                                </tr>
                                            {/if}
                                            {if $item.phone!=''}
                                            <tr style="background: #fff;">
                                                <td style="background: #f9f9f9;" class="span2">Телефон:</td>
                                                <td style="background: #fff;">{$item.phone}</td>
                                            </tr>
                                            {/if}
                                            {if $item.work_phone!=''}
                                                <tr style="background: #fff;">
                                                    <td style="background: #f9f9f9;" class="span2">Рабочий Телефон:</td>
                                                    <td style="background: #fff;">{$item.work_phone}</td>
                                                </tr>
                                            {/if}
                                            {if $item.additional_phone!=''}
                                                <tr style="background: #fff;">
                                                    <td style="background: #f9f9f9;" class="span2">Дополнительный Телефон:</td>
                                                    <td style="background: #fff;">{$item.additional_phone}</td>
                                                </tr>
                                            {/if}
                                            {if $item.skype!=''}
                                                <tr style="background: #fff;">
                                                    <td style="background: #f9f9f9;" class="span2">Skype:</td>
                                                    <td style="background: #fff;">{$item.skype}</td>
                                                </tr>
                                            {/if}
                                            </tbody>
                                        </table>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <a href="{$item.site|stripcslashes}" target="_blank">{$item.site|stripslashes}</a>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        {$item.comment|stripslashes}
                                    </td>
                                    <td class="hidden-480" style="text-align: center; max-width: 150px; vertical-align: middle;">
                                        {$item.creation_date|stripslashes}
                                    </td>
                                    <td style="text-align: center; max-width: 150px; vertical-align: middle;">
                                        {if $item.active==1}
                                            <a href="javascript:void(0);" onclick="changeUserStatus('{$item.id}');" id="status_link{$item.id}"><span style="font-weight:bold; color:#006600;">Активный</span></a>
                                        {else}
                                            <a href="javascript:void(0);" onclick="changeUserStatus('{$item.id}');" id="status_link{$item.id}"><span style="font-weight:bold; color:#660000;">Заблокирован</span></a>
                                        {/if}
                                    </td>
                                    <td class="span3" style="text-align: center; vertical-align: middle;">
                                        <a href="/admin/users/view/id/{$item.id}">Просмотр</a> |
                                        <a href="/admin/users/edit/id/{$item.id}">Изменить</a> |
                                        <a href="javascript:void(0);"  onclick="return customConfirmBox('Вы уверены что хотите удалить этого пользователя?','/admin/users/delete/id/{$item.id}')">Удалить</a>
                                            <br/>
                                        <a href="javascript:void(0);" class="btn mini grey createBill"><i class="icon-dollar"></i> Выставить счет</a>
                                    </td>
                                </tr>
                                {foreachelse}
                                <tr>
                                    <td colspan="6" style="text-align: center;">Нет ни одного пользователя...</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        {include file='admin/users/paging.tpl'}

                    </div>


                </div>



            </div>

        </div>


    </div>

</div>

{literal}
<script type="text/javascript">
    (function($){
        $(document).ready(function(){
           $('.createBill').click(function(event){
               event.preventDefault();
               var username = $(this).parents('tr').find('td:eq(1)').text();
               var useremail = $(this).parents('tr').find('.useremail').text();
               var userid = $(this).parents('tr').find('.userid').text();
                customLoaderBox('Подождите пожалуйста...');
               $.ajax({
                   type: "POST",
                   url: "/admin/bills/createBill",
                   dataTyep:'html',
                   data:{useremail:useremail,username:username,userid:userid},
                   success: function(response){
                        $('#customConfirm').css({'top':'0','position':'absolute'}).html(response);

                   }
               });
           });
        });
    })(jQuery);
function changeUserStatus(user_id){
	$.post("/admin/users/change-user-status", {id:user_id},
		function(data) {
   			if(data==1){
				$("#status_link"+user_id).html("<span style='font-weight:bold; color:#006600;'>Активный</span>");
   			} else {
   				$("#status_link"+user_id).html("<span style='font-weight:bold; color:#660000;'>Заблокирован</span>");
   			}
		}
	);	
}

    function filterOn(){
        $('#filter input[name="Filter[active]"]').val(1);
        $("#filter").submit();
    }

    function filterOff(){
        $('#filter input[name="Filter[active]"]').val(0);
        $("#filter").submit();
    }
</script>
{/literal}