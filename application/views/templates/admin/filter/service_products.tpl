
<div class="span3 type_service for_services">
    <label class="control-label" style="width: 80px;">Продукты:</label>
    <div class="controls serviceProducts" style="margin-left: 90px;">
        <select name="FilterOrders[product_id]" class="m-wrap chosen">
            <option value="0">----------------------</option>
            {foreach from=$serviceItems key=key item=item}
                <option value="{$item.productid}" {if $filter_orders.product_id==$item.productid} selected="selected" {/if} >{$item.title}</option>
            {/foreach}
        </select>
    </div>
</div>