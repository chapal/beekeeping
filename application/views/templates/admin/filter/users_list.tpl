<table class="table table-striped table-bordered" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="span1" style="text-align: center;">#</th>
        <th>Имя</th>
        <th class="hidden-480 span5" style="text-align: center;">Контакты</th>
        <th class="hidden-480" style="text-align: center;">Сайт</th>
        <th class="hidden-480" style="text-align: center;">Комментарий</th>
        <th class="hidden-480" style="text-align: center;">Дата</th>
        <th style="text-align: center;">Статус</th>
        <th style="text-align: center;">Количество оплат</th>
        <th style="text-align: center;">Действия</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$users item=item}
        <tr>
            <td class="span1 userid" style="text-align: center; vertical-align: middle;">{$item.id}</td>
            <td style="vertical-align: middle;">
                {$item.first_name|stripslashes} {$item.last_name|stripslashes}
            </td>
            <td class="hidden-480" style="text-align: center; max-width: 150px; vertical-align: middle;">
                <table class="table table-bordered" style="margin-bottom: 0px;">
                    <tbody>
                    <tr>
                        <td style="background: #f9f9f9;" class="span2">Email:</td>
                        <td style="background: #fff;" class="useremail">{$item.email}</td>
                    </tr>
                    {if $item.work_email!=''}
                        <tr style="background: #fff;">
                            <td style="background: #f9f9f9;" class="span2">Рабочий Email:</td>
                            <td style="background: #fff;">{$item.work_email}</td>
                        </tr>
                    {/if}
                    {if $item.additional_email !=''}
                        <tr style="background: #fff;">
                            <td style="background: #f9f9f9;" class="span2">Дополнительный Email:</td>
                            <td style="background: #fff;">{$item.additional_email}</td>
                        </tr>
                    {/if}
                    {if $item.phone!=''}
                        <tr style="background: #fff;">
                            <td style="background: #f9f9f9;" class="span2">Телефон:</td>
                            <td style="background: #fff;">{$item.phone}</td>
                        </tr>
                    {/if}
                    {if $item.work_phone!=''}
                        <tr style="background: #fff;">
                            <td style="background: #f9f9f9;" class="span2">Рабочий Телефон:</td>
                            <td style="background: #fff;">{$item.work_phone}</td>
                        </tr>
                    {/if}
                    {if $item.additional_phone!=''}
                        <tr style="background: #fff;">
                            <td style="background: #f9f9f9;" class="span2">Дополнительный Телефон:</td>
                            <td style="background: #fff;">{$item.additional_phone}</td>
                        </tr>
                    {/if}
                    {if $item.skype!=''}
                        <tr style="background: #fff;">
                            <td style="background: #f9f9f9;" class="span2">Skype:</td>
                            <td style="background: #fff;">{$item.skype}</td>
                        </tr>
                    {/if}
                    </tbody>
                </table>
            </td>
            <td style="vertical-align: middle;">
                <a href="{$item.site|stripcslashes}" target="_blank">{$item.site|stripslashes}</a>
            </td>
            <td style="vertical-align: middle;">
                {$item.comment|stripslashes}
            </td>
            <td class="hidden-480" style="text-align: center; max-width: 150px; vertical-align: middle;">
                {$item.creation_date|stripslashes}
            </td>
            <td style="text-align: center; max-width: 150px; vertical-align: middle;">
                {if $item.active==1}
                    <a href="javascript:void(0);" onclick="changeUserStatus('{$item.id}');" id="status_link{$item.id}"><span style="font-weight:bold; color:#006600;">Активный</span></a>
                {else}
                    <a href="javascript:void(0);" onclick="changeUserStatus('{$item.id}');" id="status_link{$item.id}"><span style="font-weight:bold; color:#660000;">Заблокирован</span></a>
                {/if}
            </td>
            <td class="hidden-480" style="text-align: center; max-width: 150px; vertical-align: middle;">
                {$item.num|stripslashes}
            </td>
            <td class="span3" style="text-align: center; vertical-align: middle;">
                <a href="/admin/users/view/id/{$item.id}">Просмотр</a> |
                <a href="/admin/users/edit/id/{$item.id}">Изменить</a> |
                <a href="javascript:void(0);"  onclick="return customConfirmBox('Вы уверены что хотите удалить этого пользователя?','/admin/users/delete/id/{$item.id}')">Удалить</a>
                <br/>
            </td>
        </tr>
        {foreachelse}
        <tr>
            <td colspan="6" style="text-align: center;">Нет ни одного пользователя...</td>
        </tr>
    {/foreach}
    </tbody>
</table>

{literal}
    <script>
        function changeUserStatus(user_id){
            $.post("/admin/users/change-user-status", {id:user_id},
                    function(data) {
                        if(data==1){
                            $("#status_link"+user_id).html("<span style='font-weight:bold; color:#006600;'>Активный</span>");
                        } else {
                            $("#status_link"+user_id).html("<span style='font-weight:bold; color:#660000;'>Заблокирован</span>");
                        }
                    }
            );
        }
    </script>
{/literal}