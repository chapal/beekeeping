<div class="container-fluid" xmlns="http://www.w3.org/1999/html">
    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Фильтр
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li><a href="/admin/services/index/page/1">Фильтр</a></li>
            </ul>

        </div>
    </div>

    <div id="dashboard" style="overflow: visible">
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Фильтр покупок</div>

                    </div>
                    <div class="portlet-body" id="gallery">
                        <form action="" id="filterOrders" class="form-horizontal" style="margin: 10px 0px 10px 0px;" method="post">
                            <div class="control-group" style="padding-top: 0px; margin-bottom: 0px;">
                                <div class="row-fluid">
                                    <div class="span3 checkFilterType">
                                        <label class="control-label" style="width: 80px;">Тип:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <select name="FilterOrders[type]"  class="m-wrap chosen ">
                                                <option value="0"> ------ </option>
                                                <option value="product" {if $filter_orders.type=='product'} selected="selected" {/if}> Продукт </option>
                                                <option value="service" {if $filter_orders.type=='service'} selected="selected" {/if}> Сервис </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="span3 type_product" {if $filter_orders != false && $filter_orders.type=='product'} {else}style="display: none;"{/if}>
                                        <label class="control-label" style="width: 80px;">Продукты:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <select name="FilterOrders[product_id]" class="m-wrap chosen">
                                                <option value=""> ------ </option>
                                                {foreach from=$productsItems key=key item=item}
                                                    <option value="{$item.id}" {if $filter_orders.product_id==$item.id} selected="selected" {/if}>{$item.title}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="span3 type_service serviceSelect {if $filter_orders != false && $filter_orders.type=='service'} triggerService {/if}" {if $filter_orders != false && $filter_orders.type=='service'} {else}style="display: none;"{/if}>
                                        <label class="control-label" style="width: 80px;">Сервисы:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <select name="FilterOrders[service_id]"  class="m-wrap chosen">
                                                <option value=""> ------ </option>
                                                {foreach from=$servicesItems key=key item=item}
                                                    <option value="{$item.id}" {if $filter_orders.service_id==$item.id} selected="selected" {/if} >{$item.name}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn green"><i class="icon-search"></i> Найти</button>
                                </div>


                            </div>
                        </form>



                    </div>


                </div>

            </div>

        </div>
        <div class="row-fluid">
            {if $filter_orders != false && $userTemplate == ''}
                Пользователей не найдено....
            {else}
                {$userTemplate}
            {/if}
        </div>

    </div>


</div>
{literal}
    <script>
        (function($){
            $(document).ready(function() {

                if($('.triggerService').length > 0) {
                   setTimeout(function(){
                       $('.serviceSelect select').trigger('change');
                   },300);


                }

                var previousType;
                $('.checkFilterType select').on('change',function(){
                    if($(this).val()===previousType) {
                        return true;
                    }
                    previousType=$(this).val();

                    $('form').find('div[class^="span"]').not('.checkFilterType').hide();
                    $('form').find('div[class^="span"]').not('.checkFilterType').find('input,select').attr('disabled','disabled');

                    $('form').find('.type_'+$(this).val()).show();
                    $('form').find('.type_'+$(this).val()).find('select,input').removeAttr('disabled');
                }); // end change select

                $('.serviceSelect select').on('change',function(){
                    $.ajax({
                        type:'POST',
                        url:'/admin/filter/checkservice',
                        dataType:'html',
                        data:{serviceId:$(this).val()},
                        beforeSend:function(){
                            $('.for_services').remove();
                        },
                        success:function(response){
                           $('.serviceSelect').after(response);
                        }

                    });
                });
            });
        })(jQuery);
    </script>
{/literal}