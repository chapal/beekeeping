<table width="100%">
<tr>
	<td align="center">
		<span style="font-size:14px;">{$section.title|stripslashes}</span>
	</td>
</tr>
<tr>
	<td>
		<div style="overflow:auto; height:270px;">
			{$section.description|stripslashes|strip_tags}
		</div>
	</td>
</tr>
</table>