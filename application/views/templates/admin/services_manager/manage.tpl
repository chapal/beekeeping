<div class="container-fluid" xmlns="http://www.w3.org/1999/html">
    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Управление сервисами
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li><a href="/admin/services/index/page/1">Управление сервисами</a></li>
            </ul>

        </div>
    </div>

    <div id="dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Список</div>
                        <div class="actions">
                            <a href="/admin/servicesmanager/add" class="btn blue"><i class="icon-plus"></i> Добавить</a>
                        </div>
                    </div>
                    <div class="portlet-body" id="gallery">

                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
                            <thead>
                            <tr>
                                <th class="span1" style="text-align: center;">#</th>
                                <th class="span3">Сервис</th>
                                <th class="span3">Товар</th>
                                <th class="span3">Стоимость</th>
                                <th class="span3">Период</th>
                                <th class="span3">Тип периода</th>
                                <th style="text-align: center;">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach from=$items item=item}
                                <tr>

                                    <td class="span1" style="text-align: center; vertical-align: middle;">{$item.id}</td>
                                    <td style="vertical-align: middle;" class="hidden-480">
                                        {if $item.servicename!=''}
                                            <a href="/admin/services/edit/id/{$item.id}" target="_blank"><span class="green">{$item.servicename|stripcslashes}</span></a>
                                        {else}
                                            <span style="color: red;">Не зарегестрирован</span>
                                        {/if}

                                    </td>
                                    <td class="span2" style="text-align: center; vertical-align: middle;">
                                        {$item.productname|stripcslashes}
                                    </td>
                                    <td class="span2" style="text-align: center; vertical-align: middle;">
                                        {$item.price}
                                    </td>
                                    <td class="span2" style="text-align: center; vertical-align: middle;">
                                        {$item.period}
                                    </td>
                                    <td class="span2" style="text-align: center; vertical-align: middle;">
                                        {if $item.period_type =='days'}Дни
                                        {elseif $item.period_type =='months'}Месяцы
                                        {elseif $item.period_type =='years'}Года
                                        {/if}

                                    </td>

                                    <td class="span4" style="text-align: center; vertical-align: middle;">
                                        <a href="/admin/servicesmanager/edit/id/{$item.id}" title="Посмотреть">Изменить</a>
                                        &nbsp;|&nbsp;
                                        <a href="/admin/servicesmanager/delete/id/{$item.id}" onclick="return confirm('Вы уверены, что хотите удалить этот сервис?')" title="Удалить">Удалить</a>

                                    </td>
                                </tr>
                                {foreachelse}
                                <tr>
                                    <td colspan="9" style="text-align: center;">Нет ни одного сервиса...</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        {include file='admin/services_manager/paging.tpl'}

                    </div>


                </div>

            </div>

        </div>

    </div>


</div>
