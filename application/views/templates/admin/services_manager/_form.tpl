<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                {if $action == 'edit'}Изменение{else}Добавление{/if} сервиса к товару<small>&nbsp;</small>
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="/admin/services/index/page/1">Управление сервисами</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:void(0);">{if $action == 'edit'}Изменение{else}Добавление{/if} сервиса к товару</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="dashboard">

        <div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption">{if $action == 'edit'}<i class="icon-pencil"></i> Изменение{else}<i class="icon-plus"></i> Добавление{/if} сервиса к товару</div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
                            <div class="span12">
                                <form method="POST" action="/admin/servicesmanager/{$action}" name="services_form" id="services_form" enctype="multipart/form-data">
                                    {if $action == 'edit'}
                                        <input type="hidden" name="ServicesManager[id]" value="{$item.id}">
                                    {/if}

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Выберите сервис:</label>
                                            <div class="controls">
                                                <select data-cvalidation="required" name="ServicesManager[service_id]" class="span6">
                                                    {foreach from=$services key=key item=service}
                                                        <option value="{$service.id}" {if $action == 'edit'}{if $item.service_id==$service.id}selected="selected"{/if}{/if}>{$service.name|stripcslashes}</option>
                                                    {/foreach}

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Выберите продукт:</label>
                                            <div class="controls">
                                                <select data-cvalidation="required" name="ServicesManager[product_id]" class="span6">
                                                    <option>---Не привязывать к продукту---</option>
                                                    {foreach from=$products key=key item=product}
                                                        <option value="{$product.id}" {if $action == 'edit'}{if $item.product_id==$product.id}selected="selected"{/if}{/if} >{$product.title|stripcslashes}</option>
                                                    {/foreach}

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Стоимость:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="toBeInt" data-cvalidation-fieldname="Стоимость" name="ServicesManager[price]"  value="{$item.price|stripslashes|trim}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Период:Каждые</label>
                                            <div class="controls">
                                                <input type="text"  name="ServicesManager[period]"  value="{if $action == 'add'}3{/if}{$item.period|stripslashes|trim}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Измерение:</label>
                                            <div class="controls">
                                                <select data-cvalidation="required" name="ServicesManager[period_type]" class="span6">
                                                        <option value="days" {if $action == 'edit'}{if $item.period_type=='days'}selected="selected"{/if}{/if}>Дни</option>
                                                        <option value="months" {if $action == 'edit'}{if $item.period_type=='months'}selected="selected"{/if}{/if}{if $action == 'add'}selected="selected"{/if} >Месяцы</option>
                                                        <option value="years" {if $action == 'edit'}{if $item.period_type=='years'}selected="selected"{/if}{/if}>Года</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions" style="padding-left: 20px;">
                                        <button type="submit" class="btn blue submitForm"><i class="icon-ok"></i>Сохранить</button>
                                        <button type="button" class="btn" onclick="document.location.href='/admin/servicesmanager/index/page/1'"><i class="icon-undo"></i> Отмена</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END INLINE TABS PORTLET-->
            </div>
        </div>

    </div>

</div>

{literal}
    <script type="text/javascript" src="/js/CValidation.js"></script>
    <script>
        var valid = new CValidation('ru','listError');
        valid.ready(function(){
            $('.submitForm').click(function(event){
                event.preventDefault();
                valid.submitForm('#services_form');
            });
        });

    </script>
{/literal}