<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Опции товар <small>&nbsp;</small>
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="/admin/options/index/page/1">Опции товар</a>
                    <i class="icon-angle-right"></i>
                </li>

                <li>
                    <a href="/admin/options/properties/option_id/{$option_id}/spage/1/page/1">Значения опции</a>
                    <i class="icon-angle-right"></i>
                </li>

                <li>
                    <a href="javascript:void(0);">{if $action == 'modify-property'}Изменение{else}Добавление{/if} значения</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="dashboard">

        <div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption">{if $action == 'modify-property'}<i class="icon-pencil"></i> Изменение{else}<i class="icon-plus"></i> Добавление{/if} значения</div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
                            <div class="span12">
                                <form method="POST" action="/admin/options/{$action}" name="property_form" enctype="multipart/form-data">
                                    <input type="hidden" name="step" value="2">
                                    {if $item.id}
                                        <input type="hidden" name="id" value="{$item.id}">
                                    {/if}

                                    <div style="padding-left: 10px; padding-top: 10px;">


                                        <div class="control-group">
                                            <label class="control-label">Заголовок:</label>
                                            <div class="controls">
                                                <input type="text" name="title" id="title" value="{$item.title|stripslashes}" class="span6 m-wrap" onchange="setLink();" onkeyup="setLink();" onkeypress="setLink();" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Ссылка:</label>
                                            <div class="controls">
                                                <input type="text" name="link" id="link" readonly="readonly" value="{$item.link}" class="span6 m-wrap" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Полное описание:</label>
                                            <div class="controls">
                                                <textarea name="description" class="ckeditor">{$item.description|stripslashes}</textarea>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">{$adminLangParams.TITLE_POSITION}:</label>
                                            <div class="controls">
                                                <input type="text" id="position" class="span1 m-wrap" maxlength="3" name="position" value="{$item.position|stripslashes}">
                                            </div>
                                        </div>


                                        {include file='admin/options/meta.tpl'}

                                    </div>
                                    <div class="form-actions" style="padding-left: 20px;">
                                        <input type="hidden" name="price" value="0" />
                                        <input type="hidden" name="option_id" value="{$option_id}" />
                                        <input type="hidden" name="spage" value="{$spage}" />
                                        <input type="hidden" name="page" value="{$page}" />
                                        <button type="button" class="btn blue" {if $action!='modify-property'} onclick="checkForm('')" {else} onclick="checkForm('modify')" {/if}><i class="icon-ok"></i> Сохранить</button>
                                        <button type="button" class="btn" onclick="document.location.href='/admin/options/properties/option_id/{$option_id}/spage/1/page/1'"><i class="icon-undo"></i> Отмена</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END INLINE TABS PORTLET-->
            </div>
        </div>

    </div>

</div>


{literal}

    <script type="text/javascript">

        function checkForm(){
            if ($("#title").val() == '') {
                alert('Вы должны заподнить поле заголовок...');
            } else if ($("#position").val() == '') {
                alert('Вы должны заподнить поле позиция...');
            } else {
                setLink();
                document.forms.property_form.submit();
            }
        }

        function setLink(){
            var link = createLinkFromTitle($("#title").val());
            $("#link").val(link);
        }

    </script>
{/literal}