<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Опции товара
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li><a href="/admin/options/index/page/1">Опции товара</a></li>
            </ul>

        </div>
    </div>

    <div id="dashboard">

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Список опций</div>
                        <div class="actions">
                            <a href="/admin/options/add-option" class="btn blue"><i class="icon-plus"></i> Добавить</a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
                            <thead>
                            <tr>
                                <th class="span1" style="text-align: center;">#</th>
                                <th class="span4">Заголовок</th>
                                <th class="hidden-480" style="text-align: center;">Значения</th>
                                <th class="hidden-480" style="text-align: center;">Число значений</th>
                                 <th style="text-align: center;">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach from=$options item=item}
                                <tr>
                                    <td class="span1" style="text-align: center; vertical-align: middle;">{$item.id}</td>
                                    <td style="vertical-align: middle;">
                                        {$item.title|stripslashes}
                                    </td>
                                    <td align="center" style="padding:5px 5px 5px 5px; width:150px; text-align: center;"><a href="/admin/options/properties/option_id/{$item.id}/spage/{$page}/page/0">{$adminLangParams.ACTION_EDIT}</a></td>
                                    <td style="padding:5px 5px 5px 5px; width:120px; text-align: center; {if $item.count>0}font-weight: bold;{/if}">{$item.count}</td>
                                    <td class="span3" style="text-align: center; vertical-align: middle;">
                                    <a href="/admin/options/modify-option/id/{$item.id}">{$adminLangParams.ACTION_EDIT}</a>
                                    &nbsp;|&nbsp;
                                    <a href="/admin/options/delete-option/id/{$item.id}" onclick="return confirm('Вы уверены, что хотите удалить эту запись?')">{$adminLangParams.ACTION_DELETE}</a>
                                    </td>
                                </tr>
                                {foreachelse}
                                <tr>
                                    <td colspan="5" style="text-align: center;">Ни одной записи не найдено...</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        {include file='admin/options/options/paging.tpl'}

                    </div>


                </div>


            </div>

        </div>

    </div>

</div>