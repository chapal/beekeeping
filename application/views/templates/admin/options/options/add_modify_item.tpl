<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Опции товар <small>&nbsp;</small>
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="/admin/options/index/page/1">Опции товар</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:void(0);">{if $action == 'modify-option'}Изменение{else}Добавление{/if} опции</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="dashboard">

        <div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption">{if $action == 'modify-option'}<i class="icon-pencil"></i> Изменение{else}<i class="icon-plus"></i> Добавление{/if} опции</div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
                            <div class="span12">
                                <form method="POST" action="/admin/options/{$action}" name="option_form" id="option_form" enctype="multipart/form-data">
                                    <input type="hidden" name="step" value="2">
                                    {if $item.id}
                                        <input type="hidden" name="id" value="{$item.id}">
                                    {/if}

                                    <div style="padding-left: 10px; padding-top: 10px;">


                                        <div class="control-group">
                                            <label class="control-label">Заголовок:</label>
                                            <div class="controls">
                                                <input type="text" name="title" id="title" value="{$item.title|stripslashes}" class="span6 m-wrap" onchange="setLink();" onkeyup="setLink();" onkeypress="setLink();" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Ссылка:</label>
                                            <div class="controls">
                                                <input type="text" name="link" id="link" readonly="readonly" value="{$item.link}" class="span6 m-wrap" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Полное описание:</label>
                                            <div class="controls">
                                                <textarea name="description" class="ckeditor">{$item.description|stripslashes}</textarea>
                                            </div>
                                        </div>

                                        {include file='admin/options/meta.tpl'}

                                    </div>
                                    <div class="form-actions" style="padding-left: 20px;">
                                        <button type="button" class="btn blue" {if $action!='modify-option'} onclick="checkForm('')" {else} onclick="checkForm('modify')" {/if}><i class="icon-ok"></i> Сохранить</button>
                                        <button type="button" class="btn" onclick="document.location.href='/admin/options/index/page/1'"><i class="icon-undo"></i> Отмена</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END INLINE TABS PORTLET-->
            </div>
        </div>

    </div>

</div>


<script>
    {literal}
    function setLink(){
        var link = createLinkFromTitle($("#title").val());
        $("#link").val(link+'.html');
    }

    function checkForm(type){
        if(type==''){
            if ($("#title").val() == '') {
                alert('Заполните поле "Заголовок"');
            } else {
                $("#option_form").submit();
            }
        } else {
            if ($("#title").val() == '') {
                alert('Заполните поле "Заголовок"');
            } else {
                $("#option_form").submit();
            }
        }
    }

    {/literal}
</script>