<div class="form-horizontal" id="optionsContainer">
{foreach from=$options item=option}
    <div class="control-group">
        <label class="control-label span2" style="margin-right: 10px;">{$option.title|stripslashes|strip_tags}:</label>
        <div class="controls">
            <select name="option[]">
                <option value="0">&nbsp;--- выберите вариант ---</option>
                {foreach from=$option.properties item=property}
                    <option value="{$option.id}-{$property.id}" {if $property.selected==1} selected="selected"{/if}>{$property.title|stripslashes|strip_tags}</option>
                {/foreach}
            </select>
        </div>
    </div>
{/foreach}
</div>

