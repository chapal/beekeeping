<div class="container-fluid" xmlns="http://www.w3.org/1999/html">
    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Компании
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li><a href="/admin/orders/index/page/1">Компании</a></li>
            </ul>

        </div>
    </div>

    <div id="dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Список компаний</div>
                        <div class="actions">
                            <a href="/admin/companies/add" class="btn blue"><i class="icon-plus"></i> Добавить</a>
                        </div>
                    </div>
                    <div class="portlet-body" id="gallery">

                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
                            <thead>
                            <tr>

                                <th class="span1" style="text-align: center;">#</th>
                                <th class="hidden-480">Наименование</th>
                                <th>Юридический адрес</th>
                                <th class="hidden-480" style="text-align: center;">Почтовый адрес</th>
                                <th class="span2" style="text-align: center;">Р.сч.№</th>
                                <th class="hidden-480 span1" style="text-align: center;">Банк получателя</th>
                                <th class="hidden-480 span1" style="text-align: center;">Должность</th>
                                <th class="hidden-480 span2" style="text-align: center;">ФИО</th>
                                <th style="text-align: center;">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach from=$companies item=item}
                                <tr>


                                    <td class="span1" style="text-align: center; vertical-align: middle;">{$item.id}</td>
                                    <td style="vertical-align: middle;" class="hidden-480">
                                        {if $item.name!=''}
                                            <a href="/admin/companies/view/id/{$item.id}" target="_blank"><span class="green">{$item.name|stripcslashes}</span></a>
                                        {else}
                                            <span style="color: red;">Не зарегестрирован</span>
                                        {/if}

                                    </td>

                                    <td class="span2" style="text-align: center; vertical-align: middle;">
                                        {$item.legal_address|stripcslashes}
                                    </td>
                                    <td class="hidden-480 span2" style="text-align: center; vertical-align: middle;">
                                        {$item.postal_address|stripcslashes}
                                    </td>
                                    <td class="span2" style="text-align: center; vertical-align: middle;">
                                        {$item.account}
                                    </td>
                                    <td class="span3" style="text-align: left; vertical-align: middle;">
                                        {foreach from=$item.payee_bank key=currency item=payee_bank}
                                            <p style="outline: 1px dashed #666;padding: 10px;">{$currency|stripcslashes}:  {$payee_bank|stripcslashes}</p>

                                        {/foreach}
                                    </td>
                                    <td class="span2" style="text-align: center; vertical-align: middle;">
                                        {$item.position}
                                    </td>
                                    <td class="span2" style="text-align: center; vertical-align: middle;">
                                        {$item.fio}
                                    </td>

                                    <td class="span4" style="text-align: center; vertical-align: middle;">
                                        <a href="/admin/companies/view/id/{$item.id}" title="Посмотреть">Посмотреть</a>
                                        &nbsp;|&nbsp;
                                        <a href="/admin/companies/edit/id/{$item.id}" title="Посмотреть">Изменить</a>
                                        &nbsp;|&nbsp;
                                        <a href="/admin/companies/delete/id/{$item.id}" onclick="return confirm('Вы уверены, что хотите удалить эту компанию?')" title="Удалить">Удалить</a>

                                    </td>
                                </tr>
                                {foreachelse}
                                <tr>
                                    <td colspan="9" style="text-align: center;">Нет ни одной компании...</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        {include file='admin/companies/paging.tpl'}

                    </div>


                </div>

            </div>

        </div>

    </div>


</div>
