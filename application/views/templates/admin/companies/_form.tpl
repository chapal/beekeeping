<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                {if $action == 'edit'}Изменение{else}Добавление{/if} компании <small>&nbsp;</small>
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="/admin/companies/index/page/1">Компании</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:void(0);">{if $action == 'edit'}Изменение{else}Добавление{/if} компании</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="dashboard">

        <div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption">{if $action == 'edit'}<i class="icon-pencil"></i> Изменение{else}<i class="icon-plus"></i> Добавление{/if} компании</div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
                            <div class="span12">
                                <form method="POST" action="/admin/companies/{$action}" name="companies_form" id="companies_form" enctype="multipart/form-data">
                                    {if $action == 'edit'}
                                        <input type="hidden" name="Companies[id]" value="{$item.id}">
                                    {/if}

                                    <div style="padding-left: 10px; padding-top: 10px;">

                                        <div class="control-group">
                                            <label class="control-label">Наименование:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="" data-cvalidation-fieldname="Наименование" name="Companies[name]" id="name" value="{$item.name|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Юридический адрес:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="" data-cvalidation-fieldname="Юридически адрес" name="Companies[legal_address]" id="legal_address" value="{$item.legal_address|stripslashes}" class="span6 m-wrap"  />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Почтовый адрес:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="" data-cvalidation-fieldname="Почтовый адрес" name="Companies[postal_address]" id="postal_address" value="{$item.postal_address|stripslashes}" class="span6 m-wrap"  />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Рассчетный счет:</label>
                                            <div class="controls">
                                                <input type="text"  data-cvalidation="validname" data-cvalidation-fieldname="Рассчетный счет" name="Companies[account]" id="account" value="{$item.account|stripslashes}" class="span6 m-wrap"  />
                                            </div>
                                        </div>



                                        <div class="control-group">
                                            <label class="control-label">Банк получателя:</label>
                                            {if $action == 'edit'}
                                                {foreach from=$item.payee_bank key=currency item=payee_bank}
                                                    <select name="Companies[payee_bank_currency][]" class="span6">
                                                        <option value="BYR" {if $currency == "BYR"} selected="selected"{/if}>BYR</option>
                                                        <option value="RUR" {if $currency == "RUR"} selected="selected"{/if}>RUR</option>
                                                        <option value="USD" {if $currency == "USD"} selected="selected"{/if}>USD</option>
                                                        <option value="EUR" {if $currency == "EUR"} selected="selected"{/if}>EUR</option>
                                                    </select>
                                                    <div class="controls">
                                                        <textarea name="Companies[payee_banks][]" data-cvalidation="required" data-cvalidation-fieldname="Банк получателя" class="span6 m-wrap">{$payee_bank|stripslashes}</textarea>
                                                    </div>
                                                {/foreach}

                                            {/if}
                                            {if $action == 'add'}
                                            <select name="Companies[payee_bank_currency][]" class="span6">
                                                <option value="BYR">BYR</option>
                                                <option value="RUR">RUR</option>
                                                <option value="USD">USD</option>
                                                <option value="EUR">EUR</option>
                                            </select>
                                            <div class="controls">
                                                <textarea name="Companies[payee_banks][]" data-cvalidation="required" data-cvalidation-fieldname="Банк получателя" class="span6 m-wrap">{$item.payee_bank|stripslashes}</textarea>
                                            </div>
                                            <button type="button" class="btn blue mini addPayeeBank">Добавить банк получателя</button>
                                            {/if}
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Регион:</label>
                                            <div class="controls">
                                                <select name="Companies[region]" class="span6">
                                                    <option  {if $action == 'edit'} {if $item.region=='russia'}selected="selected"{/if} {else}selected="selected"{/if} value="russia">Для России</option>
                                                    <option {if $action == 'edit'} {if $item.region=='belorussia'}selected="selected"{/if} {/if} value="belorussia" >Для Белоруси</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="russia for-region" {if $action == 'edit'} {if $item.region=='belorussia'}style="display: none;"{/if} {/if}>
                                            <div class="control-group">
                                                <label class="control-label">ОГРН:</label>
                                                <div class="controls">
                                                    <input type="text" data-cvalidation="" data-cvalidation-fieldname="ФИО" name="Companies[ogrn]" value="{$item.ogrn|stripslashes}" class="span6 m-wrap" onchange="setLink();"  />
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">ИНН:</label>
                                                <div class="controls">
                                                    <input type="text" data-cvalidation="" data-cvalidation-fieldname="ФИО" name="Companies[inn]" value="{$item.inn|stripslashes}" class="span6 m-wrap" onchange="setLink();"  />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">КПП:</label>
                                                <div class="controls">
                                                    <input type="text" data-cvalidation="" data-cvalidation-fieldname="ФИО" name="Companies[kpp]"  value="{$item.kpp|stripslashes}" class="span6 m-wrap" onchange="setLink();"  />
                                                </div>
                                            </div>

                                        </div>

                                        <div class="belorussia for-region" {if $action == 'edit'} {if $item.region=='russia'}style="display: none;"{/if} {else}style="display: none;" {/if}>
                                            <div class="control-group">
                                                <label class="control-label">ОКПО:</label>
                                                <div class="controls">
                                                    <input type="text" data-cvalidation="" data-cvalidation-fieldname="ФИО" name="Companies[okpo]"  value="{$item.okpo|stripslashes}" class="span6 m-wrap"   />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">УНП:</label>
                                                <div class="controls">
                                                    <input type="text" data-cvalidation="" data-cvalidation-fieldname="ФИО" name="Companies[unp]"  value="{$item.unp|stripslashes}" class="span6 m-wrap"   />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="control-group">
                                            <label class="control-label">Должность:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="" data-cvalidation-fieldname="Должность" name="Companies[position]" id="position" value="{$item.position|stripslashes}" class="span6 m-wrap"  />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">ФИО:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="" data-cvalidation-fieldname="ФИО" name="Companies[fio]" id="fio" value="{$item.fio|stripslashes}" class="span6 m-wrap"  />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Загрузить логотип компании:</label>
                                            <div class="controls">
                                                <div class="fileupload fileupload-new" data-provides="fileupload" style="margin-bottom: 0px;">
                                                    <div class="input-append" style="margin-bottom: 0px;">
                                                        {if $action == 'edit'}
                                                               <img src="/tmp/companies/{$item.logo}" width="100" style="margin-right: 10px" />
                                                        {/if}
                                                        <div class="uneditable-input" style="margin-right: 5px;{if $action == 'edit'}display: none;{/if}">
                                                            <i class="icon-file fileupload-exists"></i>
                                                            <span class="fileupload-preview"></span>
                                                        </div>
                                                        <span class="btn btn-success btn-file">
                                                            <span class="fileupload-new"><i class="icon-picture"></i> {if $action == 'edit'} Изменить картинку {else} Выберите картинку {/if}</span>
                                                            <span class="fileupload-exists"><i class="icon-picture"></i> {if $action == 'edit'} Изменить картинку {else} Выберите картинку {/if}</span>
                                                            <input type="file" class="default" id="upload_id" name="Companies[logo]" />
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Загрузить изображение печати:</label>
                                            <div class="controls">

                                                <div class="fileupload fileupload-new" data-provides="fileupload" style="margin-bottom: 0px;">
                                                    <div class="input-append" style="margin-bottom: 0px;">
                                                        {if $action == 'edit'}
                                                                <img src="/tmp/companies/{$item.print}" style="margin-right: 10px;width: 100px; min-width: 80px;" />
                                                        {/if}
                                                        <div class="uneditable-input" style="margin-right: 5px; {if $action == 'edit'}display: none;{/if}">
                                                            <i class="icon-file fileupload-exists"></i>
                                                            <span class="fileupload-preview"></span>
                                                        </div>

                                                        <span class="btn btn-success btn-file">
                                                            <span class="fileupload-new"><i class="icon-picture"></i>{if $action == 'edit'} Изменить изображение {else} Выберите изображение {/if}</span>
                                                            <span class="fileupload-exists"><i class="icon-picture"></i> {if $action == 'edit'} Изменить изображение {else} Выберите изображение {/if}</span>
                                                            <input type="file" class="default" id="upload_id" name="Companies[print]" />
                                                        </span>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-actions" style="padding-left: 20px;">
                                        <button type="button" class="btn blue submitForm"><i class="icon-ok"></i> Сохранить</button>
                                        <button type="button" class="btn" onclick="document.location.href='/admin/companies/index/page/1'"><i class="icon-undo"></i> Отмена</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END INLINE TABS PORTLET-->
            </div>
        </div>

    </div>

</div>

{literal}
    <script type="text/javascript" src="/js/CValidation.js"></script>
    <script>
        var valid = new CValidation('ru','listError');
        valid.ready(function(){
            $('.submitForm').click(function(event){
                valid.submitForm('#companies_form',event);
            });
        });

        function initEvents()
        {
            $('.deletePayeeBank').off('click');
            $('.deletePayeeBank').on('click',function(){
               $(this).parent().prev().remove();
               $(this).parent().remove();

            });
        }
        (function($){
            $(document).ready(function(){
                $('select[name="Companies[region]"]').change(function(){
                    $('#companies_form .for-region').hide();
                    $('#companies_form').find('div[class^="'+$(this).val()+'"]').show();
                    console.log($('#companies_form').find('div[class="'+$(this).val()+'"]'));
                });

                $('.addPayeeBank').on('click',function(){
                    var selectedCurrency = $(this).prev().prev().val();
                    var payeeBank = $(this).prev().clone();
                    var payeeBankCurrency = $(this).prev().prev().clone();
                    payeeBank.find('button').remove();
                    var btnToRemove="<button type='button' class='btn red mini deletePayeeBank'>Удалить банк получателя</button>";
                    payeeBank.append(btnToRemove);

                    payeeBankCurrency.find('option[value="'+selectedCurrency+'"]').remove();
                    if(payeeBankCurrency.find('option').length==0) {
                        $(this).hide();
                        return false;
                    }
                    $(this).before(payeeBankCurrency).before(payeeBank);
                    initEvents();
                });



            });//end ready
        })(jQuery);
    </script>
{/literal}