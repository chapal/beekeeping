<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Просмотр компании
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="/admin/companies/index/page/1">Компании</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:void(0);">Просмотр компании</a>
                </li>
            </ul>
        </div>
    </div>
    <div id="dashboard">

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Просмотр компании</div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
                            <tbody>
                                <tr>
                                    <td>Наименование</td>
                                    <td><b>{$item.name|stripcslashes}</b></td>
                                </tr>
                                <tr>
                                    <td>Юридический адрес</td>
                                    <td>{$item.legal_address|stripcslashes}</td>
                                </tr>
                                <tr>
                                    <td>Почтовый адрес</td>
                                    <td>{$item.postal_address|stripcslashes}</td>
                                </tr>
                                <tr>
                                    <td>Р.сч.№</td>
                                    <td>{$item.account}</td>
                                </tr>
                                <tr>
                                    <td>Банк получателя</td>
                                    <td>
                                        {foreach from=$item.payee_bank key=currency item=payee_bank}
                                            <p style="outline: 1px dashed #666;padding: 10px;">{$currency|stripcslashes}:  {$payee_bank|stripcslashes}</p>

                                        {/foreach}
                                    </td>
                                </tr>
                                {if $item.region=='russia'}
                                    <tr>
                                        <td>ОГРН</td>
                                        <td>{$item.ogrn|stripcslashes}</td>
                                    </tr>
                                    <tr>
                                        <td>ИНН</td>
                                        <td>{$item.inn|stripcslashes}</td>
                                    </tr>
                                    <tr>
                                        <td>КПП</td>
                                        <td>{$item.kpp|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if $item.region=='belorussia'}
                                    <tr>
                                        <td>ОКПО</td>
                                        <td>{$item.okpo|stripcslashes}</td>
                                    </tr>
                                    <tr>
                                        <td>УНП</td>
                                        <td>{$item.unp|stripcslashes}</td>
                                    </tr>
                                {/if}
                                <tr>
                                    <td>Должность</td>
                                    <td>{$item.position|stripcslashes}</td>
                                </tr>
                                <tr>
                                    <td>ФИО</td>
                                    <td>{$item.fio|stripcslashes}</td>
                                </tr>
                                <tr>
                                    <td>Логотип</td>
                                    <td><img src="/tmp/companies/{$item.logo}" width="100" /></td>
                                </tr>
                                <tr>
                                    <td>Печать</td>
                                    <td><img src="/tmp/companies/{$item.print}" width="100" /></td>
                                </tr>
                            </tbody>
                        </table>
                        <br/>
                        <button type="button" class="btn" onclick="document.location.href='/admin/companies/index/page/1'"><i class="icon-undo"></i> Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>