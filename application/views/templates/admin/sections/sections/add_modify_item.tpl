<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                {if $action == 'modifysection'}Изменение{else}Добавление{/if} раздела <small>&nbsp;</small>
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="/sections/index/page/1">Разделы товаров</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:void(0);">{if $action == 'modifysection'}Изменение{else}Добавление{/if} раздела</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="dashboard">

        <div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption">{if $action == 'modifysection'}<i class="icon-pencil"></i> Изменение{else}<i class="icon-plus"></i> Добавление{/if} раздела</div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
                            <div class="span12">
                                <form method="POST" action="/admin/sections/{$action}" name="section_form" id="section_form" enctype="multipart/form-data">
                                    <input type="hidden" name="step" value="2">
                                    {if $item.id}
                                        <input type="hidden" name="id" value="{$item.id}">
                                    {/if}

                                    <div style="padding-left: 10px; padding-top: 10px;">

                                        <div class="control-group">
                                            <label class="control-label">Заголовок:</label>
                                            <div class="controls">
                                                <input type="text" name="title" id="title" value="{$item.title|stripslashes}" class="span6 m-wrap" onchange="setLink();" onkeyup="setLink();" onkeypress="setLink();" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Ссылка:</label>
                                            <div class="controls">
                                                <input type="text" name="link" id="link" readonly="readonly" value="{$item.link}" class="span6 m-wrap" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Короткое описание:</label>
                                            <div class="controls">
                                                <textarea name="description_short" class="ckeditor">{$item.description_short|stripslashes}</textarea>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Полное описание:</label>
                                            <div class="controls">
                                                <textarea name="description" class="ckeditor">{$item.description|stripslashes}</textarea>
                                            </div>
                                        </div>


                                        <div class="control-group">
                                            <label class="control-label">Загрузить изображение:</label>
                                            <div class="controls">

                                                <div class="fileupload fileupload-new" data-provides="fileupload" style="margin-bottom: 0px;">
                                                    <div class="input-append" style="margin-bottom: 0px;">
                                                        <div class="uneditable-input" style="margin-right: 5px;">
                                                            <i class="icon-file fileupload-exists"></i>
                                                            <span class="fileupload-preview"></span>
                                                        </div>
                                                        <span class="btn btn-success btn-file">
                                                            <span class="fileupload-new"><i class="icon-picture"></i> Выберите картинку</span>
                                                            <span class="fileupload-exists"><i class="icon-picture"></i> Выберите картинку</span>
                                                            <input type="file" class="default" id="upload_id" name="image" />
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Позиция:</label>
                                            <div class="controls">
                                                <input type="text" name="position" id="position" maxlength="3"  value="{$item.position|stripslashes}" class="span1 m-wrap" />
                                            </div>
                                        </div>


                                        <div class="control-group">
                                            <div class="span6">
                                                <div class="portlet box grey" style="margin-top: 20px;">


                                                    <div class="portlet-title">
                                                        <div class="caption">Опции</div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="scroller" data-height="300px">
                                                            {foreach from=$options item=option}
                                                                <div style="float:left; font-weight: bold; clear: both; margin-bottom: 5px;">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td align="left" width="22" style="font-weight: bold; height: 30px; border: 1px solid #dedede; background: #fff; padding: 5px 5px 5px 5px;">
                                                                                <input type="checkbox" name="options[]" value="{$option.id}" {if $option.selected==1}checked="checked"{/if} />
                                                                            </td>
                                                                            <td align="left" width="730" style="font-weight: bold; height: 30px; border: 1px solid #dedede; background: #fff; padding: 5px 5px 5px 5px;">
                                                                                {$option.title|stripslashes|strip_tags}:
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            {/foreach}
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                        </div>








                                        {include file='admin/sections/meta.tpl'}

                                    </div>
                                    <div class="form-actions" style="padding-left: 20px;">
                                        <button type="button" class="btn blue" {if $action!='modifysection'} onclick="checkForm('')" {else} onclick="checkForm('modify')" {/if}><i class="icon-ok"></i> Сохранить</button>
                                        <button type="button" class="btn" onclick="document.location.href='/admin/sections/index/page/1'"><i class="icon-undo"></i> Отмена</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END INLINE TABS PORTLET-->
            </div>
        </div>

    </div>

</div>

{literal}
    <script>
        function checkForm(type){
            if(type==''){
                if ($("#title").val() == '') {
                    alert('Вы должны заподнить поле заголовок...');
                } else if ($("#position").val() == '') {
                    alert('Вы должны заподнить поле позиция...');
                } else if($("#upload_id").val()=='') {
                    alert('Выберите картинку.');
                } else {
                    document.forms.section_form.submit();
                }
            } else {
                if ($("#title").val() == '') {
                    alert('Вы должны заподнить поле заголовок...');
                } else if ($("#position").val() == '') {
                    alert('Вы должны заподнить поле позиция...');
                } else {
                    document.forms.section_form.submit();
                }
            }


        }
        function setLink(){
            var link = createLinkFromTitle($("#title").val());
            $("#link").val(link);
        }
    </script>
{/literal}