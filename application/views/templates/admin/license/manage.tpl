<div class="container-fluid" xmlns="http://www.w3.org/1999/html">
    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Лицензионное соглашение
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li><a href="/admin/license/index">лицензионное соглашение</a></li>
            </ul>

        </div>
    </div>

    <div id="dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Лицензионное соглашение</div>
                        {if !$item}
                        <div class="actions">
                            <a href="/admin/license/add" class="btn blue"><i class="icon-plus"></i> Добавить</a>
                        </div>
                        {/if}
                    </div>
                    <div class="portlet-body" id="gallery">

                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
                            <thead>
                            <tr>
                                <th class="span1" style="text-align: center;">#</th>
                                <th class="span3">Название</th>
                                <th style="text-align: center;">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                                {if $item}
                                <tr>

                                    <td class="span1" style="text-align: center; vertical-align: middle;">{$item.id}</td>
                                    <td style="vertical-align: middle;" class="hidden-480">
                                        {if $item.name!=''}
                                            <a href="/admin/license/edit/id/{$item.id}" target="_blank"><span class="green">{$item.name|stripcslashes}</span></a>
                                        {else}
                                            <span style="color: red;">Не зарегестрирован</span>
                                        {/if}

                                    </td>

                                    <td class="span4" style="text-align: center; vertical-align: middle;">
                                        <a href="/admin/license/edit/id/{$item.id}" title="Посмотреть">Изменить</a>
                                        &nbsp;|&nbsp;
                                        <a href="javascript:void(0);" onclick="return customConfirmBox('Вы уверены в том, что хотите восстановить утерянные письма?','/admin/license/delete/id/{$item.id}')" title="Удалить">Удалить</a>

                                    </td>
                                </tr>
                                {else}
                                <tr>
                                    <td colspan="9" style="text-align: center;">Лицензии не существует...</td>
                                </tr>
                                {/if}

                            </tbody>
                        </table>
                        {include file='admin/services/paging.tpl'}

                    </div>


                </div>

            </div>

        </div>

    </div>


</div>
