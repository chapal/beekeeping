<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                {if $action == 'edit'}Изменение{else}Добавление{/if} категории <small>&nbsp;</small>
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="/admin/mails/index/page/1">Письма</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:void(0);">{if $action == 'editcategory'}Изменение{else}Добавление{/if} категории</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="dashboard">

        <div class="row-fluid ">
            <div class="span12">
                <!-- BEGIN INLINE TABS PORTLET-->
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption">{if $action == 'editcategory'}<i class="icon-pencil"></i> Изменение{else}<i class="icon-plus"></i> Добавление{/if} категории</div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
                            <div class="span12">
                                <form method="POST" action="/admin/mails/{$action}" name="services_form" id="services_form" enctype="multipart/form-data">
                                    {if $action == 'editcategory'}
                                        <input type="hidden" name="MailsCategory[id]" value="{$item.id}">
                                    {/if}

                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Заголовок:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="required" data-cvalidation-fieldname="Заголовок" name="MailsCategory[title]" value="{$item.title|stripslashes}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-actions" style="padding-left: 20px;">
                                        <button type="submit" class="btn blue submitForm"><i class="icon-ok"></i>Сохранить</button>
                                        <button type="button" class="btn" onclick="document.location.href='/admin/mails/index/page/1'"><i class="icon-undo"></i> Отмена</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END INLINE TABS PORTLET-->
            </div>
        </div>

    </div>

</div>

{literal}
    <script type="text/javascript" src="/js/CValidation.js"></script>
    <script>
        var valid = new CValidation('ru','listError');
        valid.ready(function(){
            $('.submitForm').click(function(event){
                event.preventDefault();
                valid.submitForm('#services_form');
            });
        });

    </script>
{/literal}