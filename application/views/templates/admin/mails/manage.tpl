<div class="container-fluid" xmlns="http://www.w3.org/1999/html">
    <div class="row-fluid">
        <div class="span12">

            <h3 class="page-title">
                Письма
            </h3>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li><a href="/admin/mails/index/page/1">Письма</a></li>
            </ul>

        </div>
    </div>

    <div id="dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Список писем</div>
                        <div class="actions">
                            <a href="/admin/mails/add" class="btn blue"><i class="icon-plus"></i> Добавить письмо</a>
                            <a href="/admin/mails/addcategory" class="btn blue"><i class="icon-plus"></i> Добавить Категорию</a>
                        </div>
                    </div>
                    <div class="portlet-body" id="gallery">

                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
                            <thead>
                            <tr>
                                <th class="span1" style="text-align: center;">#</th>
                                <th class="span3">Название письма</th>
                                <th style="text-align: center;">Категория</th>
                                <th style="text-align: center;">Дата создания</th>
                                <th style="text-align: center;">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach from=$items item=item}
                                <tr>

                                    <td class="span1" style="text-align: center; vertical-align: middle;">{$item.id}</td>
                                    <td style="vertical-align: middle;" class="hidden-480">
                                        {if $item.title!=''}
                                            <a href="/admin/mails/edit/id/{$item.id}" target="_blank"><span class="green">{$item.title|stripcslashes}</span></a>
                                        {else}
                                            <span style="color: red;">Не зарегестрирован</span>
                                        {/if}

                                    </td>

                                    <td style="vertical-align: middle;" class="hidden-480">
                                        {if $item.categorytitle!=''}
                                            <a href="/admin/mails/editcategory/id/{$item.categoryid}"  target="_blank"><span class="green">{$item.categorytitle|stripcslashes}</span></a>
                                        {else}
                                            <span style="color: red;">Не зарегестрирован</span>
                                        {/if}

                                    </td>
                                    <td style="text-align: center">
                                        {$item.date_created|date_format:"%d-%m-%Y %H:%M"}
                                    </td>
                                    <td class="span4" style="text-align: center; vertical-align: middle;">
                                        <a href="/admin/mails/edit/id/{$item.id}" title="Посмотреть">Изменить</a>
                                        {if $item.type == 'sendbill' || $item.type == 'remindbill' || $item.type == 'debloyedbill'}
                                        {else}
                                        &nbsp;|&nbsp;
                                            <a href="javascript:void(0);" onclick="return customConfirmBox('Вы уверены, что хотите удалить письмо?','/admin/mails/delete/id/{$item.id}')" title="Удалить">Удалить</a>
                                        {/if}
                                    </td>
                                </tr>
                                {foreachelse}
                                <tr>
                                    <td colspan="9" style="text-align: center;">Нет ни одного письма...</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        {include file='admin/services/paging.tpl'}

                    </div>


                </div>

            </div>

        </div>

        <div class="row-fluid">
            <div class="span4">
                <div>
                   <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-reorder"></i>Список категорий</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
                                <thead>
                                <tr>
                                    <th class="span1" style="text-align: center;">#</th>
                                    <th class="span3">Название Категории</th>
                                    <th style="text-align: center;">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach from=$categories key=key item=item }
                                    <tr>
                                        <td>{$item.id}</td>
                                        <td>
                                            <a href="/admin/mails/editcategory/id/{$item.id}"  target="_blank"><span class="green">{$item.title|stripcslashes}</span></a>
                                        </td>
                                        <td style="text-align: center">
                                            <a href="/admin/mails/editcategory/id/{$item.id}" title="Посмотреть">Изменить</a>
                                            &nbsp;|&nbsp;
                                            <a href="javascript:void(0);" onclick="return customConfirmBox('Удаление категории повлечет удаление всех писем из этой категории, вы уверены?','/admin/mails/deletecategory/id/{$item.id}')" title="Удалить">Удалить</a>
                                        </td>
                                    </tr>
                                    {foreachelse}
                                    <tr>
                                        <td colspan="3" style="text-align: center;">Нет ни одной категории...</td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table
                            <ul>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            {if $mailsrecovery == true}
                <div class="span3">
                    <a href="javascript:void(0);" class="btn big red" onclick="return customConfirmBox('Вы уверены в том, что хотите восстановить утерянные письма?','/admin/mails/recoverymails')">Восстановить письма</a>
                </div>
            {/if}

        </div>

    </div>


</div>
<div class="changeStatusBill" style="position: absolute;background: rgba(255,255,255,0.8);box-shadow: 1px 1px 10px #4A4AAA;padding: 10px;display: none;">
    <div style="padding-left: 10px; padding-top: 10px;">
        <form method="post" action="/admin/mails/categoryaction">
            <div class="control-group">
                <label class="control-label">Действия:</label>
                <div class="controls">
                    <input type="hidden" name="MailsCategory[id]" value="" />
                    <select name="MailsCategory[action]">
                        <option value="edit">ИЗМЕНИТЬ</option>
                        <option value="remove">УДАЛИТЬ</option>

                    </select>
                    <button type="primary" class="btn blue" style="margin-top: -7px;">Выполнить</button>
                </div>
            </div>
        </form>
    </div>

</div>
{literal}
    <script type="text/javascript">
        (function($){
            $(document).ready(function(){

                $('.mailsCategory').on('click',function(event){
                    event.preventDefault();
                    var x = event.pageX;
                    var y = event.pageY;
                    var id = $(this).attr('href');
                    $('.changeStatusBill').css({'left':x+40+'px','top':y-50+'px'});
                    $('.changeStatusBill').fadeIn(function(){
                        $('.changeStatusBill input[type=hidden]').val(id);
                        $(document).on('click',function(event){
                            if(!$(event.target).parents('.changeStatusBill').length > 0) {
                                $('.changeStatusBill').fadeOut(function(){
                                    $(document).off('click');
                                });
                            }
                        });
                    });

                });
            });
        })(jQuery);
    </script>
{/literal}