<script type="text/javascript" src="/css/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/css/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<div class="row-fluid">
    <div class="span12">
        <div style="padding: 20px;">
            <div class="portlet box grey">
                <div class="portlet-title">
                    <div class="caption">{if $action == 'edit'}<i class="icon-pencil"></i> Изменить{else}<i class="icon-plus"></i> Выставить{/if} счет</div>
                </div>
                <div class="portlet-body form">
                    <div class="row-fluid">
                        <div class="span12">
                            <form method="POST" action="/admin/bills/{$action}" name="services_form" id="services_form" enctype="multipart/form-data">
                                {if $action == 'edit'}
                                    <input type="hidden" name="Bills[id]" value="{$item.billid}">
                                {/if}
                                <input type="hidden" name="method" value="{$method}" />
                                <div style="padding-left: 10px; padding-top: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Название:</label>
                                        <div class="controls">
                                            <input type="text" data-cvalidation="required" data-cvalidation-fieldname="Назавние" name="Bills[title]" value="{$item.billtitle}" class="span6 m-wrap" />
                                        </div>
                                    </div>
                                </div>
                                {if $method=='AJAX'}
                                <input type="hidden" name="Bills[user_id]" value="{$userid}" />
                                <div style="padding-left: 10px; padding-top: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Имя:</label>
                                        <div class="controls">
                                            <input type="text"  name="username" value="{$username|stripslashes|trim}" class="span6 m-wrap" />
                                        </div>
                                    </div>
                                </div>
                                <div style="padding-left: 10px; padding-top: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Email:</label>
                                        <div class="controls">
                                            <input type="text" data-cvalidation="email" name="Bills[email]"  value="{$useremail|stripslashes|trim}" class="span6 m-wrap" />
                                        </div>
                                    </div>
                                </div>
                                {/if}
                                {if $method=='GET'}
                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Выберите пользователя:</label>
                                            <div class="controls">
                                                <select  name="Bills[user_id]" class="span6">
                                                    {foreach from=$users item=user}
                                                        <option alt="{$user.email}" {if $action=='edit'} {if $item.userid==$user.id} selected="selected" {/if} {/if} value="{$user.id}">{$user.first_name} {$user.last_name} | {$user.email}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding-left: 10px; padding-top: 10px;">
                                        <div class="control-group">
                                            <label class="control-label">Email:</label>
                                            <div class="controls">
                                                <input type="text" data-cvalidation="email" name="Bills[email]"  value="{$useremail|stripslashes|trim}{if $action=='edit'}{$item.email} {/if}" class="span6 m-wrap" />
                                            </div>
                                        </div>
                                    </div>

                                {/if}
                                <div style="padding-left: 10px; padding-top: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Тип счета:</label>
                                        <div class="controls">
                                            <select data-cvalidation="notBe(0)" name="Bills[type]" class="span6">
                                                <option  selected="selected" value="0">Выберите тип</option>
                                                    <option value="products">Продукт</option>
                                                    <option value="offer">Услуга</option>
                                                    <option value="services">Сервис</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {if $action=='edit'}
                                    <input type="hidden" id="typeOfCurrentBill" value="{$item.type}" />
                                    {if $item.service_id}
                                        <input type="hidden" id="serviceIdOfCurrentBill" value="{$item.service_id}" />
                                    {/if}
                                    {if $item.product_id}
                                    <input type="hidden" id="productIdOfCurrentBill" value="{$item.product_id}" />
                                    {/if}
                                    {literal}
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                var billType = $('#typeOfCurrentBill').val();
                                                setTimeout(function(){
                                                    $('select[name="Bills[type]"]').val(billType).trigger('change');
                                                    if(billType=='offer') {
                                                        $('input[name="Bills[offer]"]').val('{/literal}{$item.offer}{literal}');
                                                        $('input[name="Bills[price]"]').val('{/literal}{$item.price|intval}{literal}');
                                                    }
                                                    if(billType=='services') {
                                                        var servicesList = setInterval(function(){
                                                            if($('select[name="Bills[service_id]"] option:eq(0)').length > 0) {
                                                                clearInterval(servicesList);
                                                                var serviceId = $('#serviceIdOfCurrentBill').val();
                                                                $('select[name="Bills[service_id]"]').find('option[value='+serviceId+']').attr('selected','selected');
                                                            }
                                                        },300);
                                                    }
                                                    if(billType=='products') {
                                                        var servicesList = setInterval(function(){
                                                            if($('select[name="Bills[product_id]"] option:eq(0)').length > 0) {
                                                                clearInterval(servicesList);
                                                                var serviceId = $('#productIdOfCurrentBill').val();
                                                                $('select[name="Bills[product_id]"]').find('option[value='+serviceId+']').attr('selected','selected');
                                                            }
                                                        },300);
                                                    }
                                                },100);

                                            });
                                        </script>
                                    {/literal}
                                {/if}
                                <div style="padding-left: 10px; padding-top: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Счет к чему:</label>
                                        <div class="controls dependOfType">
                                            <select data-cvalidation="required" name="" class="span6 " disabled="disabled">

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div style="padding-left: 10px; padding-top: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Валюта счета:</label>

                                                <select name="Bills[currency]" class="span6">
                                                    <option value="BYR" {if $currency == "BYR"} selected="selected"{/if} {if $action=='edit'}{if $item.currency=='BYR'} selected="selected"{/if}{/if}>BYR</option>
                                                    <option value="RUR" {if $currency == "RUR"} selected="selected"{/if} {if $action=='edit'}{if $item.currency=='RUR'} selected="selected"{/if}{/if}>RUR</option>
                                                    <option value="USD" {if $currency == "USD"} selected="selected"{/if} {if $action=='edit'}{if $item.currency=='USD'} selected="selected"{/if}{/if}>USD</option>
                                                    <option value="EUR" {if $currency == "EUR"} selected="selected"{/if} {if $action=='edit'}{if $item.currency=='EUR'} selected="selected"{/if}{/if}>EUR</option>
                                                </select>

                                    </div>
                                </div>
                                <div style="padding-left: 10px; padding-top: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Срок оплаты</label>
                                        <div class="controls">
                                            <div class="input-append date">
                                                <input class="m-wrap m-ctrl-medium date-picker" name="Bills[due_date]" readonly="" size="16" type="text" value="{$item.due_date}"><span class="add-on"><i class="icon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="padding-left: 10px; padding-top: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Выберите компанию получателя:</label>
                                        <div class="controls">
                                            <select data-cvalidation="required" name="Bills[company_id]" class="span6">
                                                {foreach from=$companies key=key item=company}
                                                    <option value="{$company.id}" {if $action=='edit'} {if $item.company_id==$company.id} selected="selected" {/if}{/if}>{$company.name|stripcslashes}</option>
                                                {/foreach}

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div style="padding-left: 10px; padding-top: 10px;">
                                    <div class="control-group">
                                        <label class="control-label">Комментарий к счету</label>
                                        <div class="controls">
                                            <textarea name="Bills[comment]" class="span6 m-wrap" rows="3">{$item.comment}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div style="padding-left: 10px; padding-top: 10px;">

                                        <label class="control-label">Отправить счет пользователю  <input name="sendToUser" type="checkbox" {if $action != 'edit'} checked="checked"  {/if}value=""/></label>
                                </div>




                                <div class="form-actions" style="padding-left: 20px;">
                                    <button type="submit" class="btn blue submitForm"><i class="icon-ok"></i>Сохранить</button>
                                    <button type="button" class="btn" onclick="{if $method === 'AJAX'}customCloseBox(){/if}{if $method === 'GET'}document.location.href='/admin/bills/index/page/1'{/if}"><i class="icon-undo"></i> Отмена</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{literal}

    <script type="text/javascript" src="/js/CValidation.js"></script>
    <script type="text/javascript">
        var valid2 = new CValidation('ru');
        valid2.ready(function(){
            $('#services_form .submitForm').click(function(event){

                valid2.submitForm('#services_form',event);
            });
        });
        (function($){

            $(document).ready(function(){

                var method = $('input[name="method"]');
                if(method.val()=='GET'){
                    $('select[name="Bills[user_id]"]').change(function(){
                       var textMe = $(this).find('option:selected').attr('alt');
                       $('input[name="Bills[email]"]').val(textMe);

                    });
                }

                $('select[name="Bills[type]"]').change(function(){
                    if($(this).val()=='0') {
                        return false;
                    }
                    $('form input[name="Bills[currency]"]').remove();
                    var typeSelected = $(this).val();
                    console.log(typeSelected);
                    if($(this).val()=='offer') {
                        $('.dependOfType').html('<input type="text"  name="Bills[offer]" value="" class="span6 m-wrap" /><br/><label>Стоимость</label><input type="text"  name="Bills[price]" value="" class="span6 m-wrap" />');
                        return false;
                    }
                    if($(this).val()=='products') {
                        $('.dependOfType').html('<select name="Bills[product_id]" class="span6 " disabled="disabled"></select>');
                    }
                    if($(this).val()=='services') {
                        $('.dependOfType').html('<select name="Bills[service_id]" class="span6 " disabled="disabled"></select>');
                    }
                    var ajaxUrl = (typeSelected=='services') ? 'getservices' : 'getbytype';
                    $.ajax({
                        type:'POST',
                        url:'/admin/bills/'+ajaxUrl,
                        data:{type:$(this).val()},
                        beforeSend:function(){
                            $('.dependOfType').find('input,select').css({'background':'url("/images/loading.gif") no-repeat center'});
                        },
                        success:function(response){
                            $('.dependOfType select').removeAttr('disabled').html(response);
                        },
                        complete:function(){
                            $('.dependOfType').find('input,select').css('background','inherit');
                        }

                    });
                });
                if ($.datepicker) {
                   $('.date-picker').datepicker({language:'ru'});
                }
            });
        })(jQuery);
    </script>

{/literal}