<script type="text/javascript" src="/css/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/css/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<div class="container-fluid" xmlns="http://www.w3.org/1999/html">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Счета
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="/admin">Главная</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li><a href="/admin/orders/index/page/1">Счета</a></li>
            </ul>

        </div>
    </div>

    <div id="dashboard">
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption" id="pulsate-regular"><i class="icon-list"></i>Фильтр</div>
                    </div>
                    <div class="portlet-body" id="gallery">

                        <form action="" id="filter" class="form-horizontal" style="margin: 10px 0px 10px 0px;" method="post">
                            <div class="control-group" style="padding-top: 0px; margin-bottom: 0px;">
                                <input type="hidden" name="Filter[model]" value="Bills">
                                <input type="hidden" name="Filter[active]" value="">
                                <div class="row-fluid">
                                    <div class="span4">
                                        <label class="control-label" style="width: 80px;">Статус:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <select name="Filter[status]" id="filter_status" class="m-wrap chosen">
                                                <option value=""> ------ </option>
                                                <option value="paid" {if $filter.status=='paid' } selected="selected"{/if}> ОПЛАЧЕН </option>
                                                <option value="sent" {if $filter.status=='sent' } selected="selected"{/if}> ВЫСТАВЛЕН </option>
                                                <option value="draft" {if $filter.status=='draft' } selected="selected"{/if} > ЧЕРНОВИК </option>
                                                <option value="delayed" {if $filter.status=='delayed' } selected="selected"{/if}> ПРОСРОЧЕН </option>
                                                <option value="aborted" {if $filter.status=='aborted' } selected="selected"{/if}> ОТМЕНЕН </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <label class="control-label" style="width: 80px;">Email:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <input type="text" name="Filter[email__users]" id="filter_email" value="{$filter.email__users}" class="m-wrap chosen" />
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <label class="control-label" style="width: 80px;">Имя:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <input type="text" name="Filter[first_name__users]" id="filter_first_name" value="{$filter.first_name__users}" class="m-wrap chosen" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid" style="margin-top: 20px;">
                                    <div class="span3">
                                        <label class="control-label" style="width: 80px;">Дата от:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <div class="input-append date">
                                                <input class="m-wrap m-ctrl-medium date-picker" name="Filter[due_date->from]" readonly="" size="16" type="text" value="{$filter.due_date_from}"><span class="add-on"><i class="icon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span3">
                                        <label class="control-label" style="width: 80px;">Дата до:</label>
                                        <div class="controls" style="margin-left: 90px;">
                                            <div class="input-append date">
                                                <input class="m-wrap m-ctrl-medium date-picker" name="Filter[due_date->to]" readonly="" size="16" type="text" value="{$filter.due_date_to}"><span class="add-on"><i class="icon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="span3" style="margin-left: 120px;">
                                        <a href="javascript:void(0);" class="btn green" onclick="filterOn();" style="padding: 6px 14px;" /><i class="icon-ok"></i> Включить</a>
                                        <a href="javascript:void(0);" class="btn yellow" onclick="filterOff();" style="padding: 6px 14px;" /><i class="icon-refresh"></i> Сбросить</a>
                                    </div>
                                    </div>

                            </div>
                        </form>


                    </div>


                </div>


            </div>

        </div>
        <div class="row-fluid">
            <div class="span12" style="padding-bottom: 160px">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Список счетов</div>
                        <div class="actions">
                            <a href="/admin/bills/add" class="btn blue"><i class="icon-plus"></i> Добавить</a>
                        </div>
                    </div>
                    <div class="portlet-body" id="gallery">

                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
                            <thead>
                            <tr>

                                <th class="span1" style="text-align: center;">#</th>
                                <th class="hidden-480">Названия</th>
                                <th>Пользователь</th>
                                <th class="hidden-480" style="text-align: center;">Компания-получатель</th>
                                <th class="span2" style="text-align: center;">Статус</th>
                                <th class="hidden-480 span1" style="text-align: center;">Срок оплаты</th>
                                <th class="hidden-480 span1" style="text-align: center;">Создан</th>
                                <th class="span2" style="text-align: center;">Сумма счета</th>
                                <th style="text-align: center;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach from=$items item=item}
                                <tr>
                                    <td class="span1" style="text-align: center; vertical-align: middle;">{$item.id}</td>
                                    <td style="vertical-align: middle;" class="hidden-480">
                                        {if $item.title!=''}
                                            <a href="/admin/bills/view/id/{$item.id}" target="_blank"><span class="green">{$item.title|stripcslashes}</span></a>
                                        {else}
                                            <span style="color: red;">Не зарегестрирован</span>
                                        {/if}

                                    </td>

                                    <td class="span2" style="text-align: center; vertical-align: middle;">
                                        {$item.email|stripcslashes}|{$item.first_name|stripcslashes}
                                    </td>
                                    <td class="hidden-480 span2" style="text-align: center; vertical-align: middle;">
                                        {$item.companyname|stripcslashes}
                                    </td>
                                    <td class="span2" style="text-align: center; vertical-align: middle;{if $item.status=='paid'}background-color: rgba(0,172,46,0.2) !important;{/if}">

                                        <a class="billStatus" href="{$item.id}">{if $item.status=='paid'}ОПЛАЧЕН{elseif $item.status=='sent'}ВЫСТАВЛЕН{elseif $item.status=='draft'}ЧЕРНОВИК{elseif $item.status=='delayed'}<span style="color: #FF0000">ПРОСРОЧЕН</span>{/if}</a>
                                    </td>
                                    <td class="span3" style="text-align: center; vertical-align: middle;">
                                       {if $item.type=='services'} Следующий: {/if} {$item.due_date|date_format:"%d-%m-%Y"}
                                    </td>
                                    <td class="span3" style="text-align: center; vertical-align: middle;">
                                         {$item.created|date_format:"%d-%m-%Y"}
                                    </td>
                                    <td class="span3" style="text-align: center; vertical-align: middle;">
                                       {$item.billamount}  {$item.currency}
                                    </td>

                                    <td class="span4" style="text-align: center; vertical-align: middle;">

                                        <div class="btn-group">
                                            <a class="btn purple" href="#" data-toggle="dropdown">
                                                <i class="icon-user"></i> Действия
                                                <i class="icon-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu" style="text-align: left">
                                                <li> <a href="/admin/bills/view/id/{$item.id}" title="Посмотреть"><i class="icon-eye-open"></i> Посмотреть</a></li>
                                                <li> <a href="/bills/index/pdf?num={$item.link}" title="Скачать"><i class="icon-download-alt"></i> Скачать</a></li>
                                                {if $item.status=='draft'}
                                                    <li><a href="{$item.id}" alt="{$item.id}" class="sendBill" title="Удалить"><i class="icon-check"></i> Выставить</a></li>
                                                {/if}
                                                <li><a href="{$item.id}" alt="{$item.billlink}" class="getLink" title="Получить ссылку"><i class="icon-globe"></i> Получить ссылку</a></li>
                                                {if $item.status != 'draft'}
                                                    <li> <a href="{$item.id}" alt="" class="sendAgain" title="Выставить повторно"><i class="icon-retweet"></i> Выставить повторно</a></li>
                                                {/if}
                                                <li><a href="/admin/bills/createcopy/id/{$item.id}" alt="" title="Выставить повторно"><i class="icon-copy"></i> Создать копию</a></li>
                                                <li><a href="/admin/bills/edit/id/{$item.id}"><i class="icon-cogs"></i> Изменить</a></li>
                                                <li><a href="javascript:void(0);" onclick="return customConfirmBox('Вы уверены, что хотите удалить этот счет?','/admin/bills/delete/id/{$item.id}')" title="Удалить"><i class="icon-trash"></i> Удалить</a></li>


                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                {foreachelse}
                                <tr>
                                    <td colspan="9" style="text-align: center;">Нет ни одной компании...</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        {include file='admin/companies/paging.tpl'}

                    </div>

                </div>
                <button type="button" style="display: none;" class="notificationbtn btn blue big btn-block"><span class="notificationmsg"></span><i class="m-icon-big-swapright m-icon-white"></i></button>
            </div>
        </div>
    </div>
</div>
<div id="confirmSend" class="confirmationSend" style="display: none; width: auto; min-height: 0px; position: relative;z-index: 90; margin-top: -200px !important;background-color: #F6F6F6; max-height: none; height: 138px;width: 292px;background: #FFF;margin: 0px auto;box-shadow: 0px 0px 2px #000;">
    <form id="confirmationForm" action="/admin/bills/send" method="post">
        <div style="padding-left: 10px; padding-top: 10px;">
            <div class="control-group">
                <label class="control-label">Email:</label>
                <div class="controls">
                    <input type="hidden" name="Bills[id]" value="" />
                    <input type="text" data-cvalidation="email" name="Bills[email]"  value="{$useremail|stripslashes|trim}" class="span3 m-wrap" />
                    <button type="submit" class="btn blue">Отправить</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="changeStatusBill" style="position: absolute;background: rgba(255,255,255,0.8);box-shadow: 1px 1px 10px #4A4AAA;padding: 10px;display: none;">
    <div style="padding-left: 10px; padding-top: 10px;">
        <form method="post" action="/admin/bills/changestatus">
            <div class="control-group">
                <label class="control-label">Статус:</label>
                <div class="controls">
                   <input type="hidden" name="Bills[id]" value="" />
                   <select name="Bills[status]">
                       <option value="sent">ВЫСТАВЛЕН</option>
                       <option value="draft">ЧЕРНОВИК</option>
                       <option value="delayed">ПРОСРОЧЕН</option>
                       <option value="aborted">ОТМЕНЕН</option>
                       <option value="paid">ОПЛАЧЕН</option>
                   </select>
                    <button type="primary" class="btn blue" style="margin-top: -7px;">Сохранить</button>
                </div>
            </div>
        </form>
    </div>

</div>
    <script type="text/javascript" src="/js/CValidation.js"></script>
{literal}
    <script>
        var valid2 = new CValidation('ru');
        valid2.ready(function(){
            $('#confirmationForm button[type=submit]').click(function(event){

                valid2.submitForm('#confirmationForm',event);
            });
        });
        (function(){
            $(document).ready(function(){

                $('.billStatus').on('click',function(event){
                    event.preventDefault();
                    var x = event.pageX;
                    var y = event.pageY;
                    var id = $(this).attr('href');
                    $('.changeStatusBill').css({'left':x+40+'px','top':y-50+'px'});
                    $('.changeStatusBill').fadeIn(function(){
                        $('.changeStatusBill input[type=hidden]').val(id);
                        $(document).on('click',function(event){
                            if(!$(event.target).parents('.changeStatusBill').length > 0) {
                                $('.changeStatusBill').fadeOut(function(){
                                    $(document).off('click');
                                });
                            }
                        });
                    });

                });

                $('.sendBill').on('click',function(event){
                    event.preventDefault();
                    var billsId = $(this).attr('alt');

                    $('#confirmSend').fadeIn(function(){
                        $('#confirmSend input[type=hidden]').val(billsId);
                        $(document).on('click',function(event){

                            if(!$(event.target).parents('.confirmationSend').length > 0) {
                                $('#confirmSend').fadeOut(function(){
                                    $(document).off('click');
                                });
                            }
                        });
                    });
                });//end click of $('.sendBil')


                $('.getLink').on('click',function(event){
                    event.preventDefault();
                    var link = $(this).attr('alt');
                    prompt('Скопируйте ссылку',link);
                });

                $('.sendAgain').on('click',function(event){
                        event.preventDefault();
                    if(confirm("Выставить счет повторно?")) {

                        var id = $(this).attr('href');
                        $.ajax({
                            type:'POST',
                            url:'/admin/bills/againsend',
                            data:{id:id},
                            beforeSend:function(){
                                $('.notificationbtn').removeClass('green').addClass('blue');
                                $('.notificationbtn .notificationmsg').text('Идет отправка...');
                                $('.notificationbtn').fadeIn();
                            },
                            success:function(response){
                                    if(response.status===200) {
                                        $('.notificationbtn').removeClass('blue').addClass('green');
                                        $('.notificationbtn .notificationmsg').text(response.msg);
                                        setTimeout(function(){
                                            $('.notificationbtn').fadeOut();
                                        },3000);

                                    }
                            }
                        });
                    }
                });



                if ($.datepicker) {
                    $('.date-picker').datepicker({language:'ru',format:'dd/mm/yyyy'});
                }
            }); //end ready
        })(jQuery);

        function filterOn(){
            $('#filter input[name="Filter[active]"]').val(1);
            $("#filter").submit();
        }

        function filterOff(){
            $('#filter input[name="Filter[active]"]').val(0);
            $("#filter").submit();
        }
    </script>
{/literal}