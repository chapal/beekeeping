<div class="underlined push-down-20">
    <h3><span class="light">Информация, которая необходима для</span> поисковисков</h3>
</div>

<p class="push-down-10">
    <label for="author">МЕТА заколовок:</label>
    <input type="text" id="meta_title" name="meta_title" value="{$item.meta_title|stripslashes}" class="span6 m-wrap" placeholder="Введите МЕТА название вашего товара здесь...">
</p>

<p class="push-down-10">
    <label for="author">МЕТА ключевые слова:</label>
    <input type="text" id="meta_keywords" name="meta_keywords" value="{$item.meta_keywords|stripslashes}" class="span6 m-wrap" placeholder="Введите МЕТА ключевые слова вашего товара здесь...">
</p>

<p class="push-down-10">
    <label for="author">МЕТА описание:</label>
    <input type="text" id="meta_description" name="meta_description" value="{$item.meta_description|stripslashes}" class="span6 m-wrap" placeholder="Введите МЕТА описание вашего товара здесь...">
</p>