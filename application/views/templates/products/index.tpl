<div class="darker-stripe">
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul class="breadcrumb">
                    <li>
                        <a href="/">Главная</a>
                    </li>
                    <li><span class="icon-chevron-right"></span></li>
                    <li>
                        <a href="/catalog.html">Каталог</a>
                    </li>
                    {if $currSection}
                        <li><span class="icon-chevron-right"></span></li>
                        <li>
                            <a href="/products/section/{$currSection.link}">{$currSection.title|strip_tags|stripslashes}</a>
                        </li>
                        {if $currCategory}
                            <li><span class="icon-chevron-right"></span></li>
                            <li>
                                <a href="/products/category/{$currCategory.link}/page/1">{$currCategory.title|strip_tags|stripslashes}</a>
                            </li>
                        {/if}
                    {/if}
                </ul>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <div class="push-up blocks-spacer">
        <div class="row">

            <!-- Main content -->
            <section class="span12">

                <div class="underlined push-down-15">
                    <div class="row">
                        <div class="span9">
                            <h3>
                                Разделы
                                {if $currSection}
                                    &nbsp;<span class="light">::</span>&nbsp;
                                    <a href="/products/section/{$currSection.link}"><span class="light">{$currSection.title|strip_tags|stripslashes}</span></a>
                                    {if $currCategory}
                                        &nbsp;<span class="light">::</span>&nbsp;
                                        <a href="/products/category/{$currCategory.link}"><span class="light">{$currCategory.title|strip_tags|stripslashes}</span></a>
                                    {/if}
                                {/if}
                            </h3>
                        </div>
                    </div>
                </div>


                <div class="row popup-products blocks-spacer" style="margin-bottom: 25px; margin-top: 40px;">
                    {foreach key=nKey from=$products item=prod}

                    {if $nKey>0 && $nKey%4==0}
                </div>
                <div class="row popup-products blocks-spacer">
                    {/if}

                    <!-- Product -->
                    <div class="span3">
                        <div class="product2">
                            <div class="product-inner">
                                <a href="/product/{$prod.link}">
                                    <div class="product-img">
                                        <div class="picture">
                                            {if $prod.image!=""}
                                                <img src="/images/products/{$prod.image}_middle.jpg" alt="" width="100%" />
                                            {else}
                                                <img src="/images/sections/default_big.png" alt="" width="100%" />
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="main-titles no-margin">
                                        <div class="titleHeader" style="border-top:1px solid #b2b2b2;">
                                            <h5 class="no-margin" style="font-weight: normal;">{$prod.title|stripslashes|strip_tags}</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    {/foreach}
                </div>

            </section>
        </div>
    </div>
</div>
