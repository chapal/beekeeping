<div class="row featured-items blocks-spacer">
    <div class="span12">

        <!--  ==========  -->
        <!--  = Title =  -->
        <!--  ==========  -->
        <div class="main-titles lined">
            <h2 class="title">Скоро <span class="light">в продаже</span></h2>
            <div class="arrows">
                <a href="#" class="icon-chevron-left" id="featuredItemsLeft"></a>
                <a href="#" class="icon-chevron-right" id="featuredItemsRight"></a>
            </div>
        </div>
    </div>

    <div class="span12">
        <!--  ==========  -->
        <!--  = Carousel =  -->
        <!--  ==========  -->
        <div class="carouFredSel" data-autoplay="false" data-nav="featuredItems">
            <div class="slide">
                <div class="row">





                    <!--  ==========  -->
                    <!--  = Product =  -->
                    <!--  ==========  -->
                    <div class="span4">
                        <div class="product">
                            <div class="product-img featured">
                                <div class="picture">
                                    <img src="images/dummy/featured-products/featured-1.png" alt="" width="518" height="358" />
                                    <div class="img-overlay">
                                        <a href="#" class="btn more btn-primary">More</a>
                                        <a href="#" class="btn buy btn-danger">Buy</a>
                                    </div>
                                </div>
                            </div>
                            <div class="main-titles">
                                <h4 class="title">$29</h4>
                                <h5 class="no-margin">Horsefeathers 645</h5>
                            </div>
                            <p class="desc">59% Cotton Lorem Ipsum Dolor Sit Amet esed ultrices sapien nunc nam frignila</p>
                        </div>
                    </div> <!-- /product -->





                    <!--  ==========  -->
                    <!--  = Product =  -->
                    <!--  ==========  -->
                    <div class="span4">
                        <div class="product">
                            <div class="product-img featured">
                                <div class="picture">
                                    <img src="images/dummy/featured-products/featured-2.png" alt="" width="518" height="358" />
                                    <div class="img-overlay">
                                        <a href="#" class="btn more btn-primary">More</a>
                                        <a href="#" class="btn buy btn-danger">Buy</a>
                                    </div>
                                </div>
                            </div>
                            <div class="main-titles">
                                <h4 class="title">$103</h4>
                                <h5 class="no-margin">Horsefeathers 639</h5>
                            </div>
                            <p class="desc">59% Cotton Lorem Ipsum Dolor Sit Amet esed ultrices sapien nunc nam frignila</p>
                        </div>
                    </div> <!-- /product -->





                    <!--  ==========  -->
                    <!--  = Product =  -->
                    <!--  ==========  -->
                    <div class="span4">
                        <div class="product">
                            <div class="product-img featured">
                                <div class="picture">
                                    <img src="images/dummy/featured-products/featured-3.png" alt="" width="518" height="358" />
                                    <div class="img-overlay">
                                        <a href="#" class="btn more btn-primary">More</a>
                                        <a href="#" class="btn buy btn-danger">Buy</a>
                                    </div>
                                </div>
                            </div>
                            <div class="main-titles">
                                <h4 class="title">$94</h4>
                                <h5 class="no-margin">Horsefeathers 618</h5>
                            </div>
                            <p class="desc">59% Cotton Lorem Ipsum Dolor Sit Amet esed ultrices sapien nunc nam frignila</p>
                        </div>
                    </div> <!-- /product -->




                </div>
            </div>
            <div class="slide">
                <div class="row">


                    <!--  ==========  -->
                    <!--  = Product =  -->
                    <!--  ==========  -->
                    <div class="span4">
                        <div class="product">
                            <div class="product-img featured">
                                <div class="picture">
                                    <img src="images/dummy/featured-products/featured-1.png" alt="" width="518" height="358" />
                                    <div class="img-overlay">
                                        <a href="#" class="btn more btn-primary">More</a>
                                        <a href="#" class="btn buy btn-danger">Buy</a>
                                    </div>
                                </div>
                            </div>
                            <div class="main-titles">
                                <h4 class="title">$86</h4>
                                <h5 class="no-margin">Horsefeathers 404</h5>
                            </div>
                            <p class="desc">59% Cotton Lorem Ipsum Dolor Sit Amet esed ultrices sapien nunc nam frignila</p>
                        </div>
                    </div> <!-- /product -->





                    <!--  ==========  -->
                    <!--  = Product =  -->
                    <!--  ==========  -->
                    <div class="span4">
                        <div class="product">
                            <div class="product-img featured">
                                <div class="picture">
                                    <img src="images/dummy/featured-products/featured-2.png" alt="" width="518" height="358" />
                                    <div class="img-overlay">
                                        <a href="#" class="btn more btn-primary">More</a>
                                        <a href="#" class="btn buy btn-danger">Buy</a>
                                    </div>
                                </div>
                            </div>
                            <div class="main-titles">
                                <h4 class="title">$39</h4>
                                <h5 class="no-margin">Horsefeathers 349</h5>
                            </div>
                            <p class="desc">59% Cotton Lorem Ipsum Dolor Sit Amet esed ultrices sapien nunc nam frignila</p>
                        </div>
                    </div> <!-- /product -->





                    <!--  ==========  -->
                    <!--  = Product =  -->
                    <!--  ==========  -->
                    <div class="span4">
                        <div class="product">
                            <div class="product-img featured">
                                <div class="picture">
                                    <img src="images/dummy/featured-products/featured-3.png" alt="" width="518" height="358" />
                                    <div class="img-overlay">
                                        <a href="#" class="btn more btn-primary">More</a>
                                        <a href="#" class="btn buy btn-danger">Buy</a>
                                    </div>
                                </div>
                            </div>
                            <div class="main-titles">
                                <h4 class="title">$44</h4>
                                <h5 class="no-margin">Horsefeathers 630</h5>
                            </div>
                            <p class="desc">59% Cotton Lorem Ipsum Dolor Sit Amet esed ultrices sapien nunc nam frignila</p>
                        </div>
                    </div> <!-- /product -->
                </div>
            </div>
        </div> <!-- /carousel -->
    </div>

</div>