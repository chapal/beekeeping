<div class="most-popular blocks-spacer">
    <div class="container">

        <!-- Title -->
        <div class="row">
            <div class="span12">
                <div class="main-titles lined">
                    <h2 class="title"><span class="light">Популярные</span> товары</h2>
                </div>
            </div>
        </div>
        <!-- /Title -->

        <div class="row popup-products">

            {foreach from=$popularProducts item=popularProd}

            <!-- Product -->
            <div class="span3">
                <div class="product">
                    <div class="product-inner">
                        <div class="product-img">
                            <div class="picture">
                                <img src="/images/products/{$popularProd.image}_middle.jpg" alt="" width="270" height="189" />
                                <div class="img-overlay">
                                    <a href="/product/{$popularProd.link}" class="btn more btn-primary">Обзор</a>
                                    <a class="btn buy btn-danger" href="javascript:void(0);" onclick="addToCart('{$popularProd.id}')">Добавить в корзину</a>
                                </div>
                            </div>
                        </div>
                        <div class="main-titles no-margin">
                            <h4 class="title" id="prodTitle{$popularProd.id}">${$popularProd.price}</h4>
                            <h5 class="no-margin">{$popularProd.title|stripslashes|strip_tags}</h5>
                        </div>
                        <p class="desc">{$popularProd.description|stripslashes|strip_tags}</p>
                        <input type="hidden" name="prodCount{$popularProd.id}" id="prodCount{$popularProd.id}" value="1" />
                        {*<div class="row-fluid hidden-line">*}
                            {*<div class="span6">*}
                                {*<a href="#" class="btn btn-small"><i class="icon-heart"></i></a>*}
                                {*<a href="#" class="btn btn-small"><i class="icon-exchange"></i></a>*}
                            {*</div>*}
                            {*<div class="span6 align-right">*}
                                {*<span class="icon-star stars-clr"></span>*}
                                {*<span class="icon-star stars-clr"></span>*}
                                {*<span class="icon-star"></span>*}
                                {*<span class="icon-star"></span>*}
                                {*<span class="icon-star"></span>*}
                            {*</div>*}
                        {*</div>*}
                    </div>
                </div>
            </div>
            <!-- /Product -->

            {/foreach}




        </div>
    </div>
</div>