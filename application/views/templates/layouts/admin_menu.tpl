<input type="hidden" id="menu_item_1_1" name="menu_item_1_1" value="{$adminLangParams.MENU_CONTENT_STATIC_PAGES}" />
<input type="hidden" id="menu_item_1_2" name="menu_item_1_2" value="{$adminLangParams.MENU_CONTENT_NEWS}" />
<input type="hidden" id="menu_item_1_3" name="menu_item_1_3" value="{$adminLangParams.MENU_CONTENT_ARTICLES}" />
<input type="hidden" id="menu_item_1_4" name="menu_item_1_4" value="{$adminLangParams.MENU__REVIEWS}" />
<input type="hidden" id="menu_item_1_5" name="menu_item_1_5" value="{$adminLangParams.MENU_CONTENT_SECTIONS}" />
<input type="hidden" id="menu_item_1_6" name="menu_item_1_6" value="{$adminLangParams.MENU_CONTENT_CATEGORIES}" />
<input type="hidden" id="menu_item_1_7" name="menu_item_1_7" value="{$adminLangParams.MENU__TRANSLATION_THEME}" />

<input type="hidden" id="menu_item_2_1" name="menu_item_2_1" value="{$adminLangParams.MENU_CONTENT_PRICE}" />
<input type="hidden" id="menu_item_2_2" name="menu_item_2_2" value="{$adminLangParams.MENU_CONTENT_FILES}" />
<input type="hidden" id="menu_item_2_3" name="menu_item_2_3" value="{$adminLangParams.MENU_PRODUCTS_DESIGN}" />

<input type="hidden" id="menu_item_3_1" name="menu_item_3_1" value="{$adminLangParams.MENU_ORDERS_ALL}" />
<input type="hidden" id="menu_item_3_2" name="menu_item_3_2" value="{$adminLangParams.MENU_ORDERS_ACCEPTED}" />

<input type="hidden" id="menu_item_language" name="menu_item_language" value="{$adminLangParams.MENU_LANGUAGE}" />

<script type="text/javascript">
	var menu_item_1_1 = $("#menu_item_1_1").val();
	var menu_item_1_2 = $("#menu_item_1_2").val();
	var menu_item_1_3 = $("#menu_item_1_3").val();
	var menu_item_1_4 = $("#menu_item_1_4").val();
	var menu_item_1_5 = $("#menu_item_1_5").val();
	var menu_item_1_6 = $("#menu_item_1_6").val();
	var menu_item_1_7 = $("#menu_item_1_7").val();		

	var menu_item_2_1 = $("#menu_item_2_1").val();
	var menu_item_2_2 = $("#menu_item_2_2").val();
	var menu_item_2_3 = $("#menu_item_2_3").val();

	var menu_item_3_1 = $("#menu_item_3_1").val();
	var menu_item_3_2 = $("#menu_item_3_2").val();

	var menu_item_language = $("#menu_item_language").val();
</script>