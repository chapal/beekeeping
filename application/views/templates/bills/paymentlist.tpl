<div class="container paymentpopup" style="background: rgba(255,255,255,0.9);padding: 20px;margin: 0 auto;margin-top: -200px;z-index: 9;position: relative">
    <div class="row-fluid">
        <div class="span12">
            <h1>Выберите удобный способ оплаты</h1>
            <span alt='7' class="robokassaPay" style="margin: 5px;"><a href="#"><img src="/application/views/templates/bills/assets/img/logo.gif"></a></span>
            <span alt='5' style="margin: 5px;"><a href="#"><img src="/application/views/templates/bills/assets/img/pay_wmz.png"></a></span>
            <span alt='4' style="margin: 5px;"><a href="#"><img src="/application/views/templates/bills/assets/img/cc.png"></a></span>
            <span alt='1' style="margin: 5px;"><a href="#"><img src="/application/views/templates/bills/assets/img/pay_easypay.gif"></a></span>
            <span alt='3' style="margin: 5px;"><a href="#"><img src="/application/views/templates/bills/assets/img/pay_ipay_life.gif"></a></span>
            <span alt='2' style="margin: 5px;"><a href="#"><img src="/application/views/templates/bills/assets/img/pay_ipay_mts.gif"></a></span>
            <span alt='6' style="margin: 5px;"><a href="#"><img src="/application/views/templates/bills/assets/img/pay_wmb.png"></a></span>



        </div>
    </div>
</div>
{literal}
<script>
    (function($){

        $(document).ready(function(){
            $(document).click(function(event){
                if($('.paymentpopup').is(':visible')) {
                    var clickedZone = $(event.target).parents('.paymentpopup');
                    if(!clickedZone.hasClass('paymentpopup')) {
                        $('.paymentpopup').fadeOut(function(){
                            $(document).off('click');
                        });
                    }
                }
            });

            $('.paymentpopup').on('click','span',function(event){
                event.preventDefault();
                if($(this).hasClass('robokassaPay')) {
                    $.ajax({
                        type:'POST',
                        url:'/bills/index/robokassapay',
                        dataType:'html',
                        data:{'billInfo':{/literal}{$billJSON}{literal}},
                        success:function(response){
                            $(response).hide().appendTo('body').submit();

                        }
                    });
                } else {

                    var payType = $(this).attr('alt');
                    $.ajax({
                        type:'POST',
                        url:'/bills/index/pay',
                        dataType:'html',
                        data:{'billInfo':{/literal}{$billJSON}{literal},'payment_system':payType},
                        success:function(response){
                            $(response).hide().appendTo('body').submit();

                        }
                    });
                }

            });
        });
    })(jQuery);
</script>
{/literal}