<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Счёт 1589 от 25.04.2014</title>
    <link rel="stylesheet" type="text/css" href="/application/views/templates/bills/assets/style.css" />
</head>

<body>
<table>
    <tbody><tr>
        <td>
            <img src="/tmp/companies/{$billInfo.companylogo}" alt="logo" width="100" align="left"></td>
        <td>
            <p><strong>
                    {$billInfo.companyname|stripcslashes}<br>
                    {$billInfo.postal_address} {$billInfo.legal_address}
                </strong></p></td></tr>
    </tbody></table>
<br>
<table class="vis w100">
    <tbody><tr>
        <td>Получатель: {$billInfo.companyname|stripcslashes}</td>
        <td>Р.сч.№</td>
        <td>{$billInfo.account}</td></tr>
    <tr>
        <td rowspan="{if $billInfo.region=='belorussia'}2{elseif $billInfo.region=='russia'}3{/if}">{$billInfo.payee_bank|stripcslashes}</td>
        {if $billInfo.region=='belorussia'}
            <td>УНП</td>
            <td>{$billInfo.unp}</td>
        {/if}
        {if $billInfo.region=='russia'}
            <td>ИНН</td>
            <td>{$billInfo.inn}</td>
        {/if}
    </tr>
    {if $billInfo.region=='belorussia'}
    <tr>
        <td>ОКПО</td>
        <td>{$billInfo.okpo}</td>
    </tr>
    {/if}
    {if $billInfo.region=='russia'}
        <tr>
            <td>ОГРН</td>
            <td>{$billInfo.ogrn}</td>
        </tr>
    {/if}
    {if $billInfo.region=='russia'}
        <tr>
            <td>КПП</td>
            <td>{$billInfo.kpp}</td>
        </tr>
    {/if}
    </tbody></table>

<h1>Счет № {$billInfo.billid} от {$billInfo.created|date_format:"%d.%m.%Y"}</h1>
<table>
    <tbody><tr><th class="ar at">Плательщик:</th><td>{$billInfo.first_name} {$billInfo.last_name}</td></tr>
    <tr><th class="ar at">Логин:</th><td>{$billInfo.email}</td></tr>
    <tr><th class="ar at">Телефон:</th><td>{$billInfo.phone}</td></tr>
    <tr><th class="ar at">Адрес:</th><td><input style="border: none;margin-top: -1px;" type="text" value="Напишите адресс" autofocus="" /></td></tr>
    </tbody></table>
<br>

<table class="vis w100">
    <tbody><tr>
        <td class="ar" style="width:2%">№</td>
        <td style="width:80%">Наименование товара/услуги</td>
        <td class="ar" style="width:5%">Сумма</td>
        <td class="ar" style="width:5%">Валюта</td>
    </tr>

    <tr>
        <td class="ar">1</td>
        <td>
            {if $billInfo.type=='offer'} {$billInfo.offer} {elseif  $billInfo.type=='products' } {$billInfo.title} {elseif  $billInfo.type=='services' }{$serviceInfo.servicename} {$serviceInfo.productname}{/if}
        </td>
        <td class="ar"><nobr>{$billInfo.price}</nobr></td>
        <td class="ar"><nobr>{$billInfo.currency}</nobr></td>

    </tr>
    <tr>
        <td colspan="3" class="ar"><p><b>Итого к оплате</b></p></td>
        <td class="ar"><nobr><b>{$billInfo.price} {$billInfo.currency}</b></nobr></td>
    </tr>
    </tbody></table>

<p>
    В платёжном поручении в графе «Назначение платежа» обязательно указывайте номер оплачиваемого счета <b>{$billInfo.billid}</b>.
    А также <span style="color: red;"><b>ОБЯЗАТЕЛЬНО</b></span> указывайте в системе ваш логин: <span style="color: red;"><b>{$billInfo.email}</b></span>
</p>

<table class="stamp">
    <tbody><tr>
        <td>{$billInfo.position} {$billInfo.companyname|stripcslashes}</td>
        <td><img src="/tmp/companies/{$billInfo.companyprint}" alt="" border="0" width="100"></td>
        <td>{$billInfo.fio}</td>
    </tr>
    </tbody></table>

<ul class="small">
    <li>Этот счёт действителен в течение {$endDays} банковских дней.</li>
    <li>Если у вас есть вопросы по оплате этого счёта, вы можете связаться с нами по контактам, указанным на сайте <a style="text-decoration: none; color: #000;" href="http://antalika.com/">http://antalika.com</a>
        <br/>
        {if $billInfo.region=='russia'}
            +7 (499) 703-3778
            +7 (495) 664-3550
        {/if}
        {if $billInfo.region=='belorussia'}
            +375 (29) 333-1223<br/>
            +375 (17) 395-5007 </li>
        {/if}
</ul>

</body>
</html>