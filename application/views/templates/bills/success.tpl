<html lang="en"><!--<![endif]--><!-- BEGIN HEAD --><head>
    <meta charset="utf-8">
    <title>Metronic | Pages - Coming Soon</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/css/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/css/style-metro.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/css/style-responsive.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="/css/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/css/assets/css/pages/coming-soon.css" rel="stylesheet" type="text/css">
    <!-- END PAGE LEVEL STYLES -->
    <link rel="shortcut icon" href="favicon.ico">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<div class="container ">
    <div class="row-fluid">
        <div class="span12 coming-soon-header">
            <a class="brand" href="index.html">
                <img src="/css/assets/img/logo-big.png" alt="logo">
            </a>
        </div>
    </div>
    <div class="row-fluid">

        <div class="span12 coming-soon-content">
            {if !isset($noBill)}
                <h1>Оплата прошла успешно!</h1>
                <p>Ваша оплата прошла успешно</p>
                <p> <a href="/" class="btn green big">Перейти на сайт <i class="icon-arrow-right m-icon-white"></i></a> </p>
                <br>
            {else}
                <h1 style="color:#FFF">Счет не обнаружен</h1>
                <p style="color:#FFF">Извините, вероятно произошла какая-то ошибка, текущего счета не обнаружено</p>

            {/if}

        </div>

    </div>
    <!--/end row-fluid-->
    <div class="row-fluid">
        <div class="span12 coming-soon-footer" style="color: #F2F2F2">
            2013 © Antalika. Furure Technologies.
        </div>
    </div>


</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="/css/assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/css/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="/css/assets/plugins/excanvas.min.js"></script>
<script src="/css/assets/plugins/respond.min.js"></script>
<![endif]-->
<script src="/css/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/css/assets/plugins/countdown/jquery.countdown.js" type="text/javascript"></script>
<script src="/css/assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/css/assets/scripts/app.js" type="text/javascript"></script>
<script src="/css/assets/scripts/coming-soon.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->

<div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 100%; width: 1920px; z-index: -999999; position: fixed;"><img src="/css/assets/img/bg/3.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1920px; height: 1439.7378481703988px; max-width: none; z-index: -999999; left: 0px; top: -435.8689240851994px;"></div></body><!-- END BODY --></html>