<html lang="en"><!--<![endif]--><!-- BEGIN HEAD --><head>
    <meta charset="utf-8">
    <title>Metronic | Pages - Coming Soon</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/css/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/css/style-metro.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/css/style-responsive.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="/css/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/css/assets/css/pages/coming-soon.css" rel="stylesheet" type="text/css">
    <!-- END PAGE LEVEL STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
    {literal}
        <style>
            a.brand {font-size: 22px; color: #FFF}
            a.brand:hover   {color: #FFF;text-decoration: none;}

            table tr td,table tr th {background: rgba(255,255,255,0.3) !important;color: #FFF}
        </style>
    {/literal}


</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body style="background: url('/images/bills/paper_02.png')">
<div class="container " style="background: url('/images/bills/paper_01.png');padding: 15px; box-shadow: 0px 1px 10px #666;">
    <div class="row-fluid">
        <div class="span12 coming-soon-header">
            <a class="brand" href="index.html">
                <img src="/tmp/companies/{$companyInfo.logo}" alt="logo" width="130">

            </a>
        </div>
    </div>
    {if !isset($noBill)}
    <div class="row-fluid">
        <div class="span6 coming-soon-content">
            <h1 style="color: #000">Информация по счету!</h1>
            <p style="color: #000">Вам был выставлен счет, вы можете оплатить его через любую из удобных вам платежных систем, распечатать или скачать.</p>
            <p style="color: #000">Обратите внимание что счет должен быть олачен в срок до: {$billInfo.due_date|date_format:"%d.%m.%Y"}</p>
            <br>

        </div>
        <div class="span6 coming-soon-countdown" style="color: #000">
            <div id="defaultCountdown" class="hasCountdown" style="color: #000">
                <span class="countdown_row countdown_show4" style="color: #000">
                    <h3 style="color: #000">Осталось</h3>
                    <span class="countdown_section" style="color: #000; border:1px solid #666">
                        <span class="countdown_amount">{$endDays}</span>
                        <br>Дней
                    </span>

                </span>
            </div>
        </div>
    </div>
        <div class="row-fluid">
            <div class="span8">
                <table class="table table-striped table-bordered table-hover" style="color: #000" >
                    <thead>
                        <tr>
                            <th style="color: #000">Наименование</th>
                            <th style="color: #000">Стоимость</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="color: #000">
                                {if $billInfo.type=='offer'} {$billInfo.offer} {elseif  $billInfo.type=='products' } {$billInfo.title} {elseif  $billInfo.type=='services' }{$serviceInfo.servicename} {$serviceInfo.productname}{/if}
                            </td>
                            <td style="color: #000">
                               {$billInfo.price} {$billInfo.currency}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <div class="row-fluid">
        <div class="span8 ">
            <a class="btn green big to-pay">ОПЛАТИТЬ <i class="icon-credit-card m-icon-white"></i></a>
            <a class="btn blue big to-print">РАСПЕЧАТАТЬ <i class="icon-print m-icon-white"></i></a>
            <a href="{$lingToPDF}" class="btn purple big to-pdf">СКАЧАТЬ(PDF) <i class="icon-download-alt m-icon-white"></i></a>
        </div>
    </div>
    {else}
        <div class="row-fluid">
            <div class="span12 coming-soon-countdown">
                <h1 style="color:#FFF">Счет не обнаружен</h1>
                <p style="color:#FFF">Извините, вероятно произошла какая-то ошибка, текущего счета не обнаружено</p>
            </div>
        </div>
    {/if}
    <!--/end row-fluid-->
    <div class="row-fluid">
        <div class="span12 coming-soon-footer" style="color: #F2F2F2">
            2013 © Antalika. Furure Technologies.
        </div>
    </div>


</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="/css/assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/css/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="/css/assets/plugins/excanvas.min.js"></script>
<script src="/css/assets/plugins/respond.min.js"></script>
<![endif]-->
<script src="/css/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="/css/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/css/assets/plugins/countdown/jquery.countdown.js" type="text/javascript"></script>
<script src="/css/assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/css/assets/scripts/app.js" type="text/javascript"></script>
<script src="/css/assets/scripts/coming-soon.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
{literal}
<script>
    jQuery(document).ready(function() {
        //App.init();
        //CoomingSoon.init();
    });

    (function($){

        $(document).ready(function(){
           $('.to-print').on('click',function(event){
               event.preventDefault();
               var params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=700,height=900,left=0,top=0';
               var printWindow = window.open({/literal}"{$lingToPrint}"{literal},'test',params);
           });

            $('.to-pay').on('click',function(event){
                if($('.paymentpopup').length === 0) {
                    $.ajax({
                       type:'POST',
                       url:'/bills/index/paylist',
                       dataType:'html',
                       data:{'num':{/literal}"{$link}"{literal}},
                       success:function(response){
                            $(response).hide().appendTo('body').fadeIn();
                       }
                    });
                } else {
                    $('.paymentpopup').fadeIn(function(){
                        $(document).click(function(event){
                            if($('.paymentpopup').is(':visible')) {
                                var clickedZone = $(event.target).parents('.paymentpopup');
                                if(!clickedZone.hasClass('paymentpopup')) {
                                    $('.paymentpopup').fadeOut(function(){
                                        $(document).off('click');
                                    });
                                }
                            }
                        });
                    });
                }
            });




        });
    })(jQuery);



</script>
{/literal}
<!-- END JAVASCRIPTS -->

{*<div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 100%; width: 1920px; z-index: -999999; position: fixed;"><img src="/css/assets/img/bg/3.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1920px; height: 1439.7378481703988px; max-width: none; z-index: -999999; left: 0px; top: -435.8689240851994px;"></div></body><!-- END BODY --></html>*}