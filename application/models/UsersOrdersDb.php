<?php
include_once ROOT . 'application/models/BaseModel.php';

class UsersOrdersDb extends BaseModel
{
    public $tableName = 'users_orders';

    public function __construct()
    {
        $this -> db = Zend_Registry::get('db');
    }

    public function makeFilter($data)
    {
        foreach($data as $key=>$value):
            $_SESSION['FilterOrders'][$key]=$value;
        endforeach;
        $method = "make".ucfirst(trim($data['type']))."Filter";
        unset($data['type']);
        return $this->$method($data);
    }

    private function makeServiceFilter($data)
    {
        $producstSql =  '';
       if(isset($data['product_id']) && intval($data['product_id']) !== 0) {
           $producstSql=" AND ".$this->tableName.".product_id=".intval($data['product_id']);
       }

       $users = $this->db->fetchAll("SELECT users.*,COUNT(users_orders.id) as num FROM users INNER JOIN users_orders ON users_orders.user_id=users.id
       WHERE ".$this->tableName.".service_id=".intval($data['service_id'])." ".$producstSql." GROUP BY users.id");
       return $users;
    }

    private function makeProductFilter($data)
    {

       return $this->db->fetchAll("SELECT users.*,COUNT(users_orders.id) as num
                                FROM users
                                INNER JOIN users_orders ON  users_orders.user_id=users.id
                                WHERE ".$this->tableName.".product_id=".intval($data['product_id'])." AND service_id IS NULL GROUP BY users.id");
    }
}