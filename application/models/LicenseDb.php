<?php
include_once ROOT . 'application/models/BaseModel.php';
class LicenseDb extends BaseModel
{
    public $tableName = 'license';

    public function __construct()
    {
        $this -> db = Zend_Registry::get('db');
    }

    public function getLicense()
    {
        return $this->db->fetchRow('SELECT * FROM license');
    }

}