<?php
include_once ROOT . 'application/models/BaseModel.php';

class MailsDb extends BaseModel
{
    public $tableName = 'mails';

    public function __construct()
    {
        $this -> db = Zend_Registry::get('db');
    }

    public function findCategories()
    {
        return $this->db->fetchAll("SELECT * FROM mails_category");
    }

    public function saveCategory($data)
    {
        $action = "INSERT INTO ";
        $sql = " `title`='".$data['title']."'";
        if(isset($data['id'])) {
            $sql.= ' WHERE id='.intval($data['id']);
            $action = " UPDATE ";
        }

        $this->db->exec($action." mails_category  SET ".$sql);

    }

    public function checkrecovery()
    {
        $mails = $this->db->fetchRow("SELECT COUNT(id) as num FROM ".$this->tableName." WHERE type IN ('sendbill','remindbill','debloyedbill')");
        if(intval($mails['num']) !== 3) {
            return true;
        } else {
            return false;
        }
    }

    public function getContentByType($type)
    {
        return $this->db->fetchRow("SELECT `content` FROM ".$this->tableName." WHERE type='".$type."'");
    }

    public function recoveryMails()
    {
        $this->db->exec("DELETE FROM ".$this->tableName." WHERE type IN ('sendbill','remindbill','debloyedbill')");
        $this->db->exec("INSERT INTO mails_category SET `title`='Счета'");
        $categoryId = $this->db->lastInsertId();
        $this->db->exec("INSERT INTO ".$this->tableName."
                        SET `title`='Выставление счета',
                        `content`='Вам выставили счет, перейдите по ссылке %link%',
                        `date_created`=NOW(),
                        `date_modify`=NOW(),
                        `category_id`=".$categoryId.",
                        `type`='sendbill'");
        $this->db->exec("INSERT INTO ".$this->tableName."
                        SET `title`='Напоминание об оплате счета',
                        `content`='Осталось %days% дней до оплаты.Перейдите по следующей ссылке для большего ознакомления %link%',
                        `date_created`=NOW(),
                        `date_modify`=NOW(),
                        `category_id`=".$categoryId.",
                        `type`='remindbill'");
        $this->db->exec("INSERT INTO ".$this->tableName."
                        SET `title`='Просрочка оплаты',
                        `content`='Вы не оплатили счет вовремя.Ваши услуги временно приостановлены',
                        `date_created`=NOW(),
                        `date_modify`=NOW(),
                        `category_id`=".$categoryId.",
                        `type`='debloyedbill'");
    }

    public function save($data)
    {
        $this->is_new = (isset($data['id'])) ? false : true;
        $set = '';
        foreach($data as $column=>$value):
            $set.="`".$column."`='".$value."',";
        endforeach;
        $set = substr($set,0,-1);
        $action = ($this->is_new) ? 'INSERT INTO' : 'UPDATE';
        $where = ($this->is_new) ? '' : ' WHERE `id`='.intval($data['id']);
        $result = $this->db->query($action." ".$this->tableName." SET ".$set.$where);
        $this->postSave(($this->is_new === false) ? $data['id'] : null);
        return ($this->is_new) ? $this->db->lastInsertId() : $result;
    }

    public function getItemsForPage($page,$limit = -1)
    {
        $limit = $limit == -1?self::COMPANY_PER_PAGE:$limit;

        return $this -> db -> fetchAll('SELECT '.$this->tableName.' .*,mails_category.title as categorytitle,mails_category.id as categoryid FROM '.$this->tableName.'
                                        INNER JOIN mails_category ON '.$this->tableName.'.category_id=mails_category.id
                                        ORDER BY id LIMIT '.($page*$limit).', '.$limit);
    }

    public function getCategoryById($id)
    {
        $id = intval($id);
        return $this->db->fetchRow("SELECT * FROM mails_category WHERE id=".$id);
    }

    public function deleteCategory($id)
    {
        return $this->db->exec("DELETE FROM mails_category WHERE id=".intval($id));
    }
}