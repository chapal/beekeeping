<?php
include_once ROOT . 'application/models/Filter.php';

include_once ROOT. 'application/models/FilterInterface.php';

abstract class BaseModel
{

    const COMPANY_PER_PAGE  = 20;
    const PAYMENT_TRANSACTIONS_TABLE = 'transactions';
    public $tableName;
    protected $db;
    protected $tmpData;
    protected $is_new=true;

    public function __construct()
    {
        $this -> db = Zend_Registry::get('db');
    }

    public function save($data=array())
    {
        $this->is_new = (isset($data['id'])) ? false : true;
        $data = $this->cleanData($data);
        $set = '';
        foreach($data as $column=>$value):
            $set.="`".$column."`='".$value."',";
        endforeach;
        $set = substr($set,0,-1);
        $action = ($this->is_new) ? 'INSERT INTO' : 'UPDATE';
        $where = ($this->is_new) ? '' : ' WHERE `id`='.intval($data['id']);
        $result = $this->db->query($action." ".$this->tableName." SET ".$set.$where);
        $this->postSave(($this->is_new === false) ? $data['id'] : null);
        return ($this->is_new) ? $this->db->lastInsertId() : $result;
    }

    public function getExchangedPrice($currentPrice,$currency)
    {
        $convertedPrice = '';
        $oneUSD = '';
        $oneRUR='';
        $oneEUR='';
        $xml = simplexml_load_file('http://www.nbrb.by/Services/XmlExRates.aspx?ondate='.date("m/d/Y"));
        foreach($xml as $f) {

            if($f->CharCode =='USD') {
                $oneUSD=$f->Rate;
            }
            if($f->CharCode =='RUB') {
                $oneRUR=$f->Rate;
            }
            if($f->CharCode =='EUR') {
                $oneEUR=$f->Rate;
            }


        }


        $totalPriceInBYR = intval($oneUSD)*floatval($currentPrice);

        switch ($currency):
            case 'RUR':
                $convertedPrice = floatval($totalPriceInBYR/intval($oneRUR));
                break;
            case 'EUR':

                $convertedPrice = floatval($totalPriceInBYR/intval($oneEUR));
                break;
            case 'BYR':
                $convertedPrice = $totalPriceInBYR;
                break;
            case 'USD':
                $convertedPrice = $currentPrice;
                break;

        endswitch;
        $convertedPrice= @number_format($convertedPrice,2);
        return $convertedPrice;
    }

    public function ckeckFilter($filter,$isWhere=true)
    {
        $filterSql='';
        if(isset($_SESSION['Filter']['active']) AND $_SESSION['Filter']['active']==1 AND $filter===false) {
            //Тут будет костыльная опеция по выявлению даты

            foreach($_SESSION['Filter'] as $column=>$value) {
                if(preg_match("/date([A-Za-z0-9_])+from/",$column)) {
                    $newColumn = str_replace("_from","->from",$column);
                    $_SESSION['Filter'][$newColumn]=$value;
                    unset($_SESSION['Filter'][$column]);
                }
                if(preg_match("/date([A-Za-z0-9_])+to/",$column)) {
                    $newColumn = str_replace("_to","->to",$column);
                    $_SESSION['Filter'][$newColumn]=$value;
                    unset($_SESSION['Filter'][$column]);
                }
            }
            $_SESSION['Filter']['model']=$_SESSION['Filter']['model'];
            $filter = new Filter( $_SESSION['Filter']);
        }
        if( $filter instanceof Filter ) {
            if($filter->isActive() != 0) {
                $sql = $filter->getFilterSql();
                if(!empty($sql)) {
                    $where = ($isWhere === true) ? " WHERE " : " ";
                    $filterSql .= $where. $filter->getFilterSql();
                }
            }
        }
        return $filterSql;
    }

    public function delete($id)
    {
        $id = intval($id);
        $this->tmpData = $this->db->fetchRow("SELECT * FROM ".$this->tableName." WHERE `id`=".$id);
        $this->db->query("DELETE FROM ".$this->tableName." WHERE `id`=".$id);
        $this->postDelete();
    }

    public function cleanData($data=array())
    {
        $cleanedData = array();
        foreach($data as $key=>$value):
            $cleanedData[addslashes(trim($key))]=addslashes(strip_tags(trim($value)));
        endforeach;
        return $cleanedData;
    }

    public function postDelete(){}

    public function postSave(){}



    public function __call($method,array $params)
    {

        if(preg_match("/(One|All)/",str_replace("find","",$method),$matches)) {
            if($matches[0]==="One") {
                $byWhat = str_replace("findOneBy","",$method);
                $byWhat = strtolower($byWhat);
                return $this->db->fetchRow("SELECT * FROM ".$this->tableName." WHERE `".$byWhat."`='".$params[0]."'");
            }
            if($matches[0]==="All") {
                if(preg_match("/findAllBy/",$method)) {
                    $byWhat = str_replace("findAllBy","",$method);
                    $byWhat = strtolower($byWhat);
                    return $this->db->fetchAll("SELECT * FROM ".$this->tableName." WHERE `".$byWhat."`='".$params[0]."'");
                }elseif(preg_match("/^findAll$/",$method)) {
                    return $this->db->fetchAll("SELECT * FROM ".$this->tableName);
                }
            }
        }

    }


    public function getItemsForPage($page_num, $limit = -1) {
        $limit = $limit == -1?self::COMPANY_PER_PAGE:$limit;

        return $this -> db -> fetchAll('SELECT * FROM '.$this->tableName.' ORDER BY id LIMIT '.($page_num*$limit).', '.$limit);
    }

    public function getItemsPagesCount() {
        $count = $this -> db -> fetchRow('SELECT count(*) as count FROM '.$this->tableName);
        return ceil($count['count']/self::COMPANY_PER_PAGE);
    }

}