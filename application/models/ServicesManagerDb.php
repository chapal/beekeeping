<?php
include_once ROOT . 'application/models/BaseModel.php';
class ServicesManagerDb extends BaseModel
{
    public $tableName = 'services_manager';

    public function __construct()
    {
        $this -> db = Zend_Registry::get('db');
    }

    public function getItemsForPage($page_num, $limit = -1,$unlimit=false)
    {
        $limit = $limit == -1?self::COMPANY_PER_PAGE:$limit;
        if($unlimit === false) { $limitstr = 'LIMIT '.($page_num*$limit).', '.$limit;} else {$limitstr='';}
        return $this -> db -> fetchAll('SELECT '.$this->tableName.'.*,products.title as productname,services.name as servicename FROM '.$this->tableName.'
                                LEFT JOIN products ON '.$this->tableName.'.product_id=products.id
                                INNER JOIN services ON '.$this->tableName.'.service_id=services.id ORDER BY '.$this->tableName.'.id '.$limitstr);
    }

    public function getServicesInfo()
    {

        return $this->db->fetchAll("SELECT ".$this->tableName.".price,
                                           ".$this->tableName.".period,
                                           ".$this->tableName.".period_type,
                                           ".$this->tableName.".id,
                                           products.title,
                                           products.id as productid
                                           FROM ".$this->tableName."
                                           INNER JOIN products ON ".$this->tableName.".product_id=products.id
                                           ");
    }

    public function getByServiceId($id,$products=false)
    {
        $join = ($products) ? 'INNER' : 'LEFT';

        return $this->db->fetchAll("SELECT services.name,".$this->tableName.".*,products.id as productid,products.title FROM
                                    ".$this->tableName." INNER JOIN services ON ".$this->tableName.".service_id=services.id
                                    ".$join." JOIN products ON ".$this->tableName.".product_id=products.id WHERE ".$this->tableName.".service_id=".$id);
    }

}