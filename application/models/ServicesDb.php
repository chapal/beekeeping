<?php
include_once ROOT . 'application/models/BaseModel.php';
class ServicesDb extends BaseModel
{
    public $tableName = 'services';

    public function __construct()
    {
        $this -> db = Zend_Registry::get('db');
    }

}