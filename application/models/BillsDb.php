<?php
include_once ROOT . 'application/models/BaseModel.php';

class BillsDb extends BaseModel
{
    public $tableName = 'bills';

    public function __construct()
    {
        $this -> db = Zend_Registry::get('db');
    }

    public function makeCopyOfBill($id)
    {
        $currentBillInfo = $this->db->fetchRow("SELECT * FROM bills WHERE `id`=".$id);
        unset($currentBillInfo['id']);
        $currentBillInfo['status']='draft';
        $set = '';
        foreach($currentBillInfo as $column=>$value) {
            $set .="`".$column."`='".$value."',";
        }
        $set = substr($set,0,-1);
        return $this->db->exec("INSERT INTO bills SET ".$set);
    }


    public function makeOrderFront($billId)
    {
        $getOrdeId = $this->db->fetchRow("SELECT `order_id`,`user_id` FROM bills WHERE id=".intval($billId));
        $orderData = $this->db->fetchRow("SELECT `products` FROM orders WHERE `id`=".intval($getOrdeId['order_id']));
        $productArray = json_decode($orderData['products'],true);
        foreach($productArray as $key=>$value) {
            for($i=0; $i<$value['count'];$i++) {
                $this->db->exec("INSERT INTO users_orders(user_id,product_id) VALUES(".intval($getOrdeId['user_id']).",".intval($value['id']).")");
            }
        }
        $this->db->exec("UPDATE orders SET payed=1,status=1 WHERE `id`=".intval($getOrdeId['order_id']));
        $this->db->exec("DELETE FROM bills WHERE id=".intval($billId));

    }

    public function isOrder($billId)
    {
        $check= $this->db->fetchRow("SELECT `order_id` FROM bills WHERE id=".intval($billId));

        if($check['order_id']) {
            return true;
        }else {
            return false;
        }

    }

    public function makeOrder()
    {
        //Get Data about the order
        $shoppingCart = $_SESSION['shopping_cart'];
        $totalPrice=0;
        $cd_count=0;
        $productsIN = array();
        $i=0;
        foreach($shoppingCart as $currentItem):
            $totalPrice+=floatval($currentItem['total_price']);
            $cd_count+=intval($currentItem['count']);
            $productsIN[$i]['id']=intval($currentItem['id']);
            $productsIN[$i]['count']=intval($currentItem['count']);

            $i++;
        endforeach;
        //Get current user ID, who makes the order
        $user_id = intval($_SESSION['loginNamespace']['storage']->id);
        //insert chat info into order table
        $products = json_encode($shoppingCart);
        //Replace all ' on " for valid json
        $products = str_replace('\'','"',$products);
        $insert="INSERT INTO orders(cd_count,price,total_price,sprice,user_id,products) VALUES(".$cd_count.",'".$totalPrice."','".$totalPrice."','".$totalPrice."',".$user_id.",'".json_encode($productsIN)."')";
        try {
         $this->db->exec($insert);
         $lastId = $this->db->lastInsertId();
         $this->db->exec("INSERT INTO bills(user_id,order_id) VALUES(".$user_id.",".$lastId.")");
            $_SESSION['bills']['amount']=$totalPrice;
            $lastId = $this->db->lastInsertId();
            $_SESSION['bills']['bill_id']=$lastId;
            $_SESSION['bills']['front']=1;
           return array('bill_id'=>$lastId,'amount'=>$totalPrice);
        } catch(Zend_Db_Statement_Exception $e) {
           return $e->getMessage();
        }

    }

    public function checkServiceBills()
    {
        return $this->db->fetchAll("SELECT bills.due_date,bills.paid_date,bills.id,bills.link,bills.email as sendmail,users.email,users.first_name,users.id as userid,companies.id as companyid,companies.name as companyname,
                                  services_manager.period,services_manager.period_type
                                  FROM bills INNER JOIN users ON bills.user_id=users.id
                                             INNER JOIN companies ON bills.company_id=companies.id
                                             INNER JOIN services_manager ON bills.service_id=services_manager.id
                                             WHERE bills.service_id IS NOT NULL
                                             ORDER BY bills.due_date DESC");
    }

    public function getServiceInfo($id)
    {
        return $this->db->fetchRow("SELECT services_manager.period,services_manager.period_type,bills.due_date,services_manager.service_id,services_manager.product_id FROM bills
                                    INNER JOIN services_manager ON bills.service_id=services_manager.id WHERE bills.id=".intval($id));
    }

    public function makeUserOrder($serviceInfo,$billInfo)
    {
        $productInsert = ($serviceInfo['product_id']) ? ", `product_id`=".intval($serviceInfo['product_id']) : '';
        $this->db->exec("INSERT INTO users_orders SET `user_id`=".intval($billInfo['user_id']).",`service_id`=".intval($serviceInfo['service_id']).$productInsert);
    }

    public function getAllBills($filter=false)
    {
        $filterSql = $this->ckeckFilter($filter);

        $data = $this->db->fetchAll("SELECT bills.*,users.email,users.first_name,users.id as userid,companies.id as companyid,companies.name as companyname,products.price as productprice,
                                    services_manager.price as serviceprice
                                  FROM bills INNER JOIN users ON bills.user_id=users.id
                                             INNER JOIN companies ON bills.company_id=companies.id
                                             LEFT JOIN services_manager ON bills.service_id=services_manager.id
                                             LEFT JOIN products ON bills.product_id=products.id".$filterSql."

                                             ORDER BY bills.created DESC");
        $newData = array();
        foreach($data as $key=>$bill) :
            if($bill['service_id']) {
                $bill['billamount'] = $this->getExchangedPrice(($bill['serviceprice']),$bill['currency']);
            }else {
                $bill['billamount'] = $this->getExchangedPrice(($bill['price']) ? $bill['price'] : $bill['productprice'],$bill['currency']);
            }

            $bill['billlink'] = "http://".$_SERVER['HTTP_HOST']."/bills/index?num=".$bill['link'];
            $newData[$key]=$bill;
        endforeach;
        return $newData;
    }



    public function getInfoAboutService($id)
    {
        return $this->db->fetchRow("SELECT services.name as servicename,products.title as productname,services_manager.price FROM services_manager
                                   INNER JOIN services ON services_manager.service_id=services.id
                                   LEFT JOIN products ON services_manager.product_id=products.id
                                   WHERE services_manager.id=".intval($id));
    }

    public function getByLink($link)
    {
        $data= $this->db->fetchRow("SELECT bills.*,users.email,users.first_name,users.id as userid,
                                   bills.id as billid,
                                   companies.id as companyid,
                                   companies.name as companyname,
                                   companies.logo as companylogo,
                                   bills.link as billlink,
                                   companies.print as companyprint,
                                   companies.*
                                   FROM bills INNER JOIN users ON bills.user_id=users.id
                                             INNER JOIN companies ON bills.company_id=companies.id
                                             WHERE bills.`link`='".$link."'");
        if(!is_null($data['product_id'])) {
            $productData = $this->db->fetchRow("SELECT products.* FROM products WHERE `id`=".intval($data['product_id']));
            foreach($productData as $key=>$value):
                $data[$key]=$value;
            endforeach;
        }

        if(!is_null($data['service_id'])) {
            $productData = $this->db->fetchRow("SELECT services_manager.price FROM services_manager WHERE `id`=".intval($data['service_id']));
            $data['price']=$productData['price'];
        }

        return $data;
    }

    public function getByPk($id)
    {
        $data= $this->db->fetchRow("SELECT bills.*,bills.email as billmail,bills.title as billtitle,users.email,users.first_name,users.id as userid,
                                   bills.id as billid,services.name as servicename,services_manager.price as serviceprice,
                                   companies.id as companyid,
                                   companies.name as companyname,
                                   companies.logo as companylogo,
                                   companies.print as companyprint,
                                   bills.link as billlink,
                                   companies.*
                                   FROM bills INNER JOIN users ON bills.user_id=users.id
                                             INNER JOIN companies ON bills.company_id=companies.id
                                             LEFT JOIN services_manager ON bills.service_id=services_manager.id
                                             LEFT JOIN services ON services_manager.service_id=services.id
                                             WHERE bills.`id`=".$id."");
        if(intval($data['product_id'])) {
            $productData = $this->db->fetchRow("SELECT products.* FROM products WHERE `id`=".intval($data['product_id']));
            foreach($productData as $key=>$value):
                $data[$key]=$value;
            endforeach;
        }

        return $data;
    }

}