<?php



class Filter
{

    private $isActive=0;
    private $filterData = array();
    public $filterModel;

    public function __construct(array $filter)
    {
        if(!is_array($filter)) {
            throw new Exception('Filter must be an array');
        }
        $this->isActive = $filter['active'];
        if($this->isActive == 1) {
            $_SESSION['Filter']['active']=1;
            $_SESSION['Filter']['model']=$filter['model'];
        } else {
            $this->clearFilter();
            return;
        }
        $this->filterModel = $filter['model'];
        unset( $filter['model']);unset($filter['active']);
        $this->setFilterData($filter);

    }



    private  function setFilterData($data)
    {
        $this->filterData = $data;

        foreach($this->filterData as $column=>$value) :
            $value = trim(strip_tags($value));
            if(preg_match("/->/",$column)){
                $column = str_replace("->","_",$column);
            }
            if(!empty($value)) {
                $_SESSION['Filter'][$column]=$value;
            } else {
                unset($_SESSION['Filter'][$column]);
            }
        endforeach;

    }

    public function getFilterData()
    {
        return $this->filterData;
    }

    public function getFilterTable()
    {
        $modelClass = ucfirst($this->filterModel).'Db';
        include_once __DIR__.'/'.$modelClass.'.php';
        $model = new $modelClass();
        return (isset($model->tableName)) ? $model->tableName : $model::BRANDS_TABLE;
    }

    public function isActive()
    {
        return $this->isActive;
    }

    public function getFilterSql()
    {
        $sql="";
        foreach($this->filterData as $column=>$value) :
            $value = trim(strip_tags($value));
            if(!empty($value) AND !preg_match("/->/",$column)) {
                $table = $this->getFilterTable();
                if(preg_match("/__/",$column)) {
                    $columnArray = explode("__",$column);
                    $column=$columnArray[0];
                    $table = $columnArray[1];
                }
                $sql.= " AND ".$table.".`".$column."`='".$value."' ";

            } elseif(!empty($value) AND preg_match("/->/",$column)){
                $table = $this->getFilterTable();
                $columnArray = explode("->",$column);
                $mainColumn = $columnArray[0];
                $operaton = ($columnArray[1] == 'to') ? "<=" : ">=";
                $valueArrays = explode("/",$value);
                $value = $valueArrays[2]."-".$valueArrays[1]."-".$valueArrays[0];
                $sql .= " AND ".$table.".`".$mainColumn."` ".$operaton." '".$value."' ";

            } else {
                if(preg_match("/__/",$column)) {
                    $columnArray = explode("__",$column);
                    $column=$columnArray[0];
                }
                unset($_SESSION['Filter'][$column]);
            }
        endforeach;
        $sql=substr($sql,4);
        return $sql;
    }

    public function clearFilter()
    {
        $this->filterData=array();
        $this->filterModel=NULL;
        $this->isActive=0;
        unset($_SESSION['Filter']);
        $_SESSION['Filter']['active']=0;
    }

}