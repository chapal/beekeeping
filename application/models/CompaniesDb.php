<?php
include_once ROOT . 'application/models/BaseModel.php';
class CompaniesDb extends BaseModel
{

    const CURRENCY_TABLE = 'currency';
    const COMPANY_PER_PAGE  = 20;
    const PAYMENT_TRANSACTIONS_TABLE = 'transactions';
    public $tableName = 'companies';
    protected $db;
    protected $tmpData;
    protected $is_new=true;

    public function __construct()
    {
        $this -> db = Zend_Registry::get('db');
    }

    public function save($data=array())
    {
        $this->is_new = (isset($data['id'])) ? false : true;
        $data = $this->cleanData($data);
        $set = '';
        foreach($data as $column=>$value):
            $set.="`".$column."`='".$value."',";
        endforeach;
        $set = substr($set,0,-1);
        $action = ($this->is_new) ? 'INSERT INTO' : 'UPDATE';
        $where = ($this->is_new) ? '' : ' WHERE `id`='.intval($data['id']);
        $result = $this->db->query($action." ".$this->tableName." SET ".$set.$where);
        $this->postSave(($this->is_new === false) ? $data['id'] : null);
        return ($this->is_new) ? $this->db->lastInsertId() : $result;
    }

    public function delete($id)
    {
        $id = intval($id);
        $this->tmpData = $this->db->fetchRow("SELECT * FROM ".$this->tableName." WHERE `id`=".$id);
        $this->db->query("DELETE FROM ".$this->tableName." WHERE `id`=".$id);
        $this->postDelete();
    }



    public function cleanData($data=array())
    {
        $cleanedData = array();
        foreach($data as $key=>$value):
            $cleanedData[addslashes(trim($key))]=addslashes(strip_tags(trim($value)));
        endforeach;
        return $cleanedData;
    }

    public function postDelete()
    {

        if(file_exists($this->getCompaniesDir().'/'.$this->tmpData['logo'])){
            unlink($this->getCompaniesDir().'/'.$this->tmpData['logo']);
        }
        if(file_exists($this->getCompaniesDir().'/'.$this->tmpData['print'])){
            unlink($this->getCompaniesDir().'/'.$this->tmpData['print']);
        }
    }


    public function addFile($name,$id=null)
    {
        if(!empty($_FILES['Companies']['tmp_name'][$name])) {
            $fileNameArray = explode(".",$_FILES['Companies']['name'][$name]);
            $fileName = 'company_'.$name.time().'.'.$fileNameArray[1];
            copy($_FILES['Companies']['tmp_name'][$name],$this->getCompaniesDir().'/'.$fileName);
            $this->db->query("UPDATE ".$this->tableName." SET `".$name."`='".$fileName."' WHERE `id`=".intval($id));
        }

    }

    public function postSave($id=null)
    {
        if(isset($_FILES['Companies']) AND !empty($_FILES['Companies'])) {
            if($this->is_new) {
                $lastId = $this->db->lastInsertId();
                $this->addFile('logo',$lastId);
                $this->addFile('print',$lastId);
            } else {
                //Delete previouse images and set the new
                if(!is_null($id)) {
                    $id = intval($id);
                    $currentRow = $this->db->fetchRow("SELECT * FROM ".$this->tableName." WHERE `id`=".$id);
                    if(isset($_FILES['Companies']['tmp_name']['logo']) AND !empty($_FILES['Companies']['tmp_name']['logo'])) {
                        if(file_exists($this->getCompaniesDir().'/'.$currentRow['logo'])) {
                            unlink($this->getCompaniesDir().'/'.$currentRow['logo']);
                        }
                        $this->addFile('logo',$id);
                    }
                    if(isset($_FILES['Companies']['tmp_name']['print']) AND !empty($_FILES['Companies']['tmp_name']['print'])) {
                        if(file_exists($this->getCompaniesDir().'/'.$currentRow['print'])) {
                            unlink($this->getCompaniesDir().'/'.$currentRow['print']);
                        }
                        $this->addFile('print',$id);
                    }

                }
            }
        }

    }

    public function getCompaniesDir()
    {
        $tmpDir = dirname(__FILE__).'/../../tmp';
        if(!is_dir($tmpDir.'/companies')) {
            mkdir($tmpDir.'/companies',0777);
        }

        return $tmpDir.'/companies';
    }

    public function getCompaniesForPage($lang_id, $page_num, $limit = -1) {
        $limit = $limit == -1?self::COMPANY_PER_PAGE:$limit;
        return $this -> db -> fetchAll('SELECT * FROM '.$this->tableName.' ORDER BY position LIMIT '.($page_num*$limit).', '.$limit);
    }

    public function getCompaniesPagesCount($lang_id) {
        $count = $this -> db -> fetchRow('SELECT count(*) as count FROM '.$this->tableName);
        return ceil($count['count']/self::COMPANY_PER_PAGE);
    }

}