<?php

include_once ROOT . 'application/models/AdminAreaControlModel.php';

include_once ROOT . 'application/models/BillsDb.php';

include_once ROOT . 'application/models/MailsDb.php';

include_once ROOT . 'application/models/CompaniesDb.php';

include_once ROOT . 'application/models/ProductsDb.php';

include_once ROOT . 'application/models/UsersDb.php';

include_once ROOT . 'application/models/ServicesManagerDb.php';

class Admin_CallbackController
{


    public function init()
    {
        parent::init();
    }

    public function delayInPayment($billId)
    {
        $billId = intval($billId);
        $billModel = new BillsDb();
        $billModel->save(array('id'=>$billId,'status'=>'delayed'));
        $mails = new MailsDb();
        $billinfo = $billModel->getByPk($billId);
        $mailsArray = $mails->getContentByType('debloyedbill');
        $mailBody = $mailsArray['content'];
        $emailTxt = new Zend_Config_Xml(ROOT.'configs/project/email.xml', 'email');
        Zend_Loader::loadClass('Zend_Mail');    /** Loading Zend_Mail */
        $mail = new Zend_Mail();
        $mail -> addHeader('X-MailGenerator', $_SERVER['HTTP_HOST'].' mail machine');
        $mailsObj = new MailsDb();
        $mail -> setBodyHtml($mailBody,'UTF-8');
        $mail -> setFrom('no-reply@'.$_SERVER['HTTP_HOST'], 'no-reply@'.$_SERVER['HTTP_HOST']);
        $mail -> addTo($billinfo['billmail'], $billinfo['first_name']);
        $subject = '=?UTF-8?B?'.base64_encode("Просрочка платежа ").'?=';
        $mail -> setSubject($subject);
        /** Send email */
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $mail->send();

    }

    public function successPayment()
    {
        die(var_dump('success payment'));
    }


}