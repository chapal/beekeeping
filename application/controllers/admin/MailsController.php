<?php

include_once ROOT . 'application/models/AdminAreaControlModel.php';
include_once ROOT . 'application/models/MailsDb.php';
/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_AdminAction');
class Admin_MailsController extends System_Controller_AdminAction
{

    private $mails;


    public function init()
    {
        parent::init();

        /** Check for user access */
        if(!AdminAreaControl::checkAccess()){
            $this -> _redirect('/admin');
        } else {
            $this -> mails = new MailsDb();
        }

        $this -> smarty -> assign('adminLeftMenu', 'mails');

    }

    public function recoverymailsAction()
    {
        $this->mails->recoveryMails();
        $this->_redirect('/admin/mails/index/page/1');
    }

    public function indexAction()
    {
        if(($this->_hasParam('page')&&$this->_getParam('page')==0)
            ||!$this->_hasParam('page')
            ||(($this->_hasParam('page')&&$this->_getParam('page')>1) && ($this -> mails -> getItemsPagesCount($this->lang_id)<=1 ))
            ||($this->_getParam('page')>1&&$this -> mails -> getItemsPagesCount($this->lang_id)<$this->_getParam('page'))
        ){
            $this->_redirect("/admin/services/index/page/1");
        }

        $page = $this->_hasParam('page')?((int)$this->_getParam('page')-1):0;
        $this->smarty->assign('categories',$this->mails->findCategories());
        $this -> smarty -> assign('items', $this->mails->getItemsForPage($page));
        $this -> smarty -> assign('countpage', $this->mails->getItemsPagesCount($this->lang_id));
        $this -> smarty -> assign('page',$page+1);
        $this->smarty->assign('mailsrecovery',$this->mails->checkrecovery());
        $this -> smarty -> assign('PageBody', 'admin/mails/manage.tpl');
        $this -> smarty -> assign('Title', 'Options List');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function addAction()
    {
        $this -> smarty -> assign('action', 'add');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $_POST['Mails']['date_modify'] = date("Y-m-d H:i:s");
            $_POST['Mails']['date_created'] = date("Y-m-d H:i:s");
            $this->mails->save($_POST['Mails']);
            $this->_redirect('/admin/mails/index/page/1');
        }
        $categories = $this->mails->findCategories();
        $this->smarty->assign('categories',$categories);
        $this -> smarty -> assign('PageBody', 'admin/mails/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function addcategoryAction()
    {
        $this -> smarty -> assign('action', 'addcategory');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $this->mails->saveCategory($_POST['MailsCategory']);
            $this->_redirect('/admin/mails/index/page/1');
        }
        $this -> smarty -> assign('PageBody', 'admin/mails/_form_category.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function editcategoryAction()
    {
        $this -> smarty -> assign('action', 'editcategory');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $this->mails->saveCategory($_POST['MailsCategory']);
            $this->_redirect('/admin/mails/index/page/1');
        }

        $id= $this->_getParam('id');
        $item = $this->mails->getCategoryById($id);
        $this -> smarty -> assign('PageBody', 'admin/mails/_form_category.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this->smarty->assign('item',$item);
        $this -> smarty -> display('admin/index.tpl');
    }

    public function deletecategoryAction()
    {
        $id = $this->_getParam('id');
        $this->mails->deleteCategory($id);
        $this->_redirect('/admin/mails/index/page/1');
    }

    public function deleteAction()
    {
        $id= $this->_getParam('id');
        $this->mails->delete($id);
        $this->_redirect('/admin/mails/index/page/1');
    }

    public function editAction()
    {
        $this -> smarty -> assign('action', 'edit');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $_POST['Mails']['date_modify'] = date("Y-m-d H:i:s");
            $this->mails->save($_POST['Mails']);
            $this->_redirect('/admin/mails/index/page/1');
        }

        $id= $this->_getParam('id');
        $item = $this->mails->findOneById($id);
        $categories = $this->mails->findCategories();
        $this->smarty->assign('categories',$categories);
        $this -> smarty -> assign('PageBody', 'admin/mails/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this->smarty->assign('item',$item);
        $this -> smarty -> display('admin/index.tpl');

    }

    public function categoryactionAction()
    {
        $data = $_POST['MailsCategory'];
        $action = $data['action'];
        $id = $data['id'];
        if($action=='edit') {
            $this->_redirect("/admin/mails/editcategory/id/".$id);
            exit;
        }
    }
}