<?php

include_once ROOT . 'application/models/AdminAreaControlModel.php';

include_once ROOT . 'application/models/ServicesManagerDb.php';

include_once ROOT . 'application/models/ServicesDb.php';
include_once ROOT . 'application/models/ProductsDb.php';

/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_AdminAction');
class Admin_ServicesmanagerController extends System_Controller_AdminAction
{
    private $sm;
    private $services;
    private $products;

    public function init()
    {
        parent::init();
        /** Check for user access */
        if(!AdminAreaControl::checkAccess()){
            $this -> _redirect('/admin');
        } else {
            $this -> sm = new ServicesManagerDb();
            $this->services = new ServicesDb();
            $this->products = new ProductsDb();
        }

        $this -> smarty -> assign('adminLeftMenu', 'services_manager');

    }

    public function indexAction()
    {
        if(($this->_hasParam('page')&&$this->_getParam('page')==0)
            ||!$this->_hasParam('page')
            ||(($this->_hasParam('page')&&$this->_getParam('page')>1) && ($this -> sm -> getItemsPagesCount($this->lang_id)<=1 ))
            ||($this->_getParam('page')>1&&$this -> sm -> getItemsPagesCount($this->lang_id)<$this->_getParam('page'))
        ){
            $this->_redirect("/admin/services_manager/index/page/1");
        }

        $page = $this->_hasParam('page')?((int)$this->_getParam('page')-1):0;

        $this -> smarty -> assign('items', $this->sm->getItemsForPage($page));
        $this -> smarty -> assign('countpage', $this->sm->getItemsPagesCount($this->lang_id));

        $this -> smarty -> assign('page',$page+1);
        $this -> smarty -> assign('PageBody', 'admin/services_manager/manage.tpl');
        $this -> smarty -> assign('Title', 'Options List');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function addAction()
    {
        $this -> smarty -> assign('action', 'add');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $this->sm->save($_POST['ServicesManager']);
            $this->_redirect('/admin/servicesmanager/index/page/1');
        }
        $this -> smarty -> assign('services',$this->services->findAll());
        $this -> smarty -> assign('products',$this->products->findAll());
        $this -> smarty -> assign('PageBody', 'admin/services_manager/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function editAction()
    {
        $this -> smarty -> assign('action', 'edit');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $this->sm->save($_POST['ServicesManager']);
            $this->_redirect('/admin/servicesmanager/index/page/1');
        }

        $id= $this->_getParam('id');
        $item = $this->sm->findOneById($id);

        $this -> smarty -> assign('services',$this->services->findAll());
        $this -> smarty -> assign('products',$this->products->findAll());
        $this -> smarty -> assign('PageBody', 'admin/services_manager/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this->smarty->assign('item',$item);
        $this -> smarty -> display('admin/index.tpl');

    }

    public function deleteAction()
    {
        $id= $this->_getParam('id');
        $this->sm->delete($id);
        $this->_redirect('/admin/servicesmanager/index/page/1');
    }


}