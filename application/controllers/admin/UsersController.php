<?php

include_once ROOT . 'application/models/AdminAreaControlModel.php';

include_once ROOT . 'application/models/UsersDb.php';
include_once ROOT . 'application/models/MailsDb.php';

/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_AdminAction');
class Admin_UsersController extends System_Controller_AdminAction
{
   
    private $users;
    
    public function init() {
        parent::init();
        
        /** Check for user access */
        if(!AdminAreaControl::checkAccess()){
        	$this -> _redirect('/admin');
        }
	
		$this -> users = new UsersDb();

        $this -> smarty -> assign('adminLeftMenu', 'users');
	}
    
    /**
     * Show list of a users
     *
     */
    
    public function indexAction() {

		if( ($this->_hasParam('page')&&$this->_getParam('page')==0)
			||!$this->_hasParam('page')
			||(($this->_hasParam('page')&&$this->_getParam('page')>1) && ($this -> users ->getPagesCount()<=1 ))
			||($this->_getParam('page')>1&&$this -> users ->getPagesCount()<$this->_getParam('page'))
		){
			$this->_redirect("/admin/users/index/page/1");
		}
		$page = $this->_hasParam('page')?((int)$this->_getParam('page')-1):0;
        $filter=false;
        if(isset($_POST['Filter'])) {
            $filter = new Filter($_POST['Filter']);
        }
        if(isset($_SESSION['Filter']['model']) && $_SESSION['Filter']['model'] !== 'Users') {
            unset($_SESSION['Filter']);
            $_SESSION['Filter']['active']=0;
        }
        $filterArray=@$_SESSION['Filter'];
        $this->smarty->assign('filter',$filterArray);

        $this->smarty->assign('stop',true);
        $this -> smarty -> assign('users', $this -> users -> getUsersForPage($page,-1,$filter));

        $this -> smarty -> assign('countpage', $this -> users ->getPagesCount($filter));
        $this -> smarty -> assign('page', $page+1);
        $this -> smarty -> assign('PageBody', 'admin/users/list.tpl');
        $this -> smarty -> assign('Title', 'Users List');
        $this -> smarty -> display('admin/index.tpl');
        
    }
    
    public function viewAction(){
     	$this -> smarty -> assign('item', $this->users->getUserById($this->_getParam('id')));
        $this -> smarty -> assign('PageBody', 'admin/users/view.tpl');
        $this -> smarty -> assign('Title', 'User View ');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function sendBillByEmail($email,$password,$firstname,$subject,$mailBody=null)
    {
        $emailTxt = new Zend_Config_Xml(ROOT.'configs/project/email.xml', 'email');

        Zend_Loader::loadClass('Zend_Mail');    /** Loading Zend_Mail */
        $mail = new Zend_Mail();
        $mail -> addHeader('X-MailGenerator', $_SERVER['HTTP_HOST'].' mail machine');
        $mails = new MailsDb();
        $contentMail = $mails->getContentByType('sendbill');
        if(is_null($mailBody)) {
        $mailBody = "<h1>Регистрация на сайте ".$_SERVER['HTTP_HOST']."</h1><p>
                Вы были зарегестрированы на сайте ".$_SERVER['HTTP_HOST']." ваши данные для входа:<br/>
                email: ".$email."<br/>
                пароль: ".$password."

                </p>";
        }
        $mail -> setBodyHtml($mailBody,'UTF-8');
        $mail -> setFrom('no-reply@'.$_SERVER['HTTP_HOST'], 'no-reply@'.$_SERVER['HTTP_HOST']);
        $mail -> addTo($email,$firstname);
        $subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
        $mail -> setSubject($subject);

        /** Send email */
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $mail->send();
    }
    
    public function deleteAction() {
		if($this->_hasParam('id')){
			$this -> users ->delete($this->_getParam('id'));
			$this -> _redirect( '/admin/users/index/page/1');
		}
    }

    public function addAction()
    {
        $this -> smarty -> assign('action', 'add');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $_POST['Users']['creation_date']=date("Y-m-d H:i:s");
            $_POST['Users']['modified_date']=date("Y-m-d H:i:s");
            $_POST['Users']['active']=1;
            $pass=$_POST['Users']['password'];
            $_POST['Users']['password']=md5($_POST['Users']['password']);
            $this->users->save($_POST['Users']);
            if(isset($_POST['sendToUser'])) {
                $username = ($_POST['username']) ? $_POST['username'] : 'user';
                $this->sendBillByEmail($_POST['Users']['email'],$pass,$_POST['Users']['first_name'],'Регистрация на сайте');
            }
            $this->_redirect('/admin/users/index/page/1');
        }
        $this -> smarty -> assign('PageBody', 'admin/users/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this -> smarty -> display('admin/index.tpl');
    }


    public function sendnewpassAction()
    {
        $id = intval($this->_getParam('id'));
        $user = $this->users->findOneById($id);
        $newPassword = $this->passGen(10,10);
        $user['password'] = md5($newPassword);
        $this->users->save($user);
        $mailBody = "<h1>Новый пароль</h1><p>У вас новый пароль на сайте <a href='http://".$_SERVER['HTTP_HOST']."'>".$_SERVER['HTTP_HOST']."</a> </p>
        <p>Новый пароль: ".$newPassword."</p>";
        $this->sendBillByEmail($user['email'],$newPassword,$user['first_name'],'Новый пароль',$mailBody);
        echo json_encode(array('status'=>200));
        exit;
    }

    public function editAction()
    {
        $this -> smarty -> assign('action', 'edit');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $_POST['Users']['modified_date']=date("Y-m-d H:i:s");
            //$pass=$_POST['Users']['password'];
            //$_POST['Users']['password']=md5($_POST['Users']['password']);
            $this->users->save($_POST['Users']);
            $this->_redirect('/admin/users/index/page/1');
        }

        $id= $this->_getParam('id');
        $item = $this->users->findOneById($id);
            $phoneArray = explode("-",$item['phone']);
        $phoneCode = $phoneArray[0];
        $item['phone'] = str_replace($phoneCode."-","",$item['phone']);
        $this -> smarty -> assign('phoneCode', $phoneCode);
        $this -> smarty -> assign('PageBody', 'admin/users/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this->smarty->assign('item',$item);
        $this -> smarty -> display('admin/index.tpl');

    }

    public function passGen($minchars=8, $maxchars=10, $chars="ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz")
    {
        $escapecharplus = 0;
        $repeat = mt_rand($minchars, $maxchars);
        $randomword = '';
        while ( $escapecharplus < $repeat )
        {
            $randomword .= $chars[mt_rand(1, strlen($chars)-1)];
            $escapecharplus += 1;
        }
        return $randomword;
    }

    /**
     * Ajax method for changing user status active/passive(1/0)
     *
     */
    public function changeUserStatusAction(){
		$this->users->changeUserStatus($this->_getParam('id'));
		$user = $this->users->getUserById($this->_getParam('id'));
		echo (int)$user['active'];  	
    }	
    
}