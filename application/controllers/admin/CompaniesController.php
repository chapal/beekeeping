<?php
include_once ROOT . 'application/models/AdminAreaControlModel.php';

include_once ROOT . 'application/models/LangDb.php';

include_once ROOT . 'application/models/FilesDb.php';

include_once ROOT . 'application/models/TThemesDb.php';

include_once ROOT . 'application/models/SitesDb.php';

include_once ROOT . 'application/models/ContentManagerDb.php';

include_once ROOT . 'application/models/SettingsDb.php';

include_once ROOT . 'application/models/CompaniesDb.php';


/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_AdminAction');

class Admin_CompaniesController extends System_Controller_AdminAction {


    protected $company;

    protected $languages;

    protected $files;

    protected $tthemes;

    protected $site;

    protected $content;

    protected $settings;


    public function init() {
        parent::init();

        /** Check for user access */
        if(!AdminAreaControl::checkAccess()){
            $this -> _redirect('/admin');
        }

        $this->company = new CompaniesDb();
        $this->languages = new LangDb();
        $this->files = new FilesDb();
        $this->tthemes = new  TThemesDb();
        $this->site = new SitesDb();
        $this->settings = new SettingsDb();


        $this -> smarty -> assign('adminLeftMenu', 'companies');

    }


    public function viewAction()
    {
        $id= $this->_getParam('id');
        $item = $this->company->findOneById($id);
        $item['payee_bank']=json_decode($item['payee_bank'],true);
        $this->smarty->assign('item',$item);
        $this -> smarty -> assign('PageBody', 'admin/companies/view.tpl');
        $this -> smarty -> display('admin/index.tpl');

    }

    public function editAction()
    {
        $this -> smarty -> assign('action', 'edit');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $_POST['Companies']['payee_bank']=array();
            for($i=0,$len = sizeof($_POST['Companies']['payee_banks']);$i<$len;$i++) {
                $_POST['Companies']['payee_bank'][$_POST['Companies']['payee_bank_currency'][$i]]=$_POST['Companies']['payee_banks'][$i];
            }
            unset($_POST['Companies']['payee_banks']);
            unset($_POST['Companies']['payee_bank_currency']);
            $_POST['Companies']['payee_bank']=json_encode($_POST['Companies']['payee_bank']);
            $this->company->save($_POST['Companies']);
            $this->_redirect('/admin/companies/index/page/1');
        }

        $id= $this->_getParam('id');
        $item = $this->company->findOneById($id);
        $item['payee_bank']=json_decode($item['payee_bank'],true);
        $this -> smarty -> assign('PageBody', 'admin/companies/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this->smarty->assign('item',$item);
        $this -> smarty -> display('admin/index.tpl');

    }

    public function indexAction()
    {
        if( ($this->_hasParam('page')&&$this->_getParam('page')==0)
            ||!$this->_hasParam('page')
            ||(($this->_hasParam('page')&&$this->_getParam('page')>1) && ($this -> sections ->getSectionsPagesCount($this->lang_id)<=1 ))
            ||($this->_getParam('page')>1&&$this -> sections ->getSectionsPagesCount($this->lang_id)<$this->_getParam('page'))
        ){
            $this->_redirect("/admin/sections/index/page/1");
        }

        $page = $this->_hasParam('page')?((int)$this->_getParam('page')-1):0;
        $campanies = $this -> company -> getCompaniesForPage($this->lang_id, $page);
        $editedCompanies = array();
        foreach($campanies as $key=>$company):
            $company['payee_bank']=json_decode($company['payee_bank'],true);
            $editedCompanies[$key]=$company;
        endforeach;
        $this -> smarty -> assign('companies',$editedCompanies );
        $this -> smarty -> assign('countpage', $this -> company ->getCompaniesPagesCount($this->lang_id));
        $this -> smarty -> assign('page',$page+1);
        $this -> smarty -> assign('PageBody', 'admin/companies/manage.tpl');
        $this -> smarty -> assign('Title', 'Orders List');
        $this -> smarty -> display('admin/index.tpl');

    }


    public function addAction()
    {
        $this -> smarty -> assign('action', 'add');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {

            $_POST['Companies']['payee_bank']=array();
            for($i=0,$len = sizeof($_POST['Companies']['payee_banks']);$i<$len;$i++) {
                $_POST['Companies']['payee_bank'][$_POST['Companies']['payee_bank_currency'][$i]]=$_POST['Companies']['payee_banks'][$i];
            }
            unset($_POST['Companies']['payee_banks']);
            unset($_POST['Companies']['payee_bank_currency']);
            $_POST['Companies']['payee_bank']=json_encode($_POST['Companies']['payee_bank']);
            $this->company->save($_POST['Companies']);
            $this->_redirect('/admin/companies/index/page/1');
        }
        $this -> smarty -> assign('PageBody', 'admin/companies/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this -> smarty -> display('admin/index.tpl');
    }



    public function deleteAction()
    {
        $id= $this->_getParam('id');
        $this->company->delete($id);
        $this->_redirect('/admin/companies/index/page/1');
    }
}