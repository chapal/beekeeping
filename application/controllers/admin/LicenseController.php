<?php

include_once ROOT . 'application/models/AdminAreaControlModel.php';

include_once ROOT.'application/models/LicenseDb.php';

/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_AdminAction');
class Admin_LicenseController extends System_Controller_AdminAction
{



    public function init()
    {
        parent::init();

        /** Check for user access */
        if(!AdminAreaControl::checkAccess()){
            $this -> _redirect('/admin');
        } else {

        }


        $this -> smarty -> assign('adminLeftMenu', 'license');

    }

    public function indexAction()
    {
        $licenseModel = new LicenseDb();
        $item = $licenseModel->getLicense();
        $this -> smarty -> assign('item', $item);
        $this -> smarty -> assign('PageBody', 'admin/license/manage.tpl');
        $this -> smarty -> assign('Title', 'Options List');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function addAction()
    {
        $this -> smarty -> assign('action', 'add');
        $licenseModel = new LicenseDb();
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {


            $licenseModel->save($_POST['License']);
            $this->_redirect('/admin/license/index');
        }
        $item = $licenseModel->getLicense();
        if($item) {
            $this->_redirect('/admin/license/index');
            exit;
        }
        $this -> smarty -> assign('PageBody', 'admin/license/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function checkAction()
    {
        if(isset($_SESSION['license_accept']) AND $_SESSION['license_accept']==1) {
            header("Content-Type:application/json");
            echo json_encode(array('status'=>200));
            exit;
        } else {
            header("Content-Type:application/json");
            echo json_encode(array('status'=>500));
            exit;
        }
    }

    public function editAction()
    {
        $this -> smarty -> assign('action', 'edit');
        $licenseModel = new LicenseDb();
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $licenseModel->save($_POST['License']);
            $this->_redirect('/admin/license/index');
        }

        $id= $this->_getParam('id');
        $item = $licenseModel->findOneById($id);
        $this -> smarty -> assign('PageBody', 'admin/license/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this->smarty->assign('item',$item);
        $this -> smarty -> display('admin/index.tpl');

    }

    public function deleteAction()
    {
        $id= $this->_getParam('id');
        $licenseModel = new LicenseDb();
        $licenseModel->delete($id);
        $this->_redirect('/admin/license/index');
    }


}