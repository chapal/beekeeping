<?php
include_once ROOT . 'application/models/AdminAreaControlModel.php';

include_once ROOT . 'application/models/ProductsDb.php';
include_once ROOT . 'application/models/ServicesDb.php';
include_once ROOT . 'application/models/ServicesManagerDb.php';

include_once ROOT . 'application/models/UsersOrdersDb.php';

include_once ROOT . 'application/models/ServicesManagerDb.php';



/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_AdminAction');
class Admin_FilterController extends System_Controller_AdminAction
{
    protected $su;

    public function init()
    {
        parent::init();
        parent::init();

        /** Check for user access */
        if(!AdminAreaControl::checkAccess()){
            $this -> _redirect('/admin');
        } else {
            $this->su = new UsersOrdersDb();

        }


    }

    public function indexAction()
    {
        $productsDb = new ProductsDb();
        $servicesDb = new ServicesDb();
        $serviceManagerDb = new ServicesManagerDb();
        $productsItems = $productsDb->findAllByActive(1);
        $servicesItems = $servicesDb->findAll();
        $servisecManagerItems = $serviceManagerDb->getServicesInfo();
        $users=false;
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'GET') {
            unset($_SESSION['FilterOrders']);
        }
        if(strtoupper($_SERVER['REQUEST_METHOD'])==='POST' AND isset($_POST['FilterOrders'])) {
            $userOrders = new UsersOrdersDb();
            $users = $userOrders->makeFilter($_POST['FilterOrders']);

        }
        $this->smarty->assign('filter_orders',(isset($_SESSION['FilterOrders'])) ? $_SESSION['FilterOrders'] : false);
        $this->smarty->assign('users',$users);
        $userTemplate = ($users) ? $this->smarty->fetch("admin/filter/users_list.tpl") : '';
        $this->smarty->assign('userTemplate',$userTemplate);
        $this->smarty->assign('productsItems',$productsItems);
        $this->smarty->assign('servicesItems',$servicesItems);
        $this->smarty->assign('servisecManagerItems',$servisecManagerItems);

        $this -> smarty -> assign('PageBody', 'admin/filter/manage.tpl');
        $this -> smarty -> assign('Title', 'Options List');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function checkserviceAction()
    {
        $serviceId = intval($this->_getParam('serviceId'));
        $serviceManagerDb = new ServicesManagerDb();
        $serviceItems = $serviceManagerDb->getByServiceId($serviceId,true);
        if(!$serviceItems) exit;
        $this->smarty->assign('filter_orders',(isset($_SESSION['FilterOrders'])) ? $_SESSION['FilterOrders'] : false);
        $this->smarty->assign('serviceItems',$serviceItems);
        $this->smarty->display('admin/filter/service_products.tpl');

    }
}