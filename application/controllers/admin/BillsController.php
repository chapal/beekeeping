<?php

include_once ROOT . 'application/models/AdminAreaControlModel.php';

include_once ROOT . 'application/models/BillsDb.php';

include_once ROOT . 'application/models/CompaniesDb.php';

include_once ROOT . 'application/models/ProductsDb.php';

include_once ROOT . 'application/models/MailsDb.php';

include_once ROOT . 'application/models/UsersDb.php';

include_once ROOT . 'application/models/Filter.php';

include_once ROOT . 'application/models/ServicesManagerDb.php';

include_once ROOT . 'application/controllers/core/BaseController.php';

/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_AdminAction');
class Admin_BillsController extends BaseController
{

    private $content;
    private $bill;
    private $File;

    public function init()
    {
        parent::init();

        /** Check for user access */
        if(!AdminAreaControl::checkAccess()){
            $this -> _redirect('/admin');
        } else {
            $this -> bill = new BillsDb();
        }

        $this -> smarty -> assign('adminLeftMenu', 'bills');

    }




    public function indexAction()
    {
        if(($this->_hasParam('page')&&$this->_getParam('page')==0)
            ||!$this->_hasParam('page')
            ||(($this->_hasParam('page')&&$this->_getParam('page')>1) && ($this -> bill -> getItemsPagesCount($this->lang_id)<=1 ))
            ||($this->_getParam('page')>1&&$this -> bill -> getItemsPagesCount($this->lang_id)<$this->_getParam('page'))
        ){
            $this->_redirect("/admin/services/index/page/1");
        }

        $page = $this->_hasParam('page')?((int)$this->_getParam('page')-1):0;
        $this->smarty->assign('stop',true);
        $filter=false;
        if(isset($_POST['Filter'])) {
            $filter = new Filter($_POST['Filter']);
        }
        if(isset($_SESSION['Filter']['model']) && $_SESSION['Filter']['model'] !== 'Bills') {
            unset($_SESSION['Filter']);
            $_SESSION['Filter']['active']=0;
        }
        $filterArray=@$_SESSION['Filter'];
        $this->smarty->assign('filter',$filterArray);
        $this -> smarty -> assign('items', $this->bill->getAllBills($filter));
        $this -> smarty -> assign('countpage', $this->bill->getItemsPagesCount($this->lang_id));
        $this -> smarty -> assign('page',$page+1);
        $this -> smarty -> assign('PageBody', 'admin/bills/manage.tpl');
        $this -> smarty -> assign('Title', 'Options List');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function viewAction()
    {

        $id= $this->_getParam('id');
        $item = $this->bill->getByPk($id);
        $item['payee_bank']=json_decode($item['payee_bank'],true);
        $this->smarty->assign('item',$item);
        $this -> smarty -> assign('PageBody', 'admin/bills/view.tpl');
        $this -> smarty -> display('admin/index.tpl');

    }

    public function sendAction()
    {
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $_POST['Bills']['status']='sent';
            $this->bill->save($_POST['Bills']);
            $billInfo = $this->bill->getByPk($_POST['Bills']['id']);
            $link = "http://".$_SERVER['HTTP_HOST']."/bills/index?num=".$billInfo['billlink'];
            $this->sendBillByEmail($_POST['Bills']['email'],$link,$billInfo['first_name']);
            $this->_redirect('/admin/bills/index/page/1');
        }
    }

    public function againsendAction()
    {
        $id = intval($this->_getParam('id'));
        $billInfo = $this->bill->getByPk($id);
        $link = "http://".$_SERVER['HTTP_HOST']."/bills/index?num=".$billInfo['billlink'];
        $this->sendBillByEmail($billInfo['billmail'],$link,$billInfo['first_name']);
        header('Content-type: application/json');
        echo json_encode(array('status'=>200,'msg'=>'Счет выслан повторно'));
        exit;

    }

    public function sendBillByEmail($email,$link,$username)
    {
        $emailTxt = new Zend_Config_Xml(ROOT.'configs/project/email.xml', 'email');

        Zend_Loader::loadClass('Zend_Mail');    /** Loading Zend_Mail */
        $mail = new Zend_Mail();
        $mail -> addHeader('X-MailGenerator', $_SERVER['HTTP_HOST'].' mail machine');
        $mails = new MailsDb();
        $contentMail = $mails->getContentByType('sendbill');
        $mailBody = str_replace("%link%","<a href='".$link."'>".$link."</a>",$contentMail['content']);
//        $mailBody="<div style='margin: 20px; padding: 20px;'>
//                                    <h2>Вам прислали счет</h2>
//                                    <p>Для того чтобы ознакомиться с доп. информацией перейдите по <a href='".$link."'>".$link."</a></p>
//                           </div>";
        $mail -> setBodyHtml($mailBody,'UTF-8');
        $mail -> setFrom('no-reply@'.$_SERVER['HTTP_HOST'], 'no-reply@'.$_SERVER['HTTP_HOST']);
        $mail -> addTo($email, $username);
        $subject = '=?UTF-8?B?'.base64_encode("Выставление счета ").'?=';
        $mail -> setSubject($subject);

        /** Send email */
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $mail->send();
    }

    public function changestatusAction()
    {
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $this->bill->save($_POST['Bills']);
            $this->_redirect("/admin/bills/index/page/1");
        }
    }

    public function createcopyAction()
    {
        $id = intval($this->getQueryParam('id'));
        $this->bill->makeCopyOfBill($id);
        $this->_redirect('/admin/bills/index/page/1');
    }

    public function addAction()
    {
        $this -> smarty -> assign('action', 'add');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $_POST['Bills']['due_date']=date("Y-m-d H:i:s",strtotime($_POST['Bills']['due_date']));
            $timestamp = time();
            $link = "http://".$_SERVER['HTTP_HOST']."/bills/index?num=".$timestamp;
            $_POST['Bills']['link']=$timestamp;
            if(!isset($_POST['sendToUser'])) {$_POST['Bills']['status']='draft';}
            $this->bill->save($_POST['Bills']);
            //sendEmail
            $email = $_POST['Bills']['email'];
            if(isset($_POST['sendToUser'])) {
                $username = ($_POST['username']) ? $_POST['username'] : 'user';
                $this->sendBillByEmail($email,$link,$username);
            }

            $this->_redirect('/admin/bills/index/page/1');
        }
        $company = new CompaniesDb();
        $companies = $company->findAll();
        $this->smarty->assign('companies',$companies);

        $userModel = new UsersDb();
        $users = $userModel->findAll();
        $this->smarty->assign('users',$users);
        $this->smarty->assign('method','GET');
        $this -> smarty -> assign('PageBody', 'admin/bills/createbill.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function editAction()
    {
        $this -> smarty -> assign('action', 'edit');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $this->bill->save($_POST['Bills']);
            $this->_redirect('/admin/bills/index/page/1');
        }

        $id= $this->_getParam('id');
        $item = $this->bill->getByPk($id);
        $userModel = new UsersDb();
        $users = $userModel->findAll();
        $this->smarty->assign('users',$users);
        $company = new CompaniesDb();
        $companies = $company->findAll();
        $this->smarty->assign('companies',$companies);
        $this->smarty->assign('method','GET');
        $this -> smarty -> assign('PageBody', 'admin/bills/createbill.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this->smarty->assign('item',$item);
        $this -> smarty -> display('admin/index.tpl');

    }


    public function getbytypeAction()
    {
        $type = $this->_getParam('type');
        $className = ucfirst($type).'Db';
        $model = new $className();
        $items = $model->findAll();
        $this->smarty->assign('items',$items);
        $this->smarty->display('admin/bills/items.tpl');

    }

    public function getservicesAction()
    {
            $model = new ServicesManagerDb();
            $items = $model->getItemsForPage(0,0,true);
            $this->smarty->assign('items',$items);
            $this->smarty->assign('services',true);
            $this->smarty->display('admin/bills/items.tpl');
    }

    public function createbillAction()
    {
        if($_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {
            $this->smarty->assign('action','add');
            $username = $this->_getParam('username');
            $useremail = $this->_getParam('useremail');
            $userid = $this->_getParam('userid');
            $this->smarty->assign('method','AJAX');
            $this->smarty->assign('userid',$userid);
            $this->smarty->assign('username',$username);
            $this->smarty->assign('useremail',$useremail);
            $company = new CompaniesDb();
            $companies = $company->findAll();
            $this->smarty->assign('companies',$companies);
            $this->smarty->display('admin/bills/createbill.tpl');
            exit;
        }

    }

    public function deleteAction()
    {
        $id= $this->_getParam('id');
        $this->bill->delete($id);
        $this->_redirect('/admin/bills/index/page/1');
    }


}