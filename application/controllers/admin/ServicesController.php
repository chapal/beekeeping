<?php

include_once ROOT . 'application/models/AdminAreaControlModel.php';

include_once ROOT . 'application/models/ServicesDb.php';

include_once ROOT . 'application/models/ContentManagerDb.php';

include_once ROOT . 'application/models/FilesDb.php';

/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_AdminAction');
class Admin_ServicesController extends System_Controller_AdminAction
{

    private $content;
    private $service;
    private $File;

    public function init()
    {
        parent::init();

        /** Check for user access */
        if(!AdminAreaControl::checkAccess()){
            $this -> _redirect('/admin');
        } else {
            $this -> service = new ServicesDb();
            $this -> content = new ContentManagerDb();
            $this->File = new FilesDb();
        }


        $this -> smarty -> assign('adminLeftMenu', 'services');

    }

    public function indexAction()
    {
        if(($this->_hasParam('page')&&$this->_getParam('page')==0)
            ||!$this->_hasParam('page')
            ||(($this->_hasParam('page')&&$this->_getParam('page')>1) && ($this -> service -> getItemsPagesCount($this->lang_id)<=1 ))
            ||($this->_getParam('page')>1&&$this -> service -> getItemsPagesCount($this->lang_id)<$this->_getParam('page'))
        ){
            $this->_redirect("/admin/services/index/page/1");
        }

        $page = $this->_hasParam('page')?((int)$this->_getParam('page')-1):0;

        $this -> smarty -> assign('items', $this->service->getItemsForPage($page));
        $this -> smarty -> assign('countpage', $this->service->getItemsPagesCount($this->lang_id));
        $this -> smarty -> assign('page',$page+1);
        $this -> smarty -> assign('PageBody', 'admin/services/manage.tpl');
        $this -> smarty -> assign('Title', 'Options List');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function addAction()
    {
        $this -> smarty -> assign('action', 'add');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $this->service->save($_POST['Services']);
            $this->_redirect('/admin/services/index/page/1');
        }
        $this -> smarty -> assign('PageBody', 'admin/services/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this -> smarty -> display('admin/index.tpl');
    }

    public function editAction()
    {
        $this -> smarty -> assign('action', 'edit');
        if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
            $this->service->save($_POST['Services']);
            $this->_redirect('/admin/services/index/page/1');
        }

        $id= $this->_getParam('id');
        $item = $this->service->findOneById($id);
        $this -> smarty -> assign('PageBody', 'admin/services/_form.tpl');
        $this -> smarty -> assign('Title', 'Sections Manager: Add Section');
        $this->smarty->assign('item',$item);
        $this -> smarty -> display('admin/index.tpl');

    }

    public function deleteAction()
    {
        $id= $this->_getParam('id');
        $this->service->delete($id);
        $this->_redirect('/admin/services/index/page/1');
    }


}