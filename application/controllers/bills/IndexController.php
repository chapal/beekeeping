<?php
include_once ROOT . 'application/models/SectionsDb.php';
include_once ROOT . 'application/controllers/admin/BillsController.php';
include_once ROOT . 'application/controllers/admin/CallbackController.php';
include_once ROOT . 'application/models/BillsDb.php';

include_once ROOT . 'application/models/MailsDb.php';

include_once ROOT . 'application/models/CompaniesDb.php';
include_once ROOT . 'library/payments/payment.class.php';
include_once ROOT . 'library/payments/robokassa/robokassa.class.php';
include_once ROOT . 'library/belpay/belpay.class.php';


/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_Action');

class Bills_IndexController extends System_Controller_Action {

    protected $bill;
    private $bp_secret_key='secretkeyverystrong';
    private $bp_mrch_login = "softscript";

    public function init()
    {
        parent::init();
        $this->bill = new BillsDb();
    }

    public function reminderAction()
    {
            $data = $this->bill->checkServiceBills();
            foreach($data as $key=>$current):
            $currentDate=new DateTime();
            $paidDate = new DateTime($current['paid_date']);

            switch($current['period_type']):
                case 'days':
                    $intervalObject = new DateInterval('P'.$current['period'].'D');
                    break;
                case 'months':
                    $intervalObject = new DateInterval('P'.$current['period'].'M');
                    break;
                case 'years':
                    $intervalObject = new DateInterval('P'.$current['period'].'Y');
                    break;

            endswitch;
            $paidDate->add($intervalObject);
            $diff = $currentDate->diff($paidDate);
            $daysEnd = intval($diff->format('%R%a')+1);
            if($daysEnd===7 || $daysEnd===3 || $daysEnd===1) {

                $link = "http://".$_SERVER['HTTP_HOST']."/bills/index?num=".$current['link'];
                $this->sendBillByEmail($current['sendmail'],$link,$current['first_name'],array('type'=>'remindbill','days'=>$daysEnd));
                $this->bill->save(array('id'=>$current['id'],'status'=>'sent'));
            }
            if($daysEnd===0 || $daysEnd < 0) {
                //raise callback
                call_user_func_array(array(new Admin_CallbackController(),'delayInPayment'),array('billId'=>$current['id']));
            }
            //echo 'days end '.$daysEnd;
            endforeach;


    }

    public function sendBillByEmail($email,$link,$username,$params)
    {
        $emailTxt = new Zend_Config_Xml(ROOT.'configs/project/email.xml', 'email');

        Zend_Loader::loadClass('Zend_Mail');    /** Loading Zend_Mail */
        $mail = new Zend_Mail();
        $mail -> addHeader('X-MailGenerator', $_SERVER['HTTP_HOST'].' mail machine');
        $mailsObj = new MailsDb();
        $mailsArray = $mailsObj->getContentByType($params['type']);
        $mailBody = str_replace("%link%","<a href='".$link."'>".$link."</a>",$mailsArray['content']);
        $mailBody = str_replace("%days%",$params['days'],$mailBody);
//        $mailBody="<div style='margin: 20px; padding: 20px;'>
//                                    <h2>Вам прислали счет</h2>
//                                    <p>Для того чтобы ознакомиться с доп. информацией перейдите по <a href='".$link."'>".$link."</a></p>
//                           </div>";
        $mail -> setBodyHtml($mailBody,'UTF-8');
        $mail -> setFrom('no-reply@'.$_SERVER['HTTP_HOST'], 'no-reply@'.$_SERVER['HTTP_HOST']);
        $mail -> addTo($email, $username);
        $subject = '=?UTF-8?B?'.base64_encode("Выставление счета ").'?=';
        $mail -> setSubject($subject);

        /** Send email */
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $mail->send();
    }



    public function getExchangedPrice($currentPrice,$currency)
    {
        $convertedPrice = '';
        $oneUSD = '';
        $oneRUR='';
        $oneEUR='';
        $xml = simplexml_load_file('http://www.nbrb.by/Services/XmlExRates.aspx?ondate='.date("m/d/Y"));
        foreach($xml as $f) {

            if($f->CharCode =='USD') {
                $oneUSD=$f->Rate;
            }
            if($f->CharCode =='RUB') {
                $oneRUR=$f->Rate;
            }
            if($f->CharCode =='EUR') {
                $oneEUR=$f->Rate;
            }


        }


        $totalPriceInBYR = intval($oneUSD)*floatval($currentPrice);

        switch ($currency):
            case 'RUR':
                $convertedPrice = floatval($totalPriceInBYR/intval($oneRUR));
                break;
            case 'EUR':

                $convertedPrice = floatval($totalPriceInBYR/intval($oneEUR));
                break;
            case 'BYR':
                $convertedPrice = $totalPriceInBYR;
                break;
            case 'USD':
                $convertedPrice = $currentPrice;
                break;

        endswitch;
        $convertedPrice= @number_format($convertedPrice,2);
        return $convertedPrice;
    }

    public function getOneCurrencyValue($name)
    {
        $xml = simplexml_load_file('http://www.nbrb.by/Services/XmlExRates.aspx?ondate='.date("m/d/Y"));
        foreach($xml as $f) {
            if($f->CharCode ==$name) {
                $one=$f->Rate;
                break;
            }
        }

        return $one;
    }


    public function indexAction()
    {
        $link = $this->_getParam('num');
        $billInfo = $this->bill->getByLink($link);
        if(!$billInfo){
            $this->smarty->assign('noBill',true);
        }
        $serviceInfo=false;
        if($billInfo['service_id']) {
            $serviceInfo =$this->bill->getInfoAboutService($billInfo['service_id']);
            $billInfo['price'] = $serviceInfo['price'];
        }
        $this->smarty->assign('serviceInfo',$serviceInfo);
        $lingToPrint =  "http://".$_SERVER['HTTP_HOST']."/bills/index/print?num=".$link;
        $lingToPDF =  "http://".$_SERVER['HTTP_HOST']."/bills/index/pdf?num=".$link;
        $currentDate = new DateTime();
        $dueDate = new DateTime($billInfo['due_date']);
        $dayEnd = $currentDate->diff($dueDate);
        $endDays = $dayEnd->days+1;
        $companyObj = new CompaniesDb();
        $companyInfo = $companyObj->findOneById($billInfo['company_id']);
        $billInfo['price'] = $this->getExchangedPrice($billInfo['price'],$billInfo['currency']);
        $this->smarty->assign('lingToPrint',$lingToPrint);
        $this->smarty->assign('companyInfo',$companyInfo);
        $this->smarty->assign('link',$link);
        $this->smarty->assign('lingToPDF',$lingToPDF);
        $this->smarty->assign('endDays',$endDays);
        $this->smarty->assign('billInfo',$billInfo);
        $this->smarty->display('bills/index.tpl');
    }

    public function printAction()
    {
        $link = $this->_getParam('num');
        $billInfo = $this->bill->getByLink($link);
        $serviceInfo=false;
        if($billInfo['service_id']) {
            $serviceInfo =$this->bill->getInfoAboutService($billInfo['service_id']);
            $billInfo['price'] = $serviceInfo['price'];
        }
        $this->smarty->assign('serviceInfo',$serviceInfo);
        $billInfo['payee_bank']=json_decode($billInfo['payee_bank'],true);
        if(array_key_exists($billInfo['currency'],$billInfo['payee_bank'])){
            $billInfo['payee_bank']=$billInfo['payee_bank'][$billInfo['currency']];
        }else{
            $billInfo['payee_bank']=array_shift($billInfo['payee_bank']);
        }
        $currentDate = new DateTime();
        $dueDate = new DateTime($billInfo['due_date']);
        $dayEnd = $currentDate->diff($dueDate);
        $endDays = $dayEnd->days+1;
        $billInfo['price'] = $this->getExchangedPrice($billInfo['price'],$billInfo['currency']);
        $this->smarty->assign('endDays',$endDays);
        $this->smarty->assign('billInfo',$billInfo);
        $this->smarty->display('bills/print.tpl');
    }

    public function pdfAction()
    {
        include_once ROOT .'library/mpdf/mpdf.php';
        $link = $this->_getParam('num');
        $billInfo = $this->bill->getByLink($link);
        $serviceInfo=false;
        if($billInfo['service_id']) {
            $serviceInfo =$this->bill->getInfoAboutService($billInfo['service_id']);
            $billInfo['price'] = $serviceInfo['price'];
        }
        $this->smarty->assign('serviceInfo',$serviceInfo);
        $billInfo['payee_bank']=json_decode($billInfo['payee_bank'],true);
        if(array_key_exists($billInfo['currency'],$billInfo['payee_bank'])){
            $billInfo['payee_bank']=$billInfo['payee_bank'][$billInfo['currency']];
        }else{
            $billInfo['payee_bank']=array_shift($billInfo['payee_bank']);
        }
        $currentDate = new DateTime();
        $dueDate = new DateTime($billInfo['due_date']);
        $dayEnd = $currentDate->diff($dueDate);
        $endDays = $dayEnd->days+1;
        $billInfo['price'] = $this->getExchangedPrice($billInfo['price'],$billInfo['currency']);
        $this->smarty->assign('endDays',$endDays);
        $this->smarty->assign('billInfo',$billInfo);
        $content = $this->smarty->fetch('bills/print.tpl');

        $mpdf = new mPDF('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
//      $mpdf->charset_in = 'cp1251'; /*не забываем про русский*/
        $stylesheet = file_get_contents(ROOT.'application/views/templates/bills/assets/style.css'); /*подключаем css*/
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->list_indent_first_level = 0;
        $mpdf->WriteHTML($content, 2); /*формируем pdf*/
        $file = $mpdf->Output('mpdf.pdf', 'I');

        header('Content-Disposition: attachment; filename=Bill_Antalika.pdf');
        exit();

    }

    public function paylistAction()
    {
        $link = $this->_getParam('num');
        $billInfo = $this->bill->getByLink($link);
        $this->smarty->assign('billJSON',json_encode($billInfo));
        $this->smarty->display('bills/paymentlist.tpl');

    }

    public function robokassapayAction()
    {
           $amount = ($_POST['billInfo']['type']==='offer') ? intval($_POST['billInfo']['price']) : $_POST['billInfo']['price'];
           $amount=$this->getExchangedPrice($amount,'RUR');
           $amount = str_replace(",","",$amount);
           $inv_id = intval($_POST['billInfo']['billid']);  // id заказа
           $robokassaObj = new robokassa('SoftScript.ru','halkwillwin10','halkwilllose01',$amount,$inv_id,'RUR',0);
           echo $robokassaObj->getForm();

    }

    public function checkauthAction()
    {

        if(isset($_SESSION['loginNamespace']) && !empty($_SESSION['loginNamespace'])) {
         echo json_encode(array('status'=>200));
        } else {
            echo json_encode(array('status'=>500));
        }
        exit;
    }

    public function testrobokassaAction()
    {
        die(var_dump($_POST));
    }

    public function payAction()
    {

        $amount = ($_POST['billInfo']['type']==='offer') ? intval($_POST['billInfo']['price']) : $_POST['billInfo']['price'];  // сумма заказа
        $system_label = $_POST['payment_system'];  // метод оплаты
        $currency="USD";
        if($system_label !=4) {
            $amount=$this->getExchangedPrice($amount,$_POST['billInfo']['currency']);
            $currency = $_POST['billInfo']['currency'];
        }
        $inv_id = intval($_POST['billInfo']['id']);  // id заказа
        $desc = ($_POST['billInfo']['type']==='offer') ? $_POST['billInfo']['offer'] : "Оплата за ".$_POST['billInfo']['title'];  // описание заказа
        $system_label = $_POST['payment_system'];  // метод оплаты
        $form = '
      	<form name="pay" action="http://kassa.belpay.by/incomming" method="post">
        	<input type="hidden" name="bp_mrch_login" value="'.$this->bp_mrch_login.'">
        	<input type="hidden" name="bp_outsum" value="'.$amount.'">
        	<input type="hidden" name="bp_inv_id" value="'.$inv_id.'">
        	<input type="hidden" name="bp_desc" value="'.$desc.'">
        	<input type="hidden" name="bp_signature_value" value="'.$this->generateSignHash($this->bp_mrch_login,$amount,$inv_id,$this->bp_secret_key).'">
        	<input type="hidden" name="bp_system_label" value="'.$system_label.'">
        	<input type="hidden" name="bp_curr_label" value="'.$currency.'" >
            <input type="submit" value="Отослать">
        </form>';
        echo $form;
        exit;
    }

    public function buyAction()
    {
        $oneUsd = $this->getOneCurrencyValue('USD');
        $currency = 'USD';
        $system_label = $_POST['payment_system']; //7 Robokassa
        if(isset($_SESSION['bills']['amount']) && isset($_SESSION['bills']['bill_id'])) {
            $billId = array('bill_id'=>$_SESSION['bills']['bill_id'],'amount'=>$_SESSION['bills']['amount']);
        } else {
            $billId = $this->bill->makeOrder();
        }

        if($system_label==7) {
            $billId['amount'] = $this->getExchangedPrice($billId['amount'],'RUR');
            $billId['amount'] = str_replace(",","",$billId['amount']);
            $robokassaObj = new robokassa('SoftScript.ru','halkwillwin10','halkwilllose01',$billId['amount'],$billId['bill_id'],'RUR',0);
            $robokassaObj->callShowForm();

        } else {

            //If user will choose BYR payment system, you should need to convert USD value
          if($system_label == '1' || $system_label=='3' || $system_label=='2' || $system_label=='6' ) {
              $billId['amount'] = floatval($billId['amount'])*intval($oneUsd);
              $currency="BYR";
          }
          echo '<form name="pay" action="http://kassa.belpay.by/incomming" method="post">
            <input type="hidden" name="bp_mrch_login" value="'.$this->bp_mrch_login.'">
            <input type="hidden" name="bp_outsum" value="'.$billId['amount'].'">
            <input type="hidden" name="bp_inv_id" value="'.$billId['bill_id'].'">
            <input type="hidden" name="bp_desc" value="Оплата товаров">
            <input type="hidden" name="bp_signature_value" value="'.$this->generateSignHash($this->bp_mrch_login,$billId["amount"],$billId["bill_id"],$this->bp_secret_key).'">
            <input type="hidden" name="bp_system_label" value="'.$system_label.'">
            <input type="hidden" name="bp_curr_label" value="'.$currency.'" >
            <input type="submit" value="Отослать">
          </form>';
        }
      exit;
    }

    public function generateSignHash($bp_mrch_login, $amount, $inv_id, $bp_secret_key)
    {
        return sha1($bp_mrch_login.":".$amount.":".$inv_id.":".$bp_secret_key);
    }


    public function resultrobokassaAction()
    {

        $inv_id = (integer)$this->_getParam('InvId');
        $isItOrder = $this->bill->isOrder($inv_id);
        if(!$isItOrder) {
            $billInfo = $this->bill->getByPk($inv_id);
            if (!empty($billInfo)) {
                if($billInfo['service_id']) {
                    $serviceInfo = $this->bill->getServiceInfo($billInfo['billid']);
                    $this->bill->makeUserOrder($serviceInfo,$billInfo);
                    $currentDate = new DateTime();
                    switch($serviceInfo['period_type']):
                        case 'days':
                            $intervalObject = new DateInterval('P'.$serviceInfo['period'].'D');
                            break;
                        case 'months':
                            $intervalObject = new DateInterval('P'.$serviceInfo['period'].'M');
                            break;
                        case 'years':
                            $intervalObject = new DateInterval('P'.$serviceInfo['period'].'Y');
                            break;

                    endswitch;
                    $currentDate->add($intervalObject);
                    $this->bill->save(array('status'=>'paid','id'=>$inv_id,'paid_date'=>date('Y-m-d'),'due_date'=>$currentDate->format("Y-m-d")));
                    exit("OK".$inv_id);
                }
                $this->bill->save(array('status'=>'paid','id'=>$inv_id,'paid_date'=>date('Y-m-d')));
                exit("OK".$inv_id);
            }
        } else {
            $this->bill->makeOrderFront($inv_id);
            exit("OK".$inv_id);
        }
    }

    public function resultAction()
    {

        if ($_SERVER['REMOTE_ADDR']!='93.125.53.166')
            exit("access denied");
        if (!$this->checkSignHash())
            exit("bad sign");

        $inv_id = (integer)$this->_getParam('bp_inv_id');
        $isItOrder = $this->bill->isOrder($inv_id);

        if(!$isItOrder) {
        $billInfo = $this->bill->getByPk($inv_id);
            if (!empty($billInfo)) {
                if($billInfo['service_id']) {
                    $serviceInfo = $this->bill->getServiceInfo($billInfo['billid']);
                    $this->bill->makeUserOrder($serviceInfo,$billInfo);
                    $currentDate = new DateTime();
                    switch($serviceInfo['period_type']):
                        case 'days':
                            $intervalObject = new DateInterval('P'.$serviceInfo['period'].'D');
                            break;
                        case 'months':
                            $intervalObject = new DateInterval('P'.$serviceInfo['period'].'M');
                            break;
                        case 'years':
                            $intervalObject = new DateInterval('P'.$serviceInfo['period'].'Y');
                            break;

                    endswitch;
                    $currentDate->add($intervalObject);
                    $this->bill->save(array('status'=>'paid','id'=>$inv_id,'paid_date'=>date('Y-m-d'),'due_date'=>$currentDate->format("Y-m-d")));
                    exit("OK".$inv_id);
                }
                $this->bill->save(array('status'=>'paid','id'=>$inv_id,'paid_date'=>date('Y-m-d')));
                exit("OK".$inv_id);
             }
        } else {
            $this->bill->makeOrderFront($inv_id);
            exit("OK".$inv_id);
        }
    }

    private function checkSignHash()
    {
        if (sha1($_POST["bp_outsum"].":".$_POST["bp_inv_id"].":".$this->bp_secret_key)==$_POST["bp_signature_value"])
            return true;
        else
            return false;
    }

    public function successAction()
    {
        if(isset($_GET['bp_inv_id']) OR isset($_GET['InvId'])){
            if(isset($_SESSION['bills']['front'])){
                $_SESSION['order_step']=3;
                unset($_SESSION['shopping_cart']);
                unset($_SESSION['bills']['bill_id']);
                unset($_SESSION['bills']['amount']);
                unset($_SESSION['bills']['front']);
                header("Location:http://".$_SERVER['HTTP_HOST'].'/payment-success.html');
                exit;
            }
            if(isset($_GET['bp_inv_id'])) {
                $inv_id = (integer)$_GET['bp_inv_id'];
            }
            if(isset($_GET['InvId'])) {
                $inv_id = (integer)$_GET['InvId'];
            }



            $billInfo = $this->bill->getByPk($inv_id);
            header("Location:http://".$_SERVER['HTTP_HOST'].'/bills/index/success?num='.$billInfo['billlink']);
            exit;
        }

        $link = $this->_getParam('num');
        $billInfo = $this->bill->getByLink($link);
        if(!$billInfo){
            $this->smarty->assign('noBill',true);
        }
        if($billInfo['status'] !== 'paid') {
            header("Location:http://".$_SERVER['HTTP_HOST'].'/bills/index?num='.$billInfo['billlink']);
        }
        $this->smarty->display('bills/success.tpl');


    }
    public function failAction()
    {
        if(isset($_GET['bp_inv_id'])) {
            $inv_id = (integer)$_GET['bp_inv_id'];
        }
        if(isset($_GET['InvId'])) {
            $inv_id = (integer)$_GET['InvId'];
        }

        if(isset($_SESSION['bills']['front'])){
            header("Location:http://".$_SERVER['HTTP_HOST'].'/shopping-cart.html');
            exit;
        }
        $billInfo = $this->bill->getByPk($inv_id);
        header("Location:http://".$_SERVER['HTTP_HOST'].'/bills/index?num='.$billInfo['billlink']);
        exit;
    }



}