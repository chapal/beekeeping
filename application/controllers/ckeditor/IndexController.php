<?php

/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_Action');

class Ckeditor_IndexController extends System_Controller_Action {

    protected $sections;
    protected $products;

    public function init() {
        parent::init();

    }

    public function indexAction()
    {
        $callback = $_GET['CKEditorFuncNum'];
        $file_name = mt_rand(1000,9999).'_'.time().'_'.$_FILES['upload']['name'];
        $file_name_tmp = $_FILES['upload']['tmp_name'];
        $file_new_name = $_SERVER['DOCUMENT_ROOT'].'/upload/ckeditor_images/';
        if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/upload/ckeditor_images')) {
            mkdir($_SERVER['DOCUMENT_ROOT'].'/upload/ckeditor_images',0777);
        }

        $full_path = $file_new_name.$file_name;
        if(move_uploaded_file($file_name_tmp, $full_path)) {
            $http_path = 'http://'.$_SERVER['HTTP_HOST'].'/upload/ckeditor_images/'.$file_name;
        }
        echo "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\");</script>";


    }


}