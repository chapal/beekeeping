<?php

/** Zend_Controller_Action */
Zend_Loader::loadClass('System_Controller_AdminAction');
class BaseController extends System_Controller_AdminAction
{
    public function getQueryParam($paramName)
    {
        $url = $_SERVER['REQUEST_URI'];
        $uriArray = explode("/",$url);
        $arraySize = sizeof($uriArray);
        $paramValue=false;
        for($i=0;$i<$arraySize;$i++) {
            if($uriArray[$i]===$paramName) {
                $paramValue = $uriArray[++$i];
                break;
            }
        }
        return $paramValue;
    }
}