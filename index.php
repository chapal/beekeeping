<?php

/** Setting error reporting */
//error_reporting(E_ALL);

/** Define root path to project */
define('ROOT', dirname(__FILE__).'/');

define('SITE_URL', 'http://'.$_SERVER['HTTP_HOST']);

/** Define library root */
define('LIB_ROOT', ROOT . 'library/');

/** Set project include path */
ini_set('include_path', LIB_ROOT);
//ini_set(session.save_path, '/home/chapal/www/beekeeping/tmp/session');
/** Set PEAR library components path */
$PEAR_PATH = LIB_ROOT . 'PEAR';
set_include_path(get_include_path() . PATH_SEPARATOR . $PEAR_PATH);

/** ************************************************ */

/** Getting loader */
require_once 'Zend/Loader.php';

/** Getting usefull libs */ 
Zend_Loader::loadClass('Zend_Controller_Front');                // Front controller
Zend_Loader::loadClass('Zend_Controller_Router_Route_Regex');   // Controller router
Zend_Loader::loadClass('Zend_Cache');                           // Cache
Zend_Loader::loadClass('Zend_Db');                              // Database
Zend_Loader::loadClass('Zend_Db_Table_Abstract');               // Database abstract
Zend_Loader::loadClass('Zend_Session');                         // Session handler
Zend_Loader::loadClass('Zend_Config_Xml');                      // Configurer
Zend_Loader::loadClass('Zend_Registry');                        // Registry
Zend_Loader::loadClass('Zend_Debug');                           // Debugger


/** ************************************************ */

/** Starting session with parameters */
Zend_Session::setOptions(array('save_path' => ROOT . 'tmp/session',
                               'use_only_cookies' => 'on',
                               'remember_me_seconds' => 864000      // Remember me 10 days
                               ));
//session_start();
//Zend_Session::start();

//Zend_Session::rememberMe(10080);

/** Initialize Smarty templater */
require_once LIB_ROOT . 'smarty/Smarty.class.php';

$Smarty = new Smarty();
$Smarty -> debugging        = false;
$Smarty -> force_compile    = true;
$Smarty -> caching          = false;
$Smarty -> compile_check    = true;
$Smarty -> cache_lifetime   = -1;
$Smarty -> template_dir     = ROOT . 'application/views/templates';
$Smarty -> compile_dir      = ROOT . 'application/views/templates_c';
$Smarty -> plugins_dir      = array(
                              SMARTY_DIR . 'plugins', ROOT . 'application/views/plugins');

/** Set Smarty into registry */
Zend_Registry::set('smarty', $Smarty);

/** Database */
Zend_Loader::loadFile('Database.php', ROOT . 'backend/', true);

/** Controller */
Zend_Loader::loadFile('Router.php', ROOT . 'backend/', true);


