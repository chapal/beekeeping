<?php
  class belpay extends APayments implements IPayment, IPaymentCallback {
      public $pay_name = "BelPay";
      public $settings = array();
      private $amount;
      private $inv_id;
      private $inv_desc;

      public function isCallback() {
        if (!isset($_POST['bp_inv_id']))
          return false;
        else
          return true;
      }


      public function getForm($amount, $options = array()) {  // при пополнении баланса
          global $sql;
          $sql->exec("INSERT INTO pay_outcomming(userid, payvariant, paycost, date, comments) VALUES({$options['user_id']}, 'belpay', $amount, NOW(), 'Пополнение баланса')");
          $this->inv_id = $sql->lastInsertId();
          $this->inv_desc = "Пополнение баланса на ".$_SERVER['HTTP_HOST'];
          $this->amount = $amount;
          $output = $this->constructHtml();
          return $output;
      }


      public function getBuyForm($amount, $pct_id) {  // при покупке купона
          global $sql;
          $sql->exec("INSERT INTO pay_outcomming(payvariant, paycost, date, comments, pct_id) VALUES('belpay', $amount, NOW(), 'Покупка купонов', '$pct_id')");
          $this->inv_id = $sql->lastInsertId();
          $this->inv_desc = "Покупка купонов";
          $this->amount = $amount;
          $output = $this->constructHtml();
          return $output;
      }


      protected function constructHtml() {
        $url = ($this->settings['test_mode']==0) ? 'http://kassa.belpay.by/incomming' : 'http://kassa.belpay.by/test';
        $form = '
        	<form name="pay" action="'.$url.'" method="post">
          	<input type="hidden" name="bp_mrch_login" value="'.$this->settings['bp_mrch_login'].'">
          	<input type="hidden" name="bp_outsum" value="'.$this->amount.'">
          	<input type="hidden" name="bp_inv_id" value="'.$this->inv_id.'">
          	<input type="hidden" name="bp_desc" value="'.$this->inv_desc.'">
          	<input type="hidden" name="bp_signature_value" value="'.$this->generateSignHash().'">
          	<input type="hidden" name="bp_system_label" value="">
          	<input type="submit" name="process" value="Оплатить">
        	</form>';
        return $form;
      }


      public function process() {
        global $sql;
        if ($_SERVER['REMOTE_ADDR']!='93.125.53.166')
          exit("access denied");
        if (!$this->checkSignHash())
          exit("bad sign");
        $inv_id = (integer)$_POST['bp_inv_id'];
        $pay_info = $sql->fetchOne("SELECT * FROM pay_outcomming WHERE id=$inv_id AND status=0");
        if (!empty($pay_info)) {
          $sql->exec("UPDATE pay_outcomming SET status=1 WHERE id=$inv_id");
          if (empty($pay_info['pct_id'])) {  // пополнение счёта
            $sql->exec("UPDATE users SET balance=balance+{$pay_info['paycost']} WHERE id={$pay_info['userid']}");
            // и в журнал
            $sql->exec("INSERT INTO admin_pays(date, userid, paycost, payvar, paytype) VALUES(NOW(), {$pay_info['userid']}, {$pay_info['paycost']}, 'belpay', 1)");
          }
          else {  // оплата купонов
            $pct_info = $sql->fetchOne("SELECT * FROM pey_coupons_transactions WHERE id={$pay_info['pct_id']} AND tran_status=0");
            if (!empty($pct_info) and ($pct_info['total_price']==$_POST['bp_outsum']))
              Coupons::makeBuyOrder($pay_info['pct_id']);
          }
          exit("OK".$inv_id);
        }
      }


      private function generateSignHash() {
        return sha1($this->settings['bp_mrch_login'].":".$this->amount.":".$this->inv_id.":".$this->settings['bp_secret_key']);
      }


      private function checkSignHash() {
        if (sha1($_POST["bp_outsum"].":".$_POST["bp_inv_id"].":".$this->settings['bp_secret_key'])==$_POST["bp_signature_value"])
          return true;
        else
          return false;
      }


      public function writelog($title, $data) {
          $file = 'belpay.log';
          $mode = (file_exists($file) && is_writeable($file)) ? "a" : "w";
          $fp = @fopen($file, $mode);
          fwrite($fp, date('Y-m-d H:i:s') . ": " . $title . ":\r\n");
          if (is_array($data))
              fwrite($fp, var_export($data, true) . "\r\n");
          else
              fwrite($fp, $data . "\r\n");
          fwrite($fp, "\r\n");
          fclose($fp);
      }

  }
?>