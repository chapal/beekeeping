В приложении реализован универсальный фильтр

Логика реализована внутри базовой модели BaseModel

этот класс абстрактный и по логике новые модели должны от него наследоваться

Для того чтобы реализовать фильтр создается форма с атрибутом id="filter"

в форме должно быть как минимум два обязательных поля
<input type="hidden" name="Filter[model]" value="Bills">
<input type="hidden" name="Filter[active]" value="">

как видно все поля формы фильтра должны начинатся с Filter
Filter[model] - название модели к которой фильтр приязывается
Filter[active] - активен ли фильтр, данные параметр задается через jquery

Фильтр должен иметь обязательные кнопки
<a href="javascript:void(0);" class="btn green" onclick="filterOn();" style="padding: 6px 14px;" /><i class="icon-ok"></i> Включить</a>
<a href="javascript:void(0);" class="btn yellow" onclick="filterOff();" style="padding: 6px 14px;" /><i class="icon-refresh"></i> Сбросить</a>

внешний вид кнопок не принципиален и основное на что следует обратить внимание на две функции
filterOn - включает фильтр по заданым критериям
filterOff - сбрасывает настройки фильтра


эти две функции обязательно необходимо подлючить

function filterOn(){
            $('#filter input[name="Filter[active]"]').val(1);
            $("#filter").submit();
}

function filterOff(){
    $('#filter input[name="Filter[active]"]').val(0);
    $("#filter").submit();
}

Рассмотрим теперь состовляющие формы на конкретном примере
Пример фильтра можно посмотреь в счетах
/admin/bills/index/page/1
 <input type="hidden" name="Filter[model]" value="Bills">
<input type="hidden" name="Filter[active]" value="">
 <select name="Filter[status]" id="filter_status" class="m-wrap chosen">
    <option value=""> ------ </option>
    <option value="paid" {if $filter.status=='paid' } selected="selected"{/if}> ОПЛАЧЕН </option>
    <option value="sent" {if $filter.status=='sent' } selected="selected"{/if}> ВЫСТАВЛЕН </option>
    <option value="draft" {if $filter.status=='draft' } selected="selected"{/if} > ЧЕРНОВИК </option>
    <option value="delayed" {if $filter.status=='delayed' } selected="selected"{/if}> ПРОСРОЧЕН </option>
    <option value="aborted" {if $filter.status=='aborted' } selected="selected"{/if}> ОТМЕНЕН </option>
 </select>
 <input type="text" name="Filter[email__users]" id="filter_email" value="{$filter.email__users}" class="m-wrap chosen" />
 <input type="text" name="Filter[first_name__users]" id="filter_first_name" value="{$filter.first_name__users}" class="m-wrap chosen" />
 <input class="m-wrap m-ctrl-medium date-picker" name="Filter[due_date->from]" readonly="" size="16" type="text" value="{$filter.due_date_from}"><span class="add-on"><i class="icon-calendar"></i></span>
 <input class="m-wrap m-ctrl-medium date-picker" name="Filter[due_date->to]" readonly="" size="16" type="text" value="{$filter.due_date_to}"><span class="add-on"><i class="icon-calendar"></i></span>

 В скрытое поле модели, мы добавили модель Bills. Приложение подключить класс application/models/BillsDb.php и создат
 экземпляр класса и вытянет название таблицы

 Filter[status] если статут будет установлен приложение будет искать его в модели Bills, а именно если таблица назывется
 bills, то bills.`status`='value'

 Filter[email__users] Тут стоит обратить внимание что у нас стоят двойное подчеркивание между email и users
 Это говорит о том, что мы ссылкаемя на таблицу, которая указывается через двойное поддчеркивание
 т.е. приложение будет искать значение колонки email в таблице users. users.`email`
 таким образом, если у нас организована связб между различными таблицам мы можем поля фильтра привязывать к другим таблицам

 Следующий очень важный момент это фильр по датам
 Как правило в 99% случаем колонки в таблице отвечающие за какие-либо даты имеют название date, если у вас такого нет, то к
 сожалению на данном этапе фильтр по датам будет рабоать некорректно
 Filter[due_date->from] символ "->" готоврит о том, чо будет использоваться фильтр по дате и значение этого поля
 будет началом даты (т.е. дата от)
 Filter[due_date->to] говорит до какого периода будет дата (Дата до)

 Приложение парсит поле и имет в нем обязательный параметр date и либо to или from

 в нашем случае колонка называется в тбалицу due_date и приложение будет искать в первом случае
 bills.due_date >= value во втором bills.due_date <= value
 так что к сожалению пока даты нельзя привязывать к другим таблицам, т.е. даты привязываются к модели, к которой мы
 указали

в обработчике используется следующий код:
$filter=false;
if(isset($_POST['Filter'])) {
            $filter = new Filter($_POST['Filter']);
}
if(isset($_SESSION['Filter']['model']) && $_SESSION['Filter']['model'] !== 'ModelName') {
        unset($_SESSION['Filter']);
        $_SESSION['Filter']['active']=0;
    }
$filterArray=@$_SESSION['Filter'];

Тут стоти проверка на модель для фильтра, так как если в одном месте выставить филтр а потом перейти в другое
где тоже был выставлен фильтр, то произойдет ошибка, следуюет добавить данную проверку

т.е. проверяется пришли ли нам данные для фильтра из формы
так же присваиеваем переменной, которую впоследтсвии отправим в шаблон значение фильтров сохраненных в сессии, если
таковые имеются
$this->smarty->assign('filter',$filterArray);

Далее нам нужно передать переменную фильтра в метод, который производит выборку и которую нужно будет фильтровать
$this -> smarty -> assign('items', $this->bill->getAllBills($filter));


в мтеоде модели который возвращает данные вызовем $filterSql = $this->ckeckFilter($filter);

и добавим его в запрос

если в запросе уже используется WHERE тогда нужно передать вторым параметром false чтобы приложение не подставляло
слово WHERE
$filterSql = $this->ckeckFilter($filter,false);