var CoomingSoon = function () {

    return {
        //main function to initiate the module
        init: function () {

            $.backstretch([
    		        "/css/assets/img/bg/1.jpg",
    		        "/css/assets/img/bg/2.jpg",
    		        "/css/assets/img/bg/3.jpg",
    		        "/css/assets/img/bg/4.jpg"
    		        ], {
    		          fade: 1000,
    		          duration: 10000
    		    });

        }

    };

}();